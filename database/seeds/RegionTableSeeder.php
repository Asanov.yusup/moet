<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionTableSeeder extends Seeder
{
    private $region;

    public function __construct(\App\Models\Region $region)
    {
        $this->region = $region;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'name' => [
                    'ru' => 'Бишкек',
                    'en' => 'Bishkek',
                    'kg' => 'Бишкек',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Баткен',
                    'en' => 'Batken',
                    'kg' => 'Баткен',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Жалал-Абад',
                    'en' => 'Jalal-Abad',
                    'kg' => 'Жалал-Абад',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Иссык-Куль',
                    'en' => 'Issyk-Kul',
                    'kg' => 'Ысык-Көл',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Нарын',
                    'en' => 'Naryn',
                    'kg' => 'Нарын',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Ош',
                    'en' => 'Osh',
                    'kg' => 'Ош',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Талас',
                    'en' => 'Talas',
                    'kg' => 'Талас',
                ],
            ],

            [
                'name' => [
                    'ru' => 'Чуй',
                    'en' => 'Chui',
                    'kg' => 'Чуй',
                ],
            ],

        ];

        $this->region->truncate();

        foreach ($regions as $region) {
            $this->region->create($region);
        }
    }
}
