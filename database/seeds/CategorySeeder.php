<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * @var \App\Services\CategoryService
     */
    private $categoryService;

    public function __construct(\App\Services\CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        $category_data = [

            [
                'name' => [
                    'ru' => 'шапка',
                    'en' => 'header',
                    'kg' => 'башы',
                ],
                'owner' => 'menus',
                'children' => []
            ],
        ];

        foreach ($category_data as $category_datum) {
            $children = array_pop($category_datum);
            $category = $this->categoryService->createCategory($category_datum, null);
            foreach ($children as $value) {
                $children2 = array_pop($value);
                $category2 = $this->categoryService->createCategory($value, $category->id);
                foreach ($children2 as $value2) {
                    array_pop($value2);
                    $this->categoryService->createCategory($value2, $category2->id);
                }
            }
        }
    }
}
