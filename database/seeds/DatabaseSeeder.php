<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionGroupTableSeeder::class,
            PermissionTableSeeder::class,
            AdminSeeder::class,
            RegionTableSeeder::class,
            CityTableSeeder::class,
            CategorySeeder::class,
            PagesSeeder::class,
            ExampleContactTableSeeder::class,
            NewsSeeder::class
        ]);
    }
}
