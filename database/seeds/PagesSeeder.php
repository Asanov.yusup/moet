<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{

    /**
     * @var \App\Models\Page
     */
    private $page;

    /**
     * PagesSeeder constructor.
     * @param \App\Models\Page $page
     */
    public function __construct(\App\Models\Page $page)
    {
        $this->page = $page;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $pages = [
            [
                'title' => [
                    'ru' => 'Главная',
                    'en' => 'Main',
                    'kg' => 'Башкы',
                ],
                'prefix' => null,
                'page_content' => [
                    'ru' => 'Главная',
                    'en' => 'Main',
                    'kg' => 'Башкы',
                ],
                'meta_keywords' => [
                    'ru' => 'Главная',
                    'en' => 'Main',
                    'kg' => 'Башкы',
                ],
                'meta_title' => [
                    'ru' => 'Главная',
                    'en' => 'Main',
                    'kg' => 'Башкы',
                ],
                'meta_description' => [
                    'ru' => 'Главная',
                    'en' => 'Main',
                    'kg' => 'Башкы',
                ],
                'site_display' => 1

            ],

            [
                'title' => [
                    'ru' => 'Контакты',
                    'en' => 'Contacts',
                    'kg' => 'Байланыш',
                ],
                'prefix' => 'contacts',
                'page_content' => [
                    'ru' => 'МЫ В СОЦИАЛЬНЫХ СЕТЯХ
                    Whatsapp для заказа: 0770-44-10-30
                    Всегда рады ответить на любые вопросы. ',
                    'en' => 'Contacts',
                    'kg' => 'Байланыш',
                ],
                'meta_keywords' => [
                    'ru' => 'Контакты',
                    'en' => 'Contacts',
                    'kg' => 'Байланыш',
                ],
                'meta_title' => [
                    'ru' => 'Контакты',
                    'en' => 'Contacts',
                    'kg' => 'Байланыш',
                ],
                'meta_description' => [
                    'ru' => 'Контакты',
                    'en' => 'Contacts',
                    'kg' => 'Байланыш',
                ],
                'site_display' => 1
            ],
            [
                'title' => [
                    'ru' => 'Новости',
                    'en' => 'news',
                    'kg' => 'news',
                ],
                'prefix' => 'news',
                'page_content' => [
                    'ru' => 'Новости',
                    'en' => 'news',
                    'kg' => 'news',
                ],
                'meta_title' => [
                    'ru' => 'Новости',
                    'en' => 'news',
                    'kg' => 'news',
                ],
                'meta_keywords' => [
                    'ru' => 'Новости',
                    'en' => 'news',
                    'kg' => 'news',
                ],
                'meta_description' => [
                    'ru' => 'Новости',
                    'en' => 'news',
                    'kg' => 'news',
                ],
                'site_display' => 1
            ],

            [
                'title' => [
                    'ru' => 'Корзина',
                    'en' => 'Cart',
                    'kg' => 'Корзина',
                ],
                'prefix' => 'cart',
                'page_content' => [
                    'ru' => 'Корзина',
                    'en' => 'Cart',
                    'kg' => 'Байланыш',
                ],
                'meta_title' => [
                    'ru' => 'Корзина',
                    'en' => 'Корзина',
                    'kg' => 'Корзина',
                ],
                'meta_keywords' => [
                    'ru' => 'Корзина',
                    'en' => 'Корзина',
                    'kg' => 'Корзина',
                ],
                'meta_description' => [
                    'ru' => 'Корзина',
                    'en' => 'Корзина',
                    'kg' => 'Корзина',
                ],
                'site_display' => 1
            ],
        ];

        $this->page->truncate();

        foreach ($pages as $page){
            $this->page->create($page);
        }
    }
}
