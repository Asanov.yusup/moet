<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    private $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id' => 1,
                'group_id' => 1,
                'alias' => 'manage_admins',
                'name' => 'Управление администраторами',
                'desc' => 'Позволяет создавать администраторов и назначать им роли'
            ],

            [
                'id' => 2,
                'group_id' => 2,
                'alias' => 'manage_pages',
                'name' => 'Управление страницами',
                'desc' => 'Позволяет редактировать страницы магазина'
            ],

            [
                'id' => 3,
                'group_id' => 2,
                'alias' => 'manage_staff',
                'name' => 'Обслуживающий персонал',
                'desc' => 'Позволяет создавать курьеров, кассиров и другой обслуживающий персонал'
            ],

        ];

        DB::table('permissions')->truncate();

        foreach ($permissions as $permission) {
            DB::table('permissions')->insert($permission);
        }
    }
}
