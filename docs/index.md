# Общая концепция

StarterKit предназначен для быстрой реализации бэкэнда при 
условии следования простому Гайду.
 
Все проекты делаются по принципу API Based App. Иначе говоря, 
мы делаем бэкэнд, делаем апи для доступа к сущностям. 
 
Основной посыл - это не делать CMS с дальнейшей необходимостью 
поддержки тонн кода, а сделать простую систему, с базовыми 
возможностями из коробки. 

Подход в разработке заключается в том, что требования проекта мы 
реализуем согласно документации Laravel - Создем как обычно 
контроллер, вьюхи, реквесты, модели для основных сущностей.
 

Во всем этом, нам помогают сервисы реализованные в системе.

## Архитектура StarterKit

* [Core.js](arch/core_js.md)
* [Администраторы и права доступа](arch/admins.md)
* [Категории Nested Tree](arch/categories.md)
* [Новости](arch/news.md)
* [Страницы](arch/pages.md)

## Начало работы в StarterKit

* [Установка](using/install.md)

## Development Guide

* [Пример создание сущности "Контакт лист" ](guide/crud.md)

## Документация по фронту сайта

* [Установка, Архитиктура, Стеки](front/index.md)