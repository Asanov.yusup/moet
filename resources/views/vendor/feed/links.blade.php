@foreach($feeds as $name => $title)
    <link rel="alternate" type="application/rss+xml" href="{{ route("feeds.{$name}", ['locale' => $locale]) }}" title="{{ $title }}">
@endforeach
