<div class="recommended_items"><!--recommended_items-->
    <h2 class="title text-center">Рекомендуемые товары</h2>

    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($hit_products as $array => $hit_product)
                @if($array == 0)
                    <div class="item active">
                        @foreach($hit_product as $product)
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        @if($product->mainImage)
                                            <img class="img-thumbnail"
                                                 src="{{ $product->mainImage->original_file_name }}">
                                        @else
                                            <img class="img-thumbnail"
                                                 src="/app/images/no_image_placeholder.jpg">
                                        @endif
                                        <h2>{{ $product->price }}</h2>
                                        <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>
                                        <a href="#" class="btn btn-default add-to-cart"><i
                                                    class="fa fa-shopping-cart"></i>В
                                            корзину</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="item">
                        @foreach($hit_product as $product)
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        @if($product->mainImage)
                                            <img class="img-thumbnail"
                                                 src="{{ $product->mainImage->original_file_name }}">
                                        @else
                                            <img class="img-thumbnail"
                                                 src="/app/images/no_image_placeholder.jpg">
                                        @endif
                                        <h2>{{ $product->price }}</h2>
                                        <p>{{ $product->getTranslation('short_desc', $currentLocale) }}</p>
                                        <a href="#" class="btn btn-default add-to-cart"><i
                                                    class="fa fa-shopping-cart"></i>В
                                            корзину</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            @endforeach
        </div>
        <a class="left recommended-item-control" href="#recommended-item-carousel"
           data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right recommended-item-control" href="#recommended-item-carousel"
           data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
    </div>
</div><!--/recommended_items-->