@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('master.home')</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$page->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9">
                    <div class="shopping-cart">
                        @if($products->count())
                            <form action="{{ route('purchase.checkout', ['locale' => $currentLocale]) }}" method="post"
                                  class="ajaxForm" id="checkoutForm" data-block-element="#checkoutForm">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="cart-title">
                                            <h2 class="entry-title"><a href="">@lang('purchase.shopping_cart')</a></h2>
                                        </div>
                                        <!-- Start Table -->
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <td class="text-center" width="30%">@lang('account.image')</td>
                                                    <td class="text-left">@lang('account.product_name')</td>
                                                    <td class="text-left" width="20%">@lang('purchase.quantity')</td>
                                                    <td class="text-right" width="15%">@lang('account.unit_price')</td>
                                                    <td class="text-right" width="15%">@lang('purchase.total')</td>
                                                </tr>
                                                </thead>
                                                <tbody id="cartTable">
                                                @foreach ($products as $product)
                                                    <tr class="product_row product-row-{{$product->id}}">
                                                        <td class="text-center">
                                                            <a href="#">
                                                                <img class="img-thumbnail"
                                                                     src="{{ $product->mainImage->original_file_name }}"
                                                                     alt="{{ $product->mainImage->client_file_name }}"/>
                                                            </a>
                                                        </td>
                                                        <td class="text-left">
                                                            <a href="#">{{ $product->getTranslation('title', $currentLocale) }}</a>
                                                        </td>
                                                        <td class="text-left">
                                                            <div class="btn-block cart-put">
                                                                <input class="form-control cart_quantity" type="number"
                                                                       name="quantity[{{$product->id}}][qty]"
                                                                       value="{{ $array[$product->id]['qty'] }}"
                                                                       min="1"/>
                                                                <div class="input-group-btn cart-buttons">
                                                                    <button class="btn btn-primary refreshSum"
                                                                            data-toggle="tooltip"
                                                                            title="@lang('account.update')">
                                                                        <i class="fa fa-refresh"></i>
                                                                    </button>
                                                                    <button class="btn btn-danger removeFromCart"
                                                                            data-toggle="tooltip"
                                                                            data-url="{{ route('purchase.remove.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}"
                                                                            title="@lang('account.remove')">
                                                                        <i class="fa fa-times-circle"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-right"><span
                                                                    class="price_product">{{ $product->price }}</span>
                                                            сом
                                                        </td>
                                                        <td class="text-right"><span
                                                                    class="price_sum">{{ $product->price  * $array[$product->id]['qty']}}</span>
                                                            сом
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>


                                        <h3 class="title-group-3 gfont-1">@lang('purchase.what_would_you_like_to_do_next')</h3>
                                        <!-- Accordion start -->
                                        <div class="accordion-cart">
                                            <div class="panel-group" id="accordion">
                                                <!-- Start Coupon -->
                                                <div class="panel panel_default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-trigger" data-toggle="collapse"
                                                               data-parent="#accordion" href="#coupon">
                                                                Укажите Ваше имя и телефон
                                                                <i class="fa fa-caret-down"></i> </a>
                                                        </h4>
                                                    </div>
                                                    <div id="coupon" class="collapse in">
                                                        <div class="panel-body">

                                                            @if(auth()->guard('customers')->user() == null)
                                                                <label class="col-sm-3 control-label"
                                                                       for="name">@lang('purchase.name')
                                                                    <sup>*</sup></label>
                                                                <div class="input-group">
                                                                    <input class="form-control" type="text" name="name"
                                                                           id="name"
                                                                           placeholder="@lang('purchase.name')"/>
                                                                    <p class="help-block"></p>
                                                                </div>
                                                                <br>
                                                                <label class="col-sm-3 control-label"
                                                                       for="phone">@lang('purchase.phone')
                                                                    <sup>*</sup></label>
                                                                <div class="input-group">
                                                                    <input class="form-control" type="text" name="phone"
                                                                           id="phone"
                                                                           placeholder="@lang('purchase.phone')"/>
                                                                    <p class="help-block"></p>
                                                                </div>
                                                                <br>
                                                            @endif
                                                            <label class="col-sm-3 control-label"
                                                                   for="address">Место доставки</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" name="address"
                                                                       id="address" placeholder="Место доставки"/>
                                                                <p class="help-block"></p>
                                                            </div>
                                                            <br>
                                                            <label class="col-sm-3 control-label"
                                                                   for="comment">Коментарии</label>
                                                            <div class="input-group">
                                                            <textarea class="form-control" name="comment"
                                                                      id="comment" placeholder="Коментарии"></textarea>
                                                                <p class="help-block"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Coupon -->
                                                <!-- Start Shipping -->
                                            {{--<div class="panel panel_default">--}}
                                            {{--<div class="panel-heading">--}}
                                            {{--<h4 class="panel-title">--}}
                                            {{--<a class="accordion-trigger collapsed" data-toggle="collapse"--}}
                                            {{--data-parent="#accordion" href="#shipping">@lang('purchase.shipping')<i class="fa fa-caret-down"></i> </a>--}}
                                            {{--</h4>--}}
                                            {{--</div>--}}
                                            {{--<div id="shipping" class="collapse">--}}
                                            {{--<div class="panel-body">--}}
                                            {{--<div class="col-sm-12">--}}
                                            {{--<p>@lang('purchase.enter_your_address')</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-horizontal">--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label class="col-sm-2 control-label"--}}
                                            {{--for="region_id">@lang('purchase.region')</label>--}}
                                            {{--<div class="col-sm-10">--}}
                                            {{--<select class="form-control" id="region_id"--}}
                                            {{--name="region_id"--}}
                                            {{--data-url="{{ route('purchase.get.city', ['locale' => $currentLocale]) }}">--}}
                                            {{--<option value=""> --- @lang('purchase.region')--}}
                                            {{-------}}
                                            {{--</option>--}}
                                            {{--@foreach($regions as $region)--}}
                                            {{--<option value="{{ $region->id }}">{{ $region->getTranslation('name', $currentLocale) }}</option>--}}
                                            {{--@endforeach--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label for="city_id"--}}
                                            {{--class="col-sm-2 control-label">@lang('purchase.city')</label>--}}
                                            {{--<div class="col-sm-10">--}}
                                            {{--<select class="form-control" id="city_id"--}}
                                            {{--name="city_id">--}}
                                            {{--<option value="">--- @lang('purchase.city')--}}
                                            {{-------}}
                                            {{--</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <!-- Start Shipping -->
                                            </div>
                                        </div>
                                        <!-- Accordion end -->
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-8">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-right">
                                                            <strong>@lang('account.subtotal'):</strong>
                                                        </td>
                                                        <td class="text-right"><span class="sub_total_cart"></span> сом
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <strong>@lang('purchase.total'):</strong>
                                                        </td>
                                                        <td class="text-right"><span class="total_cart"></span> сом</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <input type="hidden" name="total" id="total">

                                        <div class="shopping-checkout">
                                            <a href="{{ url()->previous() }}"
                                               class="btn btn-default pull-left">@lang('purchase.continue_shopping')</a>
                                            <a href="#" id="checkout_order"
                                               class="btn btn-primary pull-right">@lang('purchase.checkout')</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @else
                            <h3>Заказов нет</h3>
                        @endif
                    </div>
                    <!-- End Shopping-Cart -->
                </div>
            </div>
        </div>
        <br>
        <!-- START SUBSCRIBE-AREA -->
    </section>
@endsection