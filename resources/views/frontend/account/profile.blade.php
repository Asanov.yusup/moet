@extends('frontend.layouts.master')
@section('content')
    <section class="page-content login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu">
                        {{--<li><h2>Личный кабинет</h2></li>--}}
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="entry-header-area">
                        <div class="entry-header">
                            <h2 class="entry-title">
                                Личный кабинет
                                {{--@lang('master.account')--}}
                            </h2>
                        </div>
                    </div>

                    <div class="checkout-area">
                        <div class="panel-group" id="accordion">

                            <div class="panel panel_default active">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-trigger collapsed" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#checkout-confirm">@lang('account.order_history_and_details')
                                            <i class="fa fa-caret-down"></i> </a>
                                    </h4>
                                </div>
                                <div id="checkout-confirm" class="collapse">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <td class="text-center">@lang('account.image')</td>
                                                    <td class="text-left">@lang('account.product_name')</td>
                                                    <td class="text-left">Категория</td>
                                                    <td class="text-left">@lang('purchase.quantity')</td>
                                                    <td class="text-right">@lang('account.unit_price')</td>
                                                    <td class="text-right">@lang('purchase.total')</td>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                {{--{{ dd($orders) }}--}}

                                                @foreach ($orders as $order)
                                                    @foreach ($order->products as $product)
                                                        {{--{{ dd($product) }}--}}
                                                        <tr class="product_row product-row-{{$product->id}}">
                                                            <td class="text-center">
                                                                <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug' => $product->categories->first()->slug,'slug'=> $product->slug])}}">
                                                                    <img width="100%" class="img-thumbnail"
                                                                         src="{{ $product->mainImage->original_file_name }}"
                                                                         alt="{{ $product->mainImage->client_file_name }}"/>
                                                                </a>
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=> $product->categories->first()->slug,'slug'=> $product->slug])}}">{{ $product->getTranslation('title', $currentLocale) }}</a>
                                                            </td>
                                                            <td class="text-left">
                                                                @foreach ($product->categories as $category)

                                                                    <a href="{{ route('products.catalog', ['lacale' => $currentLocale, 'slug' => $category->slug]) }}">
                                                                        {{ $category->getTranslation('name', $currentLocale) }}
                                                                    </a>
                                                                    <br>

                                                                @endforeach
                                                            </td>

                                                            <td>
                                                                {{ $product->pivot->qty }}

                                                            </td>
                                                            <td class="text-right">
                                                                <div class="price-box">
                                                                    <span class="price_product">{{ $product->price }}</span>сом
                                                                </div>
                                                            </td>
                                                            <td class="text-right" width="130px">
                                                                {{ ($product->price * $product->pivot->qty) }} сом
                                                            </td>
                                                        </tr>

                                                    @endforeach
                                                    <tr>
                                                        <td class="text-right" colspan="5">
                                                            <b style="float: left">Заказ №{{ $order->id }}</b>

                                                            <strong>@lang('account.subtotal') </strong>
                                                            ({{ \Carbon\Carbon::parse( $order->created_at)->format('d-m-Y') }}
                                                            г):
                                                        </td>
                                                        <td class="text-right">{{ $order->total }} сом</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel_default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-trigger collapsed" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#payment-method">
                                            Список моих избранных
                                            {{--@lang('account.my_wish_list')--}}
                                            <i class="fa fa-caret-down"></i> </a>
                                    </h4>
                                </div>
                                <div id="payment-method" class="collapse">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-center">@lang('account.image')</td>
                                                    <td class="text-left">@lang('account.product_name')</td>
                                                    <td class="text-left">Категория</td>
                                                    <td class="text-right">@lang('account.unit_price')</td>
                                                    <td class="text-right">@lang('account.action')</td>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @foreach ($favorites as $favorite)
                                                    @if(get_class($favorite->favoritesable) == $productClass)

                                                        <tr class="product_row product-row-{{$favorite->favoritesable->id}}">
                                                            <td class="text-center">
                                                                <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=>$favorite->favoritesable->categories->first()->slug,'slug'=> $favorite->favoritesable->slug])}}">
                                                                    <img width="100%" class="img-thumbnail"
                                                                         src="{{ $favorite->favoritesable->mainImage->original_file_name }}"
                                                                         alt="{{ $favorite->favoritesable->mainImage->client_file_name }}"/>
                                                                </a>
                                                            </td>
                                                            <td class="text-left">
                                                                <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=>$favorite->favoritesable->categories->first()->slug,'slug'=> $favorite->favoritesable->slug])}}">{{ $favorite->favoritesable->getTranslation('title', $currentLocale) }}</a>
                                                            </td>
                                                            <td class="text-left">

                                                                @foreach ($favorite->favoritesable->categories as $category)

                                                                    <a href="{{ route('products.catalog', ['lacale' => $currentLocale, 'slug' => $category->slug]) }}">
                                                                        {{ $category->getTranslation('name', $currentLocale) }}
                                                                    </a>
                                                                    <br>

                                                                @endforeach
                                                            </td>

                                                            <td class="text-right">
                                                                <div class="price-box">
                                                                    <span class="price_product">{{ $favorite->favoritesable->price }}</span>сом
                                                                </div>
                                                            </td>
                                                            <td class="text-right" width="130px">
                                                                {{--<button class="btn btn-primary"--}}
                                                                {{--data-toggle="tooltip"--}}
                                                                {{--title="@lang('home.to_cart')" type="button">--}}
                                                                {{--<i class="fa fa-shopping-cart"></i>--}}
                                                                {{--</button>--}}
                                                                <a href="#" class="btn btn-danger removeFavorite"
                                                                   data-url="{{ route('remove.favorite', ['locale' => $currentLocale, 'type' => $productClass, 'id' => $favorite->favoritesable->id]) }}"
                                                                   data-toggle="tooltip"
                                                                   title="@lang('account.remove')">
                                                                    <i class="fa fa-times"></i>
                                                                </a>

                                                            </td>

                                                        </tr>

                                                    @endif
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel_default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-trigger collapsed" data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#shipping-address">Мой профиль<i
                                                    class="fa fa-caret-down"></i> </a>
                                    </h4>
                                </div>
                                <div id="shipping-address" class="collapse">
                                    <div class="panel-body">
                                        <form action="{{route('frontend.auth.profile.update', ['locale' => $currentLocale])}}"
                                              method="post" class="ajaxForm" data-block-element=".box-body"
                                              id="customerForm">
                                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">@lang('account.first_name')</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="first_name" id="first_name"
                                                               class="form-control"
                                                               value="{{$customer->first_name}}" autocomplete="off"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">@lang('account.last_name')</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="last_name" id="last_name"
                                                               class="form-control"
                                                               value="{{$customer->last_name}}" autocomplete="off"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Телефон</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="phone" id="phone" class="form-control"
                                                               value="{{$customer->phone}}" autocomplete="off"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="email" id="email" class="form-control"
                                                               value="{{$customer->email}}" autocomplete="off"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Пароль</label>
                                                    <div class="col-sm-10">
                                                        <input type="password" name="password" id="password"
                                                               class="form-control"
                                                               placeholder="">
                                                    </div>
                                                </div>
                                                <div class="buttons clearfix pull-right">
                                                    {{--<a href="#" class="btn btn-primary">@lang('account.save')</a>--}}
                                                    <button type="submit" class="btn btn-primary">Изменить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

