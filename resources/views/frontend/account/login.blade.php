@extends('frontend.layouts.master')
@section('content')
    <section class="page-content login-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('master.home')</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                Авторизация | Регистрация
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="login">
                        <div class="login-tab">
                            <ul role="tablist" class="text-center">
                                <li role="presentation"
                                    class="register_tab active">
                                    <a href="#register" role="tab" data-toggle="tab">Авторизация</a>
                                </li>
                                <li role="presentation" class="register_tab">
                                    <a href="#login" role="tab" data-toggle="tab">Регистрация</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="register">

                                <div class="form-group login_form col-md-4 col-sm-12 col-xs-12">
                                    <form action="{{route('frontend.auth.login.post', ['locale' => $currentLocale])}}"
                                          method="post" class="ajaxForm" id="auth">
                                        <div class="form-group">
                                            <label for="auth_email">Email:</label>
                                            <input type="text" name="auth_email" class="form-control"
                                                   id="auth_email"
                                                   placeholder="введите ваш email">
                                            <p class="help-block"></p>

                                        </div>
                                        <div class="form-group">
                                            <label for="auth_password">Ваш пароль:</label>
                                            <input type="password" name="auth_password"
                                                   class="form-control"
                                                   id="auth_password"
                                                   placeholder="введите пароль">
                                            <p class="help-block"></p>

                                        </div>
                                        <p class="forgot-pass"><a
                                                    href="{{route('frontend.auth.get.email', ['locale' => $currentLocale])}}">Забыли
                                                пароль?</a></p>
                                        <button type="submit" class="btn btn-lg btn-primary">
                                            Войти
                                        </button>
                                    </form>

                                    <p class="forgot-pass">Или войдите через соц сети</p>
                                    <div class="text-center margin-bottom-20" id="uLogin"
                                         data-ulogin="display=panel;theme=flat;fields=first_name,last_name,email,nickname,photo,country;
                                                    providers=facebook,vkontakte,google,mailru,odnoklassniki;
                                                    redirect_uri={{ route('frontend.ulogin', ['locale' => $currentLocale]) }};mobilebuttons=0;">
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="login">
                                <div class="row">
                                    <div class="form-group login_form col-md-4 col-sm-12 col-xs-12">
                                        <form action="{{route('frontend.auth.register.post', ['locale' => $currentLocale])}}"
                                              method="post" class="ajaxForm" id="reg">
                                            <div class="form-group">
                                                <label for="last_name">Фамилия<span
                                                            class="red-color">*</span>:</label>
                                                <input type="text" name="last_name"
                                                       class="form-control"
                                                       id="last_name" placeholder="введите фамилию">
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="first_name">Имя<span
                                                            class="red-color">*</span>:</label>
                                                <input type="text" name="first_name"
                                                       class="form-control"
                                                       id="first_name" placeholder="введите имя">
                                                <p class="help-block"></p>

                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Номер телефона<span
                                                            class="red-color"></span>:</label>
                                                <input type="text" name="phone" class="form-control"
                                                       id="phone"
                                                       placeholder="введите номер мобильного телефона">
                                                <p class="help-block"></p>

                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email<span
                                                            class="red-color">*</span>:</label>
                                                <input type="text" name="email" class="form-control"
                                                       id="email"
                                                       placeholder="введите ваш email">
                                                <p class="help-block"></p>

                                            </div>
                                            <div class="form-group">
                                                <label for="password">Ваш пароль<span
                                                            class="red-color">*</span>:</label>
                                                <input type="password" name="password"
                                                       class="form-control"
                                                       id="password"
                                                       placeholder="придумайте пароль">
                                                <p class="help-block"></p>

                                            </div>
                                            <div class="form-group">
                                                <label for="re_password">Повторите пароль<span
                                                            class="red-color">*</span>:</label>
                                                <input type="password" name="re_password"
                                                       class="form-control"
                                                       id="re_password"
                                                       placeholder="повторите пароль">
                                                <p class="help-block"></p>

                                            </div>
                                            <button type="submit" class="btn btn-lg btn-primary">
                                                Зарегистрироваться
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="//ulogin.ru/js/ulogin.js"></script>
@endpush