@extends('frontend.layouts.master')
@section('content')
    <section class="page-content login-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group login_form col-md-4 col-sm-12 col-xs-12">
                        <p>Восстановление пароля</p>
                        <form action="{{route('frontend.auth.change.password', ['locale' => $currentLocale, 'code' => $code])}}"
                              method="post" class="ajaxForm"
                              data-block-element="#authForm" id="authForm">
                            <div class="form-group">
                                <label for="password">Ваш пароль<span
                                            class="red-color">*</span>:</label>
                                <input type="password" name="password"
                                       class="form-control"
                                       id="password"
                                       placeholder="придумайте пароль">
                            </div>
                            <div class="form-group">
                                <label for="re_password">Повторите пароль<span
                                            class="red-color">*</span>:</label>
                                <input type="password" name="re_password"
                                       class="form-control"
                                       id="re_password"
                                       placeholder="повторите пароль">
                            </div>

                            <button type="submit" class="btn btn-lg btn-primary">
                                Восстановить
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SUBSCRIBE-AREA -->
    </section>
@endsection
