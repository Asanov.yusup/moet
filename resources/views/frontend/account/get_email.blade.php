@extends('frontend.layouts.master')
@section('content')
    <section class="page-content login-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group login_form col-md-4 col-sm-12 col-xs-12">
                        <p>Для восстановления пароля укажите свой Email</p>
                        <form action="{{route('frontend.auth.post.email', ['locale' => $currentLocale])}}"
                              method="post" class="ajaxForm"
                              data-block-element="#authForm" id="authForm">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="text" name="email" class="form-control"
                                       id="email" placeholder="введите ваш email">
                            </div>

                            <button type="submit" class="btn btn-lg btn-primary">
                                Отправить
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END SUBSCRIBE-AREA -->
    </section>
@endsection
