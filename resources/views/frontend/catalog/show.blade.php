@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        @foreach($catalogAncestors as $ancestor)
                            <li>
                                <a href="{{route('show.catalog',['locale'=>$currentLocale, 'slug' => $ancestor->slug])}}">
                                {{ $ancestor->getTranslation('name',$currentLocale) }}
                                </a>
                            </li>
                        @endforeach
                        <li class="active">
                            <a href="#" onclick="return false">{{$catalog->getTranslation('name',$currentLocale)}}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9 col-xs-12">
                    <!-- START PRODUCT-AREA -->
                    <div class="product-area">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Start Product-Menu -->
                                <div class="product-menu">
                                    <div class="product-title">
                                        <h3 class="title-group-3 gfont-1">{{$catalog->getTranslation('name',$currentLocale)}}</h3>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="product">
                                    <div class="tab-content">
                                        <div class="row">
                                            @if(count($catalog->children))
                                                @foreach($catalog->children as $subCatalog)
                                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                                        <div class="catalog-items">
                                                            <div class="catalogs-img">
                                                                <a href="{{(count($subCatalog->children)>0) ? route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) : route('products.catalog', ['locale' => $currentLocale,'catalogSlug'=>$subCatalog->slug])}}">
                                                                    <img class="primary-img"
                                                                         src="{{ (isset($subCatalog->medias)) ? $subCatalog->medias->original_file_name : '/app/images/no_image_placeholder.jpg' }}"
                                                                         alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="catalogs-description text-center">
                                                                <h5 class="margin-ceil-10">
                                                                    <a href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) }}">
                                                                        {{ $subCatalog->getTranslation('name', $currentLocale) }}</a>
                                                                </h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection