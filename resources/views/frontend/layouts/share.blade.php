<div id="share-buttons">

    <!-- Buffer -->
    <a href="https://bufferapp.com/add?url={{ Request::url() }}&amp;text=test" rel="nofollow"
       target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/buffer.png" alt="Buffer"/>
    </a>

    <!-- Digg -->
    <a href="http://www.digg.com/submit?url={{ Request::url() }}" target="_blank" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/diggit.png" alt="Digg"/>
    </a>

    <!-- Email -->
    <a href="mailto:?Subject=Simple Share Buttons&amp;Body=test {{ Request::url() }}" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email"/>
    </a>

    <!-- Facebook -->
    <a href="http://www.facebook.com/sharer.php?u={{ Request::url() }}" target="_blank" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook"/>
    </a>

    <!-- Google+ -->
    <a href="https://plus.google.com/share?url={{ Request::url() }}" target="_blank" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google"/>
    </a>

    <!-- LinkedIn -->
    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ Request::url() }}" target="_blank" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn"/>
    </a>

    <!-- Pinterest -->
    <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
        <img src="https://simplesharebuttons.com/images/somacro/pinterest.png" alt="Pinterest"/>
    </a>

    <!-- Print -->
    <a href="javascript:;" onclick="window.print()" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/print.png" alt="Print"/>
    </a>

    <!-- Reddit -->
    <a href="http://reddit.com/submit?url={{ Request::url() }}&amp;title=test" rel="nofollow"
       target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/reddit.png" alt="Reddit"/>
    </a>

    <!-- StumbleUpon-->
    <a href="http://www.stumbleupon.com/submit?url={{ Request::url() }}&amp;title=test" rel="nofollow"
       target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/stumbleupon.png" alt="StumbleUpon"/>
    </a>

    <!-- Tumblr-->
    <a href="http://www.tumblr.com/share/link?url={{ Request::url() }}&amp;title=test" rel="nofollow"
       target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/tumblr.png" alt="Tumblr"/>
    </a>

    <!-- Twitter -->
    <a href="https://twitter.com/share?url={{ Request::url() }}&amp;text=test&amp;hashtags=test" rel="nofollow"
       target="_blank">
        <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter"/>
    </a>

    <!-- VK -->
    <a href="http://vkontakte.ru/share.php?url={{ Request::url() }}" target="_blank" rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/vk.png" alt="VK"/>
    </a>

    <!-- Yummly -->
    <a href="http://www.yummly.com/urb/verify?url={{ Request::url() }}&amp;title=test"
       target="_blank"  rel="nofollow">
        <img src="https://simplesharebuttons.com/images/somacro/yummly.png" alt="Yummly"/>
    </a>
    <a href="http://instagram.com/?url={{ Request::url()}}"  target="_blank" rel="nofollow">
        <img src="/images/instagram.png" alt="Instagram" />
    </a>

</div>
