<div class="col-md-3">
    <!-- CATEGORY-MENU-LIST START -->
    <div class="left-category-menu hidden-sm hidden-xs">
        <div class="left-product-cat">
            <div class="category-heading">
                <h2>@lang('home.categories')</h2>
            </div>
            <div class="category-menu-list">
                <ul>
                    <!-- Single menu start -->
                    {{--{{dd($currentLocale)}}--}}
                    @foreach($catalogs as $catalog)
                        <li class="arrow-plus">
                            <a href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $catalog->slug]) }}">{{$catalog->getTranslation('name', $currentLocale)}}</a>
                            <!--  MEGA MENU START -->
                            <div class="cat-left-drop-menu">
                                @foreach($catalog->children as $subCatalog)
                                    <div class="cat-left-drop-menu-left">
                                        <a class="menu-item-heading" href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) }}">{{$subCatalog->getTranslation('name',$currentLocale)}}</a>
                                        <ul>
                                            @foreach($subCatalog->children as $item)
                                            <li><a href="{{ route('products.catalog',  ['locale' => $currentLocale, 'catalogSlug' => $item->slug]) }}">{{$item->getTranslation('name',$currentLocale)}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                            <!-- MEGA MENU END -->
                        </li>
                @endforeach
                <!-- Single menu end -->

                    <!-- MENU ACCORDION END -->
                </ul>
            </div>
        </div>
    </div>
    <!-- END CATEGORY-MENU-LIST -->
</div>