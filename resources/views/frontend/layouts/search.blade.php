@if(isset($merge))
    <tr>
        @if($merge->name)
            @if((count($merge->children)>0))
                <th class="search-item"><a
                            href="{{route('show.catalog',['locale'=>$currentLocale,'slug'=>$merge->slug])}}">{{$merge->name}}</a>
                </th>
            @else
                <th class="search-item"><a
                            href="{{route('products.catalog', ['locale' => $currentLocale,'catalogSlug'=>$merge->slug])}}">{{$merge->name}}</a>
                </th>
            @endif
        @elseif($merge->title)
            <th class="search-item"><a
                        href="{{ route('show.product', ['locale' => $currentLocale, 'catSlug' => $merge->slug, 'slug' => $merge->slug])}}">{{$merge->title}}</a>
            </th>
        @endif
    </tr>
@else
    <tr class="not-found">
        <th>Не найдено</th>
    </tr>
@endif

