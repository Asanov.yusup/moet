<!doctype html>
<html lang="{{ $currentLocale }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <title>{{ MetaTag::get('title') }} :: moet.kg</title>
    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('image') !!}
    {!! MetaTag::openGraph() !!}
    {!! MetaTag::twitterCard() !!}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('feed::links', ['locale' => $currentLocale])

<!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="/app/images/favicon.ico">

    <!-- Google Fonts
		============================================ -->
    {{--<link href='https://fonts.googleapis.com/css?family=Raleway:400,600' rel='stylesheet' type='text/css'>--}}
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap CSS
            ============================================ -->
    <link rel="stylesheet" href="/electronics/css/bootstrap.min.css">
    <!-- Font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/owl.carousel.css">
    <link rel="stylesheet" href="/electronics/css/owl.theme.css">
    <link rel="stylesheet" href="/electronics/css/owl.transitions.css">
    <!-- nivo slider CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/nivo-slider.css" type="text/css"/>
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/meanmenu.min.css">
    <!-- jquery-ui CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/jquery-ui.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/animate.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="/electronics/css/responsive.css">
    <link rel="stylesheet" href="/frontend/css/app.css">
    <link rel="stylesheet" href="/frontend/ilya-birman-likely-2.3.1/likely.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css"/>

{{--@if(env('APP_ENV') != 'test')--}}
    {{--<!-- Yandex.Metrika counter -->--}}
        {{--<script type="text/javascript">--}}
            {{--(function (m, e, t, r, i, k, a) {--}}
                {{--m[i] = m[i] || function () {--}}
                    {{--(m[i].a = m[i].a || []).push(arguments)--}}
                {{--};--}}
                {{--m[i].l = 1 * new Date();--}}
                {{--k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)--}}
            {{--})--}}
            {{--(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");--}}

            {{--ym(51472717, "init", {--}}
                {{--id: 51472717,--}}
                {{--clickmap: true,--}}
                {{--trackLinks: true,--}}
                {{--accurateTrackBounce: true,--}}
                {{--webvisor: true--}}
            {{--});--}}
        {{--</script>--}}
        {{--<noscript>--}}
            {{--<div><img src="https://mc.yandex.ru/watch/51472717" style="position:absolute; left:-9999px;" alt=""/></div>--}}
        {{--</noscript>--}}

{{--@endif--}}
<!-- /Yandex.Metrika counter -->

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<!-- HEADER-AREA START -->
<header class="header-area">

    <div class="header-middle" id="navbar">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 hidden-xs">
                    <div class="logo">
                        <a href="{{route('home',['locale'=>$currentLocale])}}" title="Moet">
                            <img width="150px" src="/images/logo/logo.png" alt="Moet"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="quick-access">
                        <div class="search-by-category">
                            {{--<div class="search-container">
                                <select>
                                    <option class="all-cate">@lang('home.categories')</option>
                                    @foreach($catalogs as $catalog)
                                        <optgroup class="cate-item-head"
                                                  label="{{$catalog->getTranslation('name', $currentLocale)}}">
                                            @foreach($catalog->children as $subCatalog)
                                                <option class="cate-item-title">{{$subCatalog->getTranslation('name', $currentLocale)}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>--}}
                            <div class="header-search">
                                <form action="{{route('frontend.search', ['locale' => $currentLocale])}}" method="get">
                                    <button type="submit" class="search__button"><i class="fa fa-search"></i></button>
                                    <input type="text" name="query"
                                           @if( isset($searchQuery) ) value="{{htmlentities($searchQuery)}}"
                                           @endif class="search__input" placeholder="@lang('master.search')..."
                                           autocomplete="off">
                                </form>

                                {{--<form action="{{route('frontend.search.index',['locale'=>$currentLocale])}}"--}}
                                {{--id="search-form"--}}
                                {{--method="get">--}}
                                {{--<input type="text" class="search" name="search"--}}
                                {{--placeholder="@lang('master.search')">--}}

                                {{--<button type="submit"><i class="fa fa-search"></i></button>--}}
                                {{--</form>--}}
                                {{--<div class="search-results">--}}
                                {{--<table class="search-table"--}}
                                {{--data-url="{{route('frontend.search.index',['locale'=>$currentLocale])}}">--}}
                                {{--@include('frontend.layouts.search_list')--}}
                                {{--</table>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="top-cart">
                            <ul>
                                @if(isset($authCustomer))
                                    <li>
                                        <a href="{{route('frontend.auth.profile',['locale' => $currentLocale])}}">
                                            <span class="cart-icon">
                                                <i class="fa fa-heart-o"></i>
                                            </span>
                                        </a>
                                        <span class="notifyBox {{ (count($favoriteData[$productClass])) ? '' : 'notNotifyBox' }} "
                                              id="favoriteNotify">
                                            {{ (count($favoriteData[$productClass]) > 99) ? '...' : count($favoriteData[$productClass]) }}
                                        </span>
                                    </li>
                                @endif


                                <li style="">
                                    <a href="{{route('pages',['locale'=>$currentLocale,'prefix'=>'cart'])}}">
                                        <span class="cart-icon">
                                            <i class="fa fa-shopping-cart"></i>
                                        </span>
                                    </a>
                                    <span class="notifyBox {{ (count($cartData)) ? '' : 'notNotifyBox' }} "
                                          id="cartNotify">
                                            {{ (count($cartData) > 99) ? '...' : count($cartData) }}
                                        </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End logo & Search Box -->
        </div>
    </div>
    <!-- HEADER-MIDDLE END -->
    <!-- START MAINMENU-AREA -->
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mainmenu hidden-sm hidden-xs">
                        <nav>
                            <ul>
                                @if (isset($headerMenu->children))
                                    @foreach($headerMenu->children as $menu)
                                        <li>
                                            <a href="{{ route('pages', ['locale' => $currentLocale, 'prefix' => $menu->getTranslation('url', 'ru') ]) }}">{{ $menu->getTranslation('name', $currentLocale) }}</a>
                                            @if ($menu->children->count())
                                                <ul>
                                                    @foreach($menu->children as $subMenu)
                                                        <li>
                                                            <a href="{{ route('pages', ['locale' => $currentLocale, 'prefix' => $subMenu->getTranslation('url', 'ru') ])}}">{{ $subMenu->getTranslation('name', $currentLocale) }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                @endif

                                {{--<li class="hot"><a href="shop.html">Bestseller Products</a></li>--}}
                                {{--<li class="new"><a href="shop-list.html">New Products</a></li>--}}
                            </ul>

                            <ul class="language" style="float: right">
                                <li>
                                    <a href="#">
                                        {{$currentLocale}}
                                        <i class="fa fa-caret-down left-5"></i>
                                    </a>
                                    <ul style="max-width: 100px;">
                                        <li><a href="{{ route('changeLocale', ['locale' => 'ru']) }}">ru</a>
                                        </li>
                                        <li><a href="{{ route('changeLocale', ['locale' => 'en']) }}">en</a>
                                        </li>
                                        <li><a href="{{ route('changeLocale', ['locale' => 'kg']) }}">kg</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>


                            <ul style="float: right">
                                @if(isset($authCustomer))
                                    <li>
                                        <a href="#">
                                            {{ $authCustomer->first_name }}
                                            <i class="fa fa-caret-down left-5"></i>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{route('frontend.auth.profile',['locale'=>$currentLocale])}}">
                                                    Личный кабинет
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{route('frontend.auth.logout',['locale'=>$currentLocale])}}">Выйти</a>
                                            </li>
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{route('frontend.auth.login',['locale'=>$currentLocale])}}">Войти</a>
                                    </li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN-MENU-AREA -->
    <!-- Start Mobile-Categories-menu -->
    <div class="mobile-menu-area hidden-md hidden-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav id="mobile-menu">
                        <ul>
                            @foreach($catalogs as $catalog)
                                <li>
                                    <a href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $catalog->slug]) }}">
                                        {{$catalog->getTranslation('name', $currentLocale)}}</a>
                                    @foreach($catalog->children as $subCatalog)
                                        <ul>
                                            <li>
                                                <a class="menu-item-heading"
                                                   href="{{ route('show.catalog', ['locale' => $currentLocale,'slug' => $subCatalog->slug]) }}">
                                                    {{$subCatalog->getTranslation('name',$currentLocale)}}</a>
                                            </li>
                                        </ul>
                                    @endforeach
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- End Mobile-Categories-menu -->
</header>
@yield('content')

<ul class="menuChat bottomRight">
    <li class="share top">
        <img class="chatImg" src="/images/chat_64с.png">
        <ul class="submenuChat">
            <li>
                <a href="https://api.whatsapp.com/send?phone=996550401030&text=hello" target="_blank" rel="nofollow">
                    <img src="/images/whatsapp64c.png">
                </a>
            </li>
            <li>
                <a href="https://web.telegram.org/#/im?p=@galaperedon" target="_blank" rel="nofollow">
                    <img src="/images/telegram64c.png">
                </a>
            </li>
        </ul>
    </li>
</ul>

<footer class="footer-area">
    <!-- Footer Start -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-3"></div>
                <div class="col-3">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-vk"></i></a>
                        <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                    </div>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    </div>
</footer>

<!-- FOOTER-AREA END -->
<!-- QUICKVIEW PRODUCT -->
<div id="quickview-wrapper">
    <!-- Modal -->
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    <!-- END Modal -->
</div>
<!-- END QUICKVIEW PRODUCT -->


<!-- jquery
		============================================ -->
<script src="/electronics/js/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS
		============================================ -->
<script src="/electronics/js/bootstrap.min.js"></script>
<!-- wow JS
		============================================ -->
<script src="/electronics/js/wow.min.js"></script>
<!-- meanmenu JS
		============================================ -->
<script src="/electronics/js/jquery.meanmenu.js"></script>
<!-- owl.carousel JS
		============================================ -->
<script src="/electronics/js/owl.carousel.min.js"></script>
<!-- scrollUp JS
		============================================ -->
<script src="/electronics/js/jquery.scrollUp.min.js"></script>
<!-- countdon.min JS
		============================================ -->
<script src="/electronics/js/countdon.min.js"></script>
<!-- jquery-price-slider js
		============================================ -->
<script src="/electronics/js/jquery-price-slider.js"></script>
<!-- Nivo slider js
		============================================ -->
<script src="/electronics/js/jquery.nivo.slider.js" type="text/javascript"></script>
<!-- plugins JS
		============================================ -->
<script src="/electronics/js/plugins.js"></script>
<!-- main JS
		============================================ -->
<script src="/electronics/js/main.js"></script>

<script src="/frontend/js/app.js"></script>

<script src="/frontend/ilya-birman-likely-2.3.1/likely.js"></script>

<script src="/sweetalert/docs/assets/sweetalert/sweetalert.min.js"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

@stack('scripts')

</body>
</html>
