@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$page->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                </div>

                @include('frontend.layouts.catalog')
                <div class="col-md-9">
                    <h1>{{ $page->getTranslation('title',$currentLocale) }}</h1>

                    <!-- Start Contact-Message -->
                    {!! $page->getTranslation('page_content',$currentLocale) !!}
                    <!-- End Contact-Message -->
                </div>
            </div>
        </div>
    </section>
@endsection