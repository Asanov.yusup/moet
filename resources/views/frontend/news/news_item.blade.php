@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        <li><a href="{{route('pages',['locale'=>$currentLocale, 'prefix' => 'news'])}}">Новости</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$news->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">

                <div class="col-12">
                    <div class="news-item padding-bottom">
                        <div class="row border-left">
                            <div class="col-md-3">
                                <img class="news-img"
                                     src="{{isset($news->mainImage) ? $news->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}">

                            </div>

                            <div class="col-md-9">
                                <h3>{{$news->getTranslation('title',$currentLocale)}}</h3>
                                <p>
                                    {!! $news->getTranslation('long_content',$currentLocale) !!}
                                </p>

                                <p class="pull-right">{{$news->created_at}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection