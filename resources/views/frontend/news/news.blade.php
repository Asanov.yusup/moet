@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$page->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    @foreach($news as $item)
                        <div class="news-item padding-bottom">
                            <div class="row border-left">
                                <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                    <img class="news-img col-md-3"
                                         src="{{isset($item->mainImage) ? $item->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}">
                                </a>
                                <div class="col-md-9">
                                    <h3>
                                        <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                           style="color: #474747">
                                            {{$item->getTranslation('title',$currentLocale)}}
                                        </a>
                                    </h3>
                                    <p>{{mb_substr($item->getTranslation('short_content',$currentLocale),0,200).'...'}}</p>
                                    <br>
                                    <p>
                                        <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                           class="news-detail pull-right">
                                            Подробнее
                                        </a>
                                    </p>
                                    <br>
                                    <p class="pull-right">{{\Carbon\Carbon::parse( $item->created_at)->format('d-m-Y')}}г.</p>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection