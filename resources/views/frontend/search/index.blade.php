@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <header class="page__header">
                        <ul class="page-menu breadcrumb">
                            <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                            <li><a href="#" onclick="return false">Результаты поиска</a></li>
                            <li class="active">
                                <a href="#" onclick="return false">
                                    {{htmlentities($searchQuery)}}
                                </a>
                            </li>
                        </ul>
                        <h1 class="page__header-title">Результаты поиска: {{htmlentities($searchQuery)}}</h1>
                    </header>

                    <hr>
                    <div class="page__content">
                        @if(count($data['categories']))
                            @foreach($data['categories'] as $category)
                                <p>
                                    <a style="color: #0c1923"
                                       href="{{route('show.catalog', ['locale' => $currentLocale, 'slug' => $category->slug])}}">
                                        {{ $category->getTranslation('name', $currentLocale) }}
                                    </a>
                                </p>
                            @endforeach
                        @endif
                    </div>
                    <div class="page__content">
                        @if(isset($data['products']) && count($data['products']))
                            @foreach($data['products'] as $product)
                                <p>
                                    <a style="color: #0c1923"
                                       href="{{route('show.product', ['locale' => $currentLocale, 'catalogSlug' => 'catalogSlug', 'slug' => $product->slug])}}">
                                        {{ $product->getTranslation('title', $currentLocale) }}
                                    </a>
{{--                                        {!!$product->snippet!!}--}}
                                </p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection