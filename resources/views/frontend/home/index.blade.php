@extends('frontend.layouts.master')

@section('content')
    <div class="category-slider-area">
        <div class="container">
            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9">
                    <div class="product-banner hidden-xs">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="banner" style="margin-bottom: 20px;">
                                    <a href="{{ route('home', ['locale'=>$currentLocale]) }}">
                                        <img src="{{ (isset($page->mainImage)) ? $page->mainImage->original_file_name : '/electronics/img/banner/12.jpg' }}"
                                             alt="{{ (isset($page->mainImage)) ? $page->mainImage->client_file_name : 'Product Banner' }} ">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="page-content">
        <div class="container padding-bottom">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div style="padding: 10px 10px; border: #eeeeee 1px solid">
                        <div class="small-product-area carosel-navigation">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="area-title">
                                        <h3 class="title-group gfont-1">Хиты продаж</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="active-bestseller sidebar">
                                    @foreach($hit_products as $key=>$val)
                                        <div class="col-xs-12">
                                            @foreach($val as $hit_product)
                                                <div class="single-product" style="padding: 5px 25px">
                                                    <div class="sale-off">
                                                        <span class="sale-percent">Хит</span>
                                                    </div>
                                                    <div class="label_new">
                                                        <span class="new">Хит</span>
                                                    </div>
                                                    <div class="product-img"
                                                         style="width: 100%; text-align: center; border: none;">
                                                        <img class="primary-img"
                                                             src="{{ (isset($hit_product->mainImage)) ? $hit_product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg' }}"
                                                             alt="{{ (isset($hit_product->mainImage)) ? $hit_product->mainImage->client_file_name : 'hit_product' }}">
                                                    </div>
                                                    <div class="product-description">
                                                        <h5>
                                                            <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=> $hit_product->categories->first()->slug,'slug'=> $hit_product->slug])}}">
                                                                {{strtoupper($hit_product->getTranslation('title',$currentLocale))}}</a>
                                                        </h5>
                                                        {{--<div class="product-button-2">--}}
                                                        {{--<a href="#"--}}
                                                        {{--data-url="{{ route('add.favorite', ['locale' => $currentLocale, 'type' => $productClass, 'id' => $hit_product->id]) }}"--}}
                                                        {{--data-toggle="tooltip"--}}
                                                        {{--class="{{ (in_array($hit_product->id, $favoriteData[$productClass])) ? 'isFavorite' : 'addFavorite' }}"--}}
                                                        {{--title="В избранное">--}}
                                                        {{--<i class="fa fa-heart-o"></i>--}}
                                                        {{--</a>--}}
                                                        {{--<a href="#"--}}
                                                        {{--data-url="{{ route('product.modal', ['locale' => $currentLocale, 'id' => $hit_product->id]) }}"--}}
                                                        {{--class="modal-view"--}}
                                                        {{--data-toggle="modal"--}}
                                                        {{--data-target="#productModal">--}}
                                                        {{--<i class="fa fa-search-plus"></i>--}}
                                                        {{--</a>--}}
                                                        {{--</div>--}}
                                                        {{--<p>--}}
                                                        {{--{{isset($hit_product->short_desc) ? (mb_substr($hit_product->getTranslation('short_desc',$currentLocale),0,60).'...') : ''}}--}}
                                                        {{--</p>--}}
                                                        <div class="price-box">
                                                            <span class="price">{{$hit_product->price}} сом</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="shop-blog-area sidebar">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="title-group gfont-1">Последние новости </h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="active-recent-posts carosel-circle">
                                    @foreach($news as $item)
                                        <div class="col-xs-12">
                                            <div class="single-recent-posts">
                                                <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                                    <div class="recent-posts-photo ">
                                                        <img src="{{(isset($item->mainImage)) ? $item->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}"
                                                             alt="{{isset($item->mainImage) ? $item->mainImage->client_file_name : 'Новости'}}">
                                                    </div>
                                                </a>
                                                <div class="recent-posts-text">
                                                    <h5>
                                                        <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                                           class="recent-posts-title">
                                                            {{$item->getTranslation('title',$currentLocale)}}
                                                        </a>
                                                    </h5>
                                                    <span class="recent-posts-date">{{$item->created_at}}</span>
                                                    <p class="posts-short-brif">{{mb_substr($item->getTranslation('short_content',$currentLocale),0,100).'...'}}</p>
                                                    <a href="{{route('frontend.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                                       class="posts-read-more">Читать далее ...</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-md-9 col-sm-9">
                    <div class="product-area">
                        @foreach($promotions as $promotion)
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="product-menu">
                                        <div class="product-title">
                                            <h3 class="title-group-2 gfont-1">{{$promotion->getTranslation('title',$currentLocale)}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="product carosel-navigation">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                @foreach($promotion->products as $product)
                                                    <div class="col-xs-12">
                                                        <div class="single-product">
                                                            @if($product->getOriginal('new') == 1)
                                                                <div class="sale-off">
                                                                    <span class="sale-percent">Новое</span>
                                                                </div>
                                                                <div class="label_new">
                                                                    <span class="new">Новое</span>
                                                                </div>
                                                            @elseif($product->getOriginal('hit') == 1)
                                                                <div class="sale-off">
                                                                    <span class="sale-percent">Хит</span>
                                                                </div>
                                                                <div class="label_new">
                                                                    <span class="new">Хит</span>
                                                                </div>
                                                            @endif
                                                            <div class="product-img">
                                                                <a href="{{ route('product.sale', ['locale' => $currentLocale, 'saleSlug' => $promotion->slug, 'slug' => $product->slug])}}">
                                                                    <img class="primary-img"
                                                                         src="{{ (isset($product->mainImage)) ? $product->mainImage->getThumb256Attribute() : '' }}"
                                                                         alt="{{ (isset($product->mainImage)) ? $product->mainImage->client_file_name : 'Product' }}">
                                                                    <img class="secondary-img"
                                                                         src="{{ (isset($product->mainImage)) ? $product->mainImage->getThumb256Attribute() : '' }}"
                                                                         alt="{{ (isset($product->mainImage)) ? $product->mainImage->client_file_name : 'Product' }}">
                                                                </a>
                                                            </div>
                                                            <div class="product-description margin-ceil-20">
                                                                <h5 class="text-center">
                                                                    <a href="#">{{ mb_strtoupper($product->getTranslation('title', $currentLocale)) }}</a>
                                                                </h5>
                                                                <div class="price-box text-center">
                                                                    <span class="price">{{ $product->price }} сом</span>
                                                                    {{--<span class="old-price">{{ $product->price }}--}}
                                                                    {{--сом</span>--}}
                                                                </div>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <button class="{{ (isset($cartData[$product->id]) ? '' : 'addToCart') }} product_{{ $product->id }}"
                                                                                data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}">
                                                                            <i class="fa fa-shopping-cart"></i>
                                                                            <span data-text-added="Добавлен">{{ (isset($cartData[$product->id]) ? 'Добавлен' : 'В корзину' ) }}</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="product-button-2">
                                                                        @if(isset($authCustomer))
                                                                            <a href="#"
                                                                               data-url="{{ route('add.favorite', ['locale' => $currentLocale, 'type' => $productClass, 'id' => $product->id]) }}"
                                                                               data-toggle="tooltip"
                                                                               class="{{ (in_array($product->id, $favoriteData[$productClass])) ? 'isFavorite' : 'addFavorite' }}"
                                                                               title="В избранное">
                                                                                <i class="fa fa-heart-o"></i>
                                                                            </a>
                                                                        @endif
                                                                        <a href="#"
                                                                           data-url="{{ route('product.modal', ['locale' => $currentLocale, 'id' => $product->id]) }}"
                                                                           class="modal-view"
                                                                           data-toggle="modal"
                                                                           data-target="#productModal">
                                                                            <i class="fa fa-search-plus"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection