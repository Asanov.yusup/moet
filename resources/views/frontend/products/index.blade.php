@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        @foreach($catalogAncestors as $ancestor)
                            <li>
                                <a href="{{route('show.catalog',['locale'=>$currentLocale, 'slug' => $ancestor->slug])}}">
                                    {{ $ancestor->getTranslation('name',$currentLocale) }}
                                </a>
                            </li>
                        @endforeach
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$catalog->getTranslation('name',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9 col-xs-12">
                    <!-- START PRODUCT-AREA -->
                    <div class="product-area">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Start Product-Menu -->
                                <div class="product-menu">
                                    <div class="product-title">
                                        <h3 class="title-group-3 gfont-1">{{$catalog->getTranslation('name',$currentLocale)}}</h3>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <!-- Start Product -->
                                <div class="product">
                                    <div class="tab-content">
                                        <!-- Start Product-->
                                        <div role="tabpanel" class="tab-pane fade in  active" id="display-1-2">
                                            <div class="row flexible">
                                                <!-- Start Single-Product -->
                                                @foreach($products as $product)
                                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                                        <div class="single-product">
                                                            @if($product->getOriginal('new') == 1)
                                                                <div class="label_new">
                                                                    <span class="new">новое</span>
                                                                </div>
                                                            @endif
                                                            @if($product->getOriginal('hit') == 1)
                                                                <div class="sale-off">
                                                                    <span class="sale-percent">хит</span>
                                                                </div>
                                                            @endif
                                                            <div class="product-img">
                                                                <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=>$catalog->slug,'slug'=>$product->slug])}}">
                                                                    <img class="primary-img"
                                                                         src="{{($product->mainImage->original_file_name) ? $product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}"
                                                                         alt="Product">
                                                                    <img class="secondary-img"
                                                                         src="{{$product->mainImage->original_file_name}}"
                                                                         alt="Product">
                                                                </a>
                                                            </div>
                                                            <div class="product-description">
                                                                <h5>
                                                                    <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=>$catalog->slug,'slug'=>$product->slug])}}"
                                                                       style="font-family:'Roboto'">
                                                                        {{$product->getTranslation('title',$currentLocale)}}
                                                                    </a>
                                                                </h5>
                                                                @if($product->short_desc)
                                                                    <p>
                                                                        {{--{{dd(strlen($product->getTranslation('short_desc',$currentLocale)))}}--}}
                                                                        @if((strlen($product->getTranslation('short_desc',$currentLocale))) > 79)
                                                                            {{substr($product->getTranslation('short_desc',$currentLocale),0,160).'...'}}
                                                                        @elseif(strlen($product->getTranslation('short_desc',$currentLocale)) < 80)
                                                                            {{--@else--}}
                                                                            {{$product->getTranslation('short_desc',$currentLocale)}}
                                                                        @endif
                                                                    </p>
                                                                @endif
                                                                <div class="price-box">
                                                                    @if(isset($product))<span class="price">{{$product->price}}
                                                                        сом</span>@endif
                                                                    {{--<span class="old-price">{{$product->price}}--}}
                                                                    {{--сом</span>--}}
                                                                </div>
                                                            </div>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <button class="{{ (isset($cartData[$product->id]) ? '' : 'addToCart') }} product_{{ $product->id }}"
                                                                                data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}">
                                                                            <i class="fa fa-shopping-cart"></i>
                                                                            <span data-text-added="Добавлен">{{ (isset($cartData[$product->id]) ? 'Добавлен' : 'В корзину' ) }}</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="product-button-2">
                                                                        @if(isset($authCustomer))
                                                                            <a href="#" style="color: #FFF"
                                                                               data-url="{{ route('add.favorite', ['locale' => $currentLocale, 'type' => $productClass, 'id' => $product->id]) }}"
                                                                               data-toggle="tooltip"
                                                                               class="{{ (in_array($product->id, $favoriteData[$productClass])) ? 'isFavorite' : 'addFavorite' }}"
                                                                               title="В избранное">
                                                                                <i class="fa fa-heart-o"></i>
                                                                            </a>
                                                                        @endif
                                                                        <a href="#" style="color: #FFF"
                                                                           data-url="{{ route('product.modal', ['locale' => $currentLocale, 'id' => $product->id]) }}"
                                                                           class="modal-view"
                                                                           data-toggle="modal"
                                                                           data-target="#productModal">
                                                                            <i class="fa fa-search-plus"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <!-- Start Pagination Area -->
                                            <div class="pagination-area">
                                                <div class="row">
                                                    <div class="col-xs-5">
                                                        <div class="pagination">
                                                            <ul>
                                                                @if($products->currentPage() != 1)
                                                                    <li>
                                                                        <a href="{{$products->previousPageUrl()}}"><</a>
                                                                    </li>
                                                                @endif
                                                                @if($products->currentPage() > 1)
                                                                    <li>
                                                                        <a href="?page={{$products->currentPage() - 1}}">{{$products->currentPage() - 1}}</a>
                                                                    </li>
                                                                @endif
                                                                <li class="active"><a
                                                                            href="#">{{$products->currentPage()}}</a>
                                                                </li>
                                                                @if($products->currentPage() != $products->lastPage())
                                                                    <li>
                                                                        <a href="{{$products->nextPageUrl()}}">{{$products->currentPage() + 1}}</a>
                                                                    </li>
                                                                @endif
                                                                @if($products->currentPage() != $products->lastPage())
                                                                    <li><a href="{{$products->nextPageUrl()}}">></a>
                                                                    </li>
                                                                @endif
                                                                @if($products->currentPage() != $products->lastPage())
                                                                    <li>
                                                                        <a href="@if($products->lastPage() != 1)
                                                                                ?page={{$products->lastPage()}}@else # @endif">...{{$products->lastPage()}}</a>
                                                                    </li>
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-7">
                                                        <div class="product-result">
                                                            {{--<span>Показано</span>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Pagination Area -->
                                        </div>
                                        <!-- End Product = TV -->
                                    </div>
                                </div>
                                <!-- End Product -->
                            </div>
                        </div>
                    </div>
                    <!-- END PRODUCT-AREA -->
                </div>
            </div>
        </div>
    </section>
@endsection