<div style="padding: 20px 20px">
    <div class="modal-product">
        <div class="product-images">
            <div class="main-image images">
                <img src="{{ (isset($product->mainImage)) ? $product->mainImage->original_file_name : '' }}"
                     alt="{{ (isset($product->mainImage)) ? $product->mainImage->client_file_name : 'Product' }}">
            </div>
        </div><!-- .product-images -->

        <div class="product-info">
            <h1>{{ $product->getTranslation('title', $currentLocale) }}</h1>
            <div class="price-box-3">
                <hr/>
                <div class="s-price-box">
                    <span class="new-price">{{ $product->price }} сом</span>
                    {{--<span class="old-price">$190.00</span>--}}
                </div>
                <hr/>
            </div>

            <div class="quick-desc">
                {!! $product->getTranslation('desc', $currentLocale) !!}
            </div>

            <div class="quick-add-to-cart">
                <div class="numbers-row">
                    <input type="number" id="french-hens" class="inputQty" name="qty"
                           value="{{ $cartData[$product->id]['qty'] ?? 3 }}"
                           data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}">
                </div>
                <button class="single_add_to_cart_button {{ (isset($cartData[$product->id]) ? '' : 'addToCart') }} product_{{ $product->id }}"
                        data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}">
                    <span data-text-added="Добавлен">{{ (isset($cartData[$product->id]) ? 'Добавлен' : 'В корзину' ) }}</span>
                </button>
            </div>

            <a href="{{ route('show.product', ['locale' => $currentLocale, 'catSlug' => $product->categories->first()->slug, 'slug' => $product->slug]) }}"
               class="see-all">
                Подробно
            </a>

            {{--<div class="social-sharing">--}}
            {{--<div class="widget widget_socialsharing_widget">--}}
            {{--<h3 class="widget-title-modal">Share this product</h3>--}}
            {{--<ul class="social-icons">--}}
            {{--<li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i--}}
            {{--class="fa fa-facebook"></i></a></li>--}}
            {{--<li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i--}}
            {{--class="fa fa-twitter"></i></a></li>--}}
            {{--<li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i--}}
            {{--class="fa fa-pinterest"></i></a></li>--}}
            {{--<li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i--}}
            {{--class="fa fa-google-plus"></i></a></li>--}}
            {{--<li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i--}}
            {{--class="fa fa-linkedin"></i></a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

</div>

