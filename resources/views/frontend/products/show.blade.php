@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container padding-bottom">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                        @if(isset($catalog->ancestors))
                            @foreach($catalog->ancestors as $ancestor)
                                <li>
                                    <a href="{{route('show.catalog',['locale'=>$currentLocale, 'slug' => $ancestor->slug])}}">
                                        {{ $ancestor->getTranslation('name',$currentLocale) }}
                                    </a>
                                </li>
                            @endforeach
                        @endif

                        {{--@if(isset($catalog))--}}
                            <li>
                                <a href="{{route('products.catalog',['locale'=>$currentLocale, 'slug' => $catalogSlug])}}">
                                    {{$product->categories->first()->getTranslation('name',$currentLocale)}}
                                </a>
                            </li>
                        {{--@elseif (isset($promotion))--}}
                            {{--<li>--}}
                                {{--<a href="{{route('products.catalog',['locale'=>$currentLocale, 'slug' => $catalogSlug])}}">--}}
                                    {{--{{$promotion->getTranslation('title',$currentLocale)}}--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        {{--@endif--}}

                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$product->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="toch-prond-area">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="clear"></div>
                                <div class="tab-content">
                                    <!-- Product = display-1-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-1">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="toch-photo">
                                                    <a data-fancybox="gallery"
                                                       href="{{isset($product->mainImage) ? $product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}">
                                                        <img src="{{isset($product->mainImage) ? $product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}"
                                                             {{--data-imagezoom="true"--}} alt="#"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- Start Toch-prond-Menu -->
                                <div class="toch-prond-menu">
                                    <ul role="tablist">
                                        <li role="presentation" class=" active">
                                            {{--<a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug'=>$product->category->slug,'slug'=>$product->slug])}}" role="tab" data-toggle="tab">--}}
                                            <img src="{{isset($product->mainImage) ? $product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}"
                                                 alt="{{isset($product->mainImage) ? $product->mainImage->client_file_name : 'Продукт'}}"/>
                                            {{--</a>--}}
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Toch-prond-Menu -->
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <h2 style="font-family: 'Roboto';"
                                    class="title-product"> {{$product->getTranslation('title',$currentLocale)}}</h2>
                                <div class="about-toch-prond">
                                    <hr/>
                                    <p class="short-description">
                                        {!! $product->getTranslation('desc', $currentLocale) !!}
                                    </p>
                                    <hr/>
                                    <span class="current-price">{{$product->price}} сом</span>
                                </div>
                                <div class="product-quantity">
                                    <span>@lang('purchase.quantity')</span>
                                    <input type="number" name="qty" class="inputQty"
                                           value="{{ (isset($cartData[$product->id])) ? $cartData[$product->id]['qty'] : 1 }}"
                                           data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}"
                                           data-price="{{$product->price}}"/>
                                    <button class="toch-button toch-add-cart {{ (isset($cartData[$product->id]) ? '' : 'addToCart') }} product_{{ $product->id }}"
                                            data-url="{{ route('purchase.add.cart', ['locale' => $currentLocale, 'productId' => $product->id]) }}">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span data-text-added="Добавлен">{{ (isset($cartData[$product->id]) ? 'Добавлен' : 'В корзину' ) }}</span>
                                    </button>
                                    {{--<button data-url="{{ route('add.favorite', ['locale' => $currentLocale, 'type' => $productClass, 'id' => $product->id]) }}"--}}
                                    {{--class="toch-button toch-wishlist">В избранное</button>--}}
                                    <hr/>
                                    <div class="likely likely-big" data-url="{{ Request::url() }}">
                                        {{--<div class="twitter">Твитнуть</div>--}}
                                        <div class="facebook"></div>
                                        <div class="vkontakte"></div>
                                        <div class="odnoklassniki"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-area">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <!-- Start Product-Menu -->
                                    <div class="product-menu">
                                        <div class="product-title">
                                            <h3 class="title-group-2 gfont-1">Похожие продукты</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Product-Menu -->
                            <div class="clear"></div>
                            <!-- Start Product -->
                            <div class="product carosel-navigation">
                                <div class="row">
                                    <div class="active-product-carosel">
                                        @if(isset($related_products) && $related_products->count() > 0)
                                            @foreach($related_products as $single_product)
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="{{route('show.product',['locale'=>$currentLocale,'catalogSlug' => $catalogSlug, 'slug' => $single_product->slug])}}">
                                                                <img class="primary-img"
                                                                     src="{{isset($single_product->mainImage) ? $single_product->mainImage->original_file_name : '/app/images/no_image_placeholder.jpg'}}"
                                                                     alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5>
                                                                <a href="#">{{$single_product->getTranslation('title',$currentLocale)}}</a>
                                                            </h5>
                                                            <div class="price-box">
                                                                <span class="price">{{$single_product->price}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
