@extends('frontend.layouts.master')

@section('content')
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="page-menu breadcrumb">
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('master.home')</a></li>
                        <li class="active">
                            <a href="#" onclick="return false">
                                {{$page->getTranslation('title',$currentLocale)}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                @include('frontend.layouts.catalog')
                <div class="col-md-9">
                    <!-- Start Contact-Message -->
                    <div>
                        {!! $page->getTranslation('page_content',$currentLocale) !!}
                    </div>
                    <div class="contact-message">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @break
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <fieldset>
                        <legend>@lang('contacts.contact_form')</legend>
                        <form id="feedback" method="post" novalidate
                              action="{{route('frontend.feedback.store', ['locale'=>$currentLocale])}}">
                            {{ csrf_field() }}
                            <div class="form-group form-horizontal">
                                <div class="row">
                                    <label class="col-sm-2 control-label"><sup>*</sup>@lang('purchase.name')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name"
                                               value="{{ old('name') }}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-horizontal">
                                <div class="row">
                                    <label class="col-sm-2 control-label"><sup>*</sup>@lang('contacts.email_address')
                                    </label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="email" name="email"
                                               value="{{ old('email') }}" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-horizontal">
                                <div class="row">
                                    <label class="col-sm-2 control-label"><sup>*</sup>@lang('contacts.enquiry')
                                    </label>
                                    <div class="col-sm-10">
                                            <textarea class="form-control" rows="10" name="message"
                                                      required>{{ old('message') }}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="buttons pull-right">
                                <div class="g-recaptcha"
                                     data-sitekey="6Lfe1YcUAAAAAOMyY3pCh_00v2We-L2Z2HAQioVR"></div>
                                <input style="width: 302px" class="btn btn-primary" type="submit"
                                       value="@lang('contacts.submit')"
                                       name="butTest"/>
                            </div>

                        </form>


                    </fieldset>
                </div>
                <!-- End Contact-Message -->
            </div>
        </div>
        </div>
    </section>
    <br>
@endsection