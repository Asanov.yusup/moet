<div>
  <img style="max-width: 100%; " id="image" src="{{asset('storage/uploaded_images/' . $media->getOriginal('original_file_name'))}}">
</div>
<div style="margin-top: 20px">
  <form action="{{route('admin.media.post.crop', ['id' => $media->id])}}" method="post" class="ajax" data-ui-block-type="element" data-ui-block-element="#largeModal .modal-body">
    <input type="hidden" id="width" name="width" value="0">
    <input type="hidden" id="height" name="height" value="0">
    <input type="hidden" id="offset_x" name="offset_x" value="0">
    <input type="hidden" id="offset_y" name="offset_y" value="0">
    <button type="submit" class="btn btn-info btn-sm pull-right">Сохранить</button>
  </form>

</div>



<script>
    var $image = $('#image');

    $image.cropper({
        aspectRatio: 16/11,
        mouseWheelZoom: false,
        zoomable: false,

        crop: function(event) {
            $("#width").val(event.detail.width);
            $("#height").val(event.detail.height);
            $("#offset_x").val(event.detail.x);
            $("#offset_y").val(event.detail.y);
        },

    });


</script>