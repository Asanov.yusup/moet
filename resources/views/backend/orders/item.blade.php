<tr class="row-{{$order->id}}">
    <td class="text-center">{{$order->id}}</td>
    <td>
        {{$order->name}}
        <br>

        <span style="font-size: 10px">
        {{$order->phone}}
        </span>
    </td>
    <td>
        @if(isset($order->products))
            @foreach($order->products as $key => $product)
                {{ $product->title }} - (<strong style="color: #c20000">{{$product->pivot->qty}} шт.</strong>)
                <br>
            @endforeach
        @endif
    </td>
    <td>{{$order->total}}</td>
    <td>{{$order->comment}}</td>
    <td>{{$order->created_at}}</td>
</tr>