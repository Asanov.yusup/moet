<tr class="row-{{$item->id}}">
    <td class="text-center">{{$item->id}}</td>
    <td>{{$item->first_name}} {{$item->last_name}} {{$item->middle_name}}</td>
    <td>{{$item->email}}</td>
    <td>{{$item->phone}}</td>
    <td class="text-center">{!! $item->active !!}</td>
    <td class="text-center">
        <a href="{{ route('admin.users.customers.edit', ['id' => $item->id]) }}" data-type="modal"
           data-modal="#superLargeModal"
           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
           data-toggle="m-tooltip" data-placement="top" title="Редактировать продукт">
            <i class="fa fa-pencil-square-o"></i>
        </a>
    </td>
</tr>
