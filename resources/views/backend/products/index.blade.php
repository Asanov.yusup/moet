@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    {{--<li class="m-portlet__nav-item">--}}
                        {{--<a href="{{route('admin.products.sort.list')}}" data-type="modal" data-modal="#regularModal"--}}
                           {{--class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"--}}
                           {{--data-toggle="m-tooltip" data-placement="top" title="Настроить порядок">--}}
                            {{--<i class="la la-sort-amount-desc"></i>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.products.create') }}" data-type="modal" data-modal="#superLargeModal"
                           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="Создать продукт">
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <div class="m-portlet__body">
                    <form action="{{route('admin.products.list')}}" method="get"
                          class="filter-form" data-table="#productsTable">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="filter.title">По названию</label>
                                    <input type="text" class="form-control" id="filter.title"
                                           name="filter[title]" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="filter.site_display">Отображенные</label>
                                    <select name="filter[site_display]" id="filter.site_display" class="form-control">
                                        <option value="all">Все</option>
                                        <option value="yes">Да</option>
                                        <option value="no">Нет</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-sm btn-success">Фильтр</button>
                            <a href="{{route('admin.products')}}" class="btn btn-sm btn-info">Сбросить</a>
                        </div>
                    </form>
                </div>

                <div class="box">
                    <table class="table table-bordered m-table ajax-content" id="productsTable"
                           data-ajax-content-url="{{ route('admin.products.list') }}">
                        <thead>
                        <tr>
                            <th class="text-center" width="50">Id</th>
                            <th class="text-center" width="150">Фото</th>
                            <th class="text-center">Название</th>
                            <th class="text-center">Описание</th>
                            <th class="text-center" width="200">Категория</th>
                            <th class="text-center" width="80">Акции</th>
                            <th class="text-center" width="80">Количество</th>
                            <th class="text-center" width="80">Цена</th>
                            <th class="text-center" width="50"><i class="fa fa-power-off"></i></th>
                            <th class="text-center" width="80"><i class="fa fa-bars" aria-hidden="true"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="pagination_placeholder" data-table-id="productsTable"></div>
                </div>

            </div>
        </div>
        <!--end::Section-->
    </div>
@endsection

@push('modules')
@endpush