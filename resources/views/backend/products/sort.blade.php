<form action="{{ $formAction }}" method="post" class="ajax" data-block-element=".modal-body" id="sortForm">
{{--@foreach($zones as $zone)--}}
    {{--<h4>Зоны</h4>--}}
    <table class="table table-bordered">
    <thead>
        <tr>
            <th>Продукты</th>
            <th class="text-center" width="40"><i class="la la-sort-amount-desc"></i></th>
        </tr>
    </thead>
    <tbody>

        @foreach($products as $product)
            <tr>
                <td>{{$product->getTranslation('title', 'ru')}}</td>
                <td>
                    <input type="text" style="width: 60px; text-align: center" onclick="this.select()" name="sort[{{$product->id}}]" value="{{$product->sort}}" >
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
{{--@endforeach--}}
    <button type="submit" class="btn btn-success">Сохранить</button>
</form>