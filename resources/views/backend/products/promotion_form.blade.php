<form action="{{ $formAction }}" method="post" class="ajax promotion-form" data-ui-block-type="element"
      data-ui-block-element="#regularModal .modal-body" id="ajaxForm">

    <input name="_token" type="hidden" value="{{ csrf_token() }}">
    <div class="row">
    @foreach($promotions as $promotion)
        <div class="form-group col-md-6">
            <input type="checkbox" class="promotionCheckbox" name="promotion" value="{{$promotion->id}}"
                   @if(isset( $ids[$promotion->id])) @if(in_array($product->id, $ids[$promotion->id])) checked="checked" @endif @endif>
            <label for="promotion">{{$promotion->title}}</label>
        </div>
    @endforeach
    </div>
</form>
<script>
    $('.promotionCheckbox').change(function () {
        if(this.checked) {
        var submitVal = $(this).val();
        $('.promotion-form').append("<input type='hidden' name='promotionId' value='"+
            submitVal+"' />");
        $('.promotion-form').append("<input type='hidden' name='checked' value='"+
            1+"' />");
            $('.promotion-form').submit();
        }else{
            var submitVal = $(this).val();
            $('.promotion-form').append("<input type='hidden' name='promotionId' value='"+
                submitVal+"' />");
            $('.promotion-form').submit();
        }
    });
</script>
