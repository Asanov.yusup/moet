<tr class="row-{{ $item->id }}">
    <td style="text-align: center">{{ $item->id }}</td>
    <td>{{ $item->category->getTranslation('name', 'ru') }}</td>
    <td>{{ $item->getTranslation('title', 'ru') }}</td>
    <td class="text-center">{{ date('d.m.Y', strtotime($item->created_at)) }}</td>
    <td style="text-align: center">
        <a href="{{ route('admin.content.news.edit', ['itemId' => $item->id ]) }}" class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>

        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить новости"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.content.news.destroy', ['itemId' => $item->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>