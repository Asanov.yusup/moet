@extends('backend.layouts.master')
@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-section">
            <div class="m-section__content box">
                <table class="table table-bordered m-table ajax-content" id="ajaxTable"
                       data-ajax-content-url="{{ route('feedback.list') }}">
                    <thead>
                    <tr>
                        <th class="text-center" width="50">Id</th>
                        <th class="text-center" width="150">Имя</th>
                        <th class="text-center" width="150">Email</th>
                        <th class="text-center">Сообщение</th>
                        <th class="text-center" width="130">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="pagination_placeholder" data-table-id="ajaxTable"></div>
            </div>
        </div>
    </div>
@endsection
@push('modules')
    {{--<script src="/app/js/modules/userManagement.js"></script>--}}
@endpush


