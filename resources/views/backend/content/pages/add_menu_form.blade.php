<form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
      data-ui-block-element="#superLargeModal .modal-body" id="ajaxForm">
    <div class="form-group">

        <p>Страница {{ $page->getTranslation('title', 'ru') }}</p>
        @foreach($menusArray as $menu)
            <br>
            <label for="category_id_{{ $menu['rootMenu']->id }}">{{ $menu['rootMenu']->getTranslation('name', 'ru') }}</label>

            @if (is_array($menu['children']))
                <select id="category_id_{{ $menu['rootMenu']->id }}" name="category_id[{{ $menu['rootMenu']->id }}]"
                        class="form-control">
                    <option value="">Не добавлять</option>
                    <option value="{{ $menu['rootMenu']->id }}">Корневая</option>
                    @foreach($menu['children'] as $key => $categoryForSelect)
                        <option value="{{ $key }}"
                                @if(isset($page->parent_id) && $page->category_id == $key) selected @endif>
                            -{{ $categoryForSelect }}
                        </option>
                    @endforeach
                </select>
            @else
                <p>добавлен
                    <a href="{{route('admin.settings.menu.item.view', ['id' => $menu['rootMenu']->id ]) }}">
                        Настроить
                    </a>
                </p>
            @endif

        @endforeach
    </div>

    <div class="form-group  col-md-12">
        <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
    </div>
</form>