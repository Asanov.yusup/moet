@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>

            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.promotions') }}"
                           class="m-portlet__nav-link m-portlet__nav-link--icon " data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="К списку категорий продуктов"><i
                                    class="la la-arrow-circle-left"></i></a>
                    </li>
                </ul>
            </div>
        </div>

        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element" data-ui-block-element=".m-portlet__body" id="ajaxForm">
                                {{--<div class="row">--}}
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">

                                <ul class="nav nav-tabs " role="tablist">
                                    @foreach(config('project.locales') as $count => $locale)
                                        <li role="presentation" class="nav-item">
                                            <a class="@if($count == 0) active @endif nav-link" href="#tab-{{ $locale }}"
                                               aria-controls="#tab-{{ $count }}" role="tab"
                                               data-toggle="tab">{{ $locale }}</a>
                                        </li>
                                    @endforeach
                                </ul>

                                <div class="tab-content">
                                    @foreach(config('project.locales') as $count => $locale)
                                        <div role="tabpanel" class="tab-pane @if($count == 0)  active  @endif "
                                             id="tab-{{ $locale }}">
                                            <div class="form-group">
                                                <label for="title.{{ $locale }}">Заголовок *</label>
                                                <input type="text" class="form-control" id="title.{{ $locale }}"
                                                       name="title[{{ $locale }}]"
                                                       @if(isset($product_category)) value="{{ $product_category->getTranslation('title', $locale) }}" @endif>
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group">
                                                <label for="desc.{{ $locale }}">Описание категории</label>
                                                <textarea rows="4" class="form-control editor" id="desc.{{ $locale }}"
                                                          name="desc[{{ $locale }}]">@if(isset($product_category)){{$product_category->getTranslation('desc', $locale)}}@endif</textarea>
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="form-group ">
                                                <label for="meta_title.{{$locale}}">Meta title ({{$locale}})</label>
                                                <input type="text" id="meta_title.{{$locale}}" name="meta_title[{{$locale}}]"
                                                       class="form-control m-input"
                                                       @if(isset($product_category)) value="{{ $product_category->getTranslation('meta_title', $locale) }}" @endif>
                                            </div>

                                            <div class="form-group ">
                                                <label for="meta_keywords.{{$locale}}">Meta keywords ({{$locale}})</label>
                                                <input type="text" id="meta_keywords.{{$locale}}" name="meta_keywords[{{$locale}}]"
                                                       class="form-control m-input"
                                                       @if(isset($product_category)) value="{{ $product_category->getTranslation('meta_keywords', $locale) }}" @endif>
                                            </div>

                                            <div class="form-group ">
                                                <label for="meta_description.{{$locale}}">Meta description ({{$locale}})</label>
                                                <input type="text" id="meta_description.{{$locale}}" name="meta_description[{{$locale}}]"
                                                       class="form-control m-input"
                                                       @if(isset($product_category)) value="{{ $product_category->getTranslation('meta_description', $locale) }}" @endif>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="m-checkbox-list" style="margin-bottom: 20px; margin-top: 20px;">
                                    <label class="m-checkbox">
                                        <input type="checkbox" name="site_display"
                                               @if(isset($product_category) && $product_category->getOriginal('site_display')) checked @endif>
                                        Отображать на сайте
                                        <span></span>
                                    </label>
                                </div>
                                <div class="form-group ">
                                    <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
                                </div>
                                {{--</div>--}}
                            </form>
                        </div>

                        {{--@if(!$create)--}}
                            {{--<div class="col-md-4">--}}
                                {{--<h4>Главное изображение</h4>--}}
                                {{--<div id="mainImagePlaceholder">--}}
                                    {{--@include('backend.products.partials.main_image')--}}

                                {{--</div>--}}

                                {{--@if(isset($mainImageUploadUrl))--}}
                                    {{--<form class="ajax" method="post" action="{{$mainImageUploadUrl}}" id="mainImageForm" data-ui-block-type="element" data-ui-block-element=".m-portlet__body">--}}
                                        {{--<input type="file" class="form-control hiddenInputFile" id="mainImageInput" name="mainImage" accept="image/*" style="display: none">--}}
                                    {{--</form>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>


@endsection

@push('css')
    {{--<link href="/app/js/vendors/cropperjs/dist/cropper.min.css" rel="stylesheet" type="text/css"/>--}}
@endpush

@push('modules')
    {{--<script src="/app/js/vendors/cropperjs/dist/cropper.min.js"></script>--}}
    {{--<script src="/app/js/vendors/jquery-cropper/dist/jquery-cropper.min.js"></script>--}}
@endpush