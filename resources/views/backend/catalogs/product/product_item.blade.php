<tr class="row-{{$product->id }}">

    <td style="text-align: center">{{ $product->id }}</td>
    <td class="text-center">
        @if(isset($product->mainImage))
            <img style="max-width:100px"
                 src="{{asset('storage/uploaded_images/128_' . $product->mainImage->getOriginal('original_file_name'))}}">
        @endif
    </td>
    <td align="auto">{{ $product->title }}</td>
    <td align="auto">{{ mb_substr($product->short_desc,0,50).'...' }}</td>
    <td align="auto">
        <input type="number" name="position[{{$product->id}}][position]" value="{{ $product->pivot->position }}">
        <br><br>
        @if ($loop->last)
            <button type="submit"> Сохранить позиции</button>
        @endif
    </td>
</tr>