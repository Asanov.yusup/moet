<div class="row">
    <div class="col-md-12 ">
        <form action="{{ $formAction }}" method="post" class="ajax" data-ui-block-type="element"
              data-ui-block-element="#largeModal .modal-body" id="ajaxForm">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <ul class="nav nav-tabs " role="tablist">
                @foreach(config('project.locales') as $locale)
                    <li role="presentation" class="nav-item">
                        <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                           href="#tab-{{ $locale }}">
                            {{ $locale }}
                        </a>
                    </li>
                @endforeach
            </ul>


            {{--@if(!isset($catalog))--}}
                {{--<div class="form-group">--}}
                    {{--<label for="parent_id">Родитель</label>--}}
                    {{--<select name="parent_id" class="form-control">--}}
                        {{--<option value="">Корень</option>--}}
                        {{--@foreach($catalogs as $id => $catalogItem)--}}
                            {{--<option value="{{$id}}"--}}
                                    {{--@if(isset($section) && $id == $section->parent_id) selected @endif>{{$catalogItem}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--@endif--}}
            <input type="hidden" name="parentId" value="{{ $parentId }}">
            <div class="tab-content">

                @foreach(config('project.locales') as $locale)
                    <div role="tabpanel" class="tab-pane @if($loop->first) active @endif"
                         id="tab-{{ $locale }}">
                        <div class="form-group  ">
                            <label for="name.{{ $locale }}">Заголовок ({{ $locale }})*</label>
                            <input type="text" class="form-control" id="name.{{ $locale }}" name="name[{{ $locale }}]"
                                   @if(isset($catalog)) value="{{ $catalog->getTranslation('name', $locale) }}"
                                   @endif autocomplete="off">
                            <p class="help-block"></p>
                        </div>

                        <div class="form-group ">
                            <label for="meta_title.{{$locale}}">Meta title ({{$locale}})</label>
                            <input type="text" id="meta_title.{{$locale}}" name="meta_title[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($catalog)) value="{{ $catalog->getTranslation('meta_title', $locale) }}" @endif>
                        </div>
                        <div class="form-group ">
                            <label for="meta_description.{{$locale}}">Meta description ({{$locale}})</label>
                            <input type="text" id="meta_description.{{$locale}}" name="meta_description[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($catalog)) value="{{ $catalog->getTranslation('meta_description', $locale) }}" @endif>
                        </div>
                        <div class="form-group ">
                            <label for="meta_keywords.{{$locale}}">Meta keywords ({{$locale}})</label>
                            <input type="text" id="meta_keywords.{{$locale}}" name="meta_keywords[{{$locale}}]"
                                   class="form-control m-input"
                                   @if(isset($catalog)) value="{{ $catalog->getTranslation('meta_keywords', $locale) }}" @endif>
                        </div>

                        {{--<div class="form-group ">--}}
                        {{--<label for="meta_description.{{$locale}}">Meta description ({{$locale}})</label>--}}
                        {{--<textarea rows="5" class="form-control" id="meta_description.{{$locale}}" name="meta_description[{{$locale}}]">@if(isset($section)){{$section->getTranslation('meta_description', $locale)}}@endif</textarea>--}}
                        {{--</div>--}}
                    </div>
                @endforeach
            </div>


            {{--<div class="form-group">--}}
            {{--<label for="type">Поведение</label>--}}
            {{--<select name="type" class="form-control">--}}
            {{--<option value="">По умолчанию</option>--}}
            {{--<option value="category" @if(isset($section) && $section->type == 'category') selected @endif>Категория</option>--}}
            {{--<option value="section" @if(isset($section) && $section->type == 'section') selected @endif>Переход в раздел</option>--}}
            {{--</select>--}}
            {{--</div>--}}

            @if(isset($catalog) && isset($catalog->medias))
                <div class="form-group">
                    <img class="img-thumbnail" src="{{ $catalog->medias->original_file_name }}" alt="{{$catalog->medias->original_file_name}}">
                </div>
            @endif

            <div class="form-group">
                <label for="image">Изображение</label>
                <input type="file" class="form-control" name="image" accept="image/*">
            </div>

            <div class="form-group ">
                <button type="submit" class="btn btn-success">{{  $buttonText }} </button>
            </div>
        </form>
    </div>

</div>