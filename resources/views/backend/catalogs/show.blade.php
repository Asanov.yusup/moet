@extends('backend.layouts.master')
@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">

                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.catalog.create', ['parentId' => $catalog->id]) }}"
                           data-type="modal"
                           data-modal="#largeModal"
                           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="Создать каталог"><i class="la la-plus-square-o"></i></a>
                    </li>

                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.catalog.edit', ['catalogId' => $catalog->id]) }}"
                           data-type="modal"
                           data-modal="#largeModal"
                           class="m-portlet__nav-link m-portlet__nav-link--icon handle-click" data-container="body"
                           data-toggle="m-tooltip" data-placement="top" title="Редактировать каталог"><i class="la la-edit"></i></a>
                    </li>


                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin.catalog.delete', ['catalogId' => $catalog->id]) }}"
                           class="m-portlet__nav-link m-portlet__nav-link--icon delete-catalog-item"
                           data-toggle="m-tooltip" data-placement="top" title="Удалить каталог">
                            <i class="la la-trash"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <!--begin::Section-->
        <div class="m-section">
            <div class="row">
                <div class="col-8">
                    <div class="m-section__content" id="sectionsPlaceholder">
                        <div class="tree-data" style="display: none">
                            {!! $catalogs !!}
                        </div>

                    </div>
                </div>
                <div class="col-4">
                    <div class="m-section__content">
                        @if (isset($catalog->medias))
                            <img src="{{ $catalog->medias->getThumb128Attribute('original_file_name') }}" alt="{{ $catalog->medias->original_file_name }}">
                        @endif
                    </div>
                </div>
            </div>


        </div>
        <!--end::Section-->
    </div>
@endsection

@push('modules')
<script src="/app/js/modules/catalogsManagements.js"></script>
@endpush

