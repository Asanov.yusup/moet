<?php

return [
    'cart' => 'Араба',
    'account' => 'Менин аккаунт',
    'sign_in' => 'Кирүү',
    'search' => 'Издөө',
    'home' => 'Негизги',
    'about_us' => 'Биз жөнүндө',
    'news' => 'Жаңылыктар',
    'bestseller_products' => 'Соода хиттер',
    'new_products' => 'Жаңы буюмдар',
    'contacts' => 'Байланыш',
];