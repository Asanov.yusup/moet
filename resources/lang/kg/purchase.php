<?php
return [
    'home' => 'Башкы бетке',
    'shopping_cart' => 'Араба',
    'item' => 'Товар',
    'price' => 'Баасы',
    'quantity' => 'Жалпы сан',
    'total' => 'Баардыгы',
    'what_would_you_like_to_do_next' => 'What would you like to do next?',
    'enter_your_contact_info' =>'Сиздин байланыш маалыматын киргизип алыңыз',
    'shipping' => 'Жеткирүү',
    'enter_your_address' => 'Сиздин даректи киргизип алыңыз',
    'checkout' => 'Буйрутма түзүү',
    'continue_shopping' => 'Шопинг улантуу',
    'make_purchase' => 'Тапшырык түзүү',
    'please_check' => 'Сураныч, товарлардын санын текшерип жана талааларды толтуруңуз',
    'total_cart_price' => 'Товардын жалпы суммасы',
    'delivery' => 'Жеткирүү',
    'count' => 'Эсептөө',
    'name' => 'Сиздин атыңыз',
    'phone' => 'Сиздин телефон',
    'region' => 'Аймакты тандаңыз',
    'city' => 'Шаарды тандаңыз'
];