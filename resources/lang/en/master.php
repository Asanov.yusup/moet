<?php

return [
    'cart' => 'Cart',
    'account' => 'My account',
    'sign_in' => 'Sign in',
    'search' => 'Search',
    'home' => 'Home',
    'about_us' => 'About Us',
    'news' => 'News',
    'bestseller_products' => 'Bestseller Products',
    'new_products' => 'New Products',
    'contacts' => 'Contacts',
];