<?php
return [
    'catalog' => 'Catalog',
    'categories' => 'Categories',
    'new_items' => 'New Items',
    'to_cart' => 'Add to cart',
    'recommended_items' => 'Recommended items'
];