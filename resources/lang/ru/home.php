<?php
return [
    'catalog' => 'Каталог',
    'categories' => 'Категории',
    'new_items' => 'Новые товары',
    'to_cart' => 'Добавить в корзину',
    'recommended_items' => 'Рекомендуемые товары'
];