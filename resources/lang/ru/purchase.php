<?php
return [
    'home' => 'Домой',
    'shopping_cart' => 'Корзина',
    'item' => 'Товар',
    'price' => 'Цена',
    'quantity' => 'Количество',
    'total' => 'Всего',
    'what_would_you_like_to_do_next' => 'Что бы вы хотели сделать дальше?',
    'enter_your_contact_info' =>'Введите контактные данные',
    'shipping' => 'Доставка',
    'enter_your_address' => 'Введите свой адрес',
    'checkout' => 'Оформить заказ',
    'continue_shopping' => 'Продолжить шопинг',
    'make_purchase' => 'Оформить заказ',
    'please_check' => 'Пожалуйста проверьте количество товаров и заполните поля',
    'total_cart_price' => 'Общая сумма корзины',
    'delivery' => 'Доставка',
    'count' => 'Посчитать',
    'name' => 'Ваше имя',
    'phone' => 'Ваш телефон',
    'region' => 'Выберите регион',
    'city' => 'Выберите город'
];