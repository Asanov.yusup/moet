import Vue from 'vue';
import VueResource from 'vue-resource';
import { store } from '../main/store';
Vue.use(VueResource);

class ExternalApi extends Vue {
    constructor(){
        super();
        this.$store = store;
    }

    before(request) {

        if (this.previousRequest) {
            this.previousRequest.abort();
        }

        this.previousRequest = request;
    }

    getSearchResult(text){
        this.$store.dispatch('changeStoreRequestIsBeingSent', true);
        return this.$http.get('search?q=' + text, { before : this.before });
    }

}

export let externalApi = new ExternalApi();
