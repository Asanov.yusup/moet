<?php

namespace App\Providers;

use App\Services\Sphinx\Engine\SphinxSearchEngine;
use Illuminate\Support\ServiceProvider;
use App\Services\JSONApiResponse\JSONApiResponse;
use Laravel\Scout\EngineManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        resolve(EngineManager::class)->extend('sphinx', function () {
            return new SphinxSearchEngine;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Обработчик ответов в формате json
        $this->app->singleton('JSON', function()
        {
            return new JSONApiResponse();
        });
    }
}
