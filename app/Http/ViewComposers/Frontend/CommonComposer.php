<?php

namespace App\Http\ViewComposers\Frontend;

use App\Models\Favorite;
use App\Models\Products;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

// models
use App\Models\Category;

//use App\Models\Segment;

class CommonComposer
{

    private $request;
    private $category;
    /**
     * @var Favorite
     */
    private $favorite;
    /**
     * @var Products
     */
    private $products;


    public function __construct(HttpRequest $request, Category $category, Favorite $favorite, Products $products)
    {
        $this->request = $request;
        $this->category = $category;

        $this->favorite = $favorite;
        $this->products = $products;
    }

    public function compose(View $view)
    {

        session()->start();
        if (!session()->has('productIds')) {
            session()->put('productIds', []);
        }

        $cartData = session()->get('productIds');

        $currentLocale = $this->getCurrentLocale();

        $user = Auth::guard('customers')->user();

        $headerMenuTree = '';
        $rootMenus = $this->category->where('owner', 'menus')->whereIsRoot()->get();
        foreach ($rootMenus as $menu) {
            if ($menu->getTranslation('name', 'en') == 'header') {
                $headerMenuTree = $this->category->descendantsAndSelf($menu->id)->toTree()->first();
            }
        }

        $catalogs = $this->category->with('medias')->where('owner', 'catalog')->defaultOrder()->get()->toTree();

        $productClass = get_class($this->products);
        $favoriteData = [$productClass => []];
        if ($user) {
            $customerFavorite = $this->favorite->where('customer_id', $user->id)->get();
            foreach ($customerFavorite as $favorite){
                $favoriteData[$favorite->favoritesable_type][] = $favorite->favoritesable_id;
            }
        }


        $view->with('authCustomer', $user);
        $view->with('currentLocale', $currentLocale);
        $view->with('headerMenu', $headerMenuTree);
        $view->with('catalogs', $catalogs);
        $view->with('cartData', $cartData);
        $view->with('favoriteData', $favoriteData);
        $view->with('productClass', $productClass);
    }

    public function getCurrentLocale()
    {
        if (in_array($this->request->segment(1), config('project.locales'))) {
            return $this->request->segment(1);
        }

        return config('project.default_locale');
    }
}