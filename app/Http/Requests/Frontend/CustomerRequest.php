<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\MessagesTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CustomerRequest extends FormRequest
{
    use MessagesTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "last_name" => "required",
            "first_name" => "required",
            "phone" => "required",
            'email' => 'required|email|unique:customers,email,' . Auth::guard('customers')->user()->id,
        ];
    }
}
