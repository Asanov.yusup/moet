<?php namespace App\Http\Controllers;

use App\Mail\Customer\ChangePasswordEmail;
use App\Models\Customer;
use Illuminate\Support\Facades\Mail;
use App\Mail\Customer\ConfirmationEmail;

trait CustomerTrait {

    public function registerCustomer(array $data)
    {
        return $this->customer->create($data);
    }

    public function sendConfirmMail(Customer $customer, $locale, $password = null)
    {
        $params = new \stdClass;
        $params->locale = $locale;
        $params->password = $password;

        Mail::to($customer->email)->queue(new ConfirmationEmail($customer, $params));
    }

    public function changePasswordMail(Customer $customer, $locale)
    {
        $params = new \stdClass;
        $params->locale = $locale;

        Mail::to($customer->email)->queue(new ChangePasswordEmail($customer, $params));
    }
}