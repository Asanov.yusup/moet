<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
     * @var Feedback
     */
    private $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    public function index()
    {
        return view('backend.content.feedback.index', [
            'title' => 'Список писем',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getList(Request $request)
    {
        $feedbacks = $this->feedback->orderBy('created_at', 'desc')->paginate(25);

        return response()->json([
            'tableData' => view('backend.content.feedback.list', [
                'feedbacks' => $feedbacks,
                'filters' => $request->all()
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $feedbacks->appends($feedbacks->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function view(Request $request, $messageId)
    {
        $feedback = $this->feedback->find($messageId);

        if ($feedback->status == 1) {
            $feedback->status = 0;
        }
        $feedback->save();
        return view('backend.content.feedback.feedback_item', ['title' => $feedback->name, 'feedback' => $feedback]);
    }
}
