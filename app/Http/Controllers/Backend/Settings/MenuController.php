<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\FormatTrait;
use App\Http\Controllers\ResponseTrait;

use App\Http\Requests\Backend\MenuRequest;
use App\Models\Category;
use App\Models\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    use ResponseTrait;
    use FormatTrait;

    private $menu;
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $rootMenus = $this->category->where('owner', 'menus')->whereIsRoot()->get();

        return view('backend.settings.menu.index', [
            'title' => 'Список Меню',
            'menus' => $rootMenus,

        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create()
    {
        $data = [
            'modalTitle' => 'Создание меню',
            'modalContent' => view('backend.settings.menu.form', [
                'formAction' => route('admin.settings.menu.store'),
                'buttonText' => 'Создать',
            ])->render()
        ];

        return $this->responseJson($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->category->create([
            'name' => [
                'ru' => $request->input('name'),
            ],
            'owner' => 'menus',
        ]);

        $rootMenus = $this->category->where('owner', 'menus')->whereIsRoot()->get();

        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#regularModal',
            'blockId' => '#menusList',
            'blockData' => view('backend.settings.menu.list', [
                'menus' => $rootMenus
            ])->render(),
            'success' => 'Меню добавлено'
        ]);
    }

    /**
     * @param $menuId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($menuId)
    {
        $menu = $this->category->find($menuId);

        $data = [
            'modalTitle' => 'Редактирование меню',
            'modalContent' => view('backend.settings.menu.form', [
                'menu' => $menu,
                'formAction' => route('admin.settings.menu.update', [
                    'id' => $menu->id,
                ]),
                'buttonText' => 'Сохранить',

            ])->render()];

        return $this->responseJson($data);
    }

    /**
     * @param Request $request
     * @param $menuId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(Request $request, $menuId)
    {
        $menu = $this->category->find($menuId);
        $menu->name = ['ru' => $request->input('name')];
        $menu->save();

        $rootMenus = $this->category->where('owner', 'menus')->whereIsRoot()->get();
        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#regularModal',
            'blockId' => '#menusList',
            'blockData' => view('backend.settings.menu.list', [
                'menus' => $rootMenus
            ])->render(),
            'success' => 'Меню добавлено'
        ]);
    }

    public function view($masterMenuId)
    {
        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();

        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        return view('backend.settings.menu.show', [
            'menuHtml' => $menuHtml,
            'masterMenu' => $categoryTree,
            'title' => 'Настройка меню'
        ]);
    }

    public function itemCreate($masterMenuId)
    {
        $categoriesTree = $this->category->where('owner', 'menus')->get()->toTree();
        $categoriesForSelect = $this->buildCategoriesForSelect($categoriesTree);

        $data = [
            'modalTitle' => 'Добавление меню',
            'modalContent' => view('backend.settings.menu.item_form', [
                'categoriesForSelect' => $categoriesForSelect,
                'formAction' => route('admin.settings.menu.item.store', ['masterId' => $masterMenuId]),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function itemStore(MenuRequest $request, $masterMenuId)
    {
        $array = [
            'parent_id' => $request->category_id ?? $masterMenuId,
            'name' => $request->name,
            'url' => $request->url,
            'owner' => 'menus',
            'handler' => $request->handler,
            'target' => ($request->has('target')) ? 1 : 0
        ];

        $this->category->create($array);

        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();
        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#largeModal',
            'blockId' => '#menusList',
            'blockData' => $menuHtml,
            'success' => 'Меню добавлено'
        ]);
    }

    public function itemEdit($masterMenuId, $itemId)
    {
        $category = $this->category->find($itemId);
        $categoriesTree = $this->category->defaultOrder()->where('owner', 'menus')->get()->toTree();
        $categoriesForSelect = $this->buildCategoriesForSelect($categoriesTree);

        $data = [
            'modalTitle' => 'Редактирование меню',
            'modalContent' => view('backend.settings.menu.item_form', [
                'category' => $category,
                'categoriesForSelect' => $categoriesForSelect,
                'is_create' => false,
                'formAction' => route('admin.settings.menu.item.update', [
                    'masterMenuId' => $masterMenuId,
                    'itemId' => $itemId
                ]),
                'buttonText' => 'Изменить',

            ])->render()];

        return $this->responseJson($data);
    }

    public function itemUpdate(Request $request, $masterMenuId, $itemId)
    {
        $category = $this->category->find($itemId);

        $category->name = $request->input('name');
        $category->url = $request->input('url');
        $category->handler = $request->handler;
        $category->target = ($request->has('target')) ? 1 : 0;
        $category->save();

        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();
        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#largeModal',
            'blockId' => '#menusList',
            'blockData' => $menuHtml,
            'success' => 'Меню изменено'
        ]);
    }

    public function up($masterMenuId, $itemId)
    {
        $category = $this->category->find($itemId);
        $category->up();

        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();

//        $categoryTree = $this->category->defaultOrder()->;

        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        $data = [
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#largeModal',
            'blockId' => '#menusList',
            'blockData' => $menuHtml,
            'success' => 'Меню изменено'
        ];

        return $this->responseJson($data);
    }

    public function down($masterMenuId, $itemId)
    {
        $category = $this->category->find($itemId);
        $category->down();

        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();
        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#largeModal',
            'blockId' => '#menusList',
            'blockData' => $menuHtml,
            'success' => 'Меню изменено'
        ]);
    }

    public function itemDestroy($masterMenuId, $itemId)
    {
        $menu = $this->category->with('page')->find($itemId);
        $menu->delete();
        $categoryTree = $this->category->defaultOrder()->descendantsAndSelf($masterMenuId)->toTree()->first();
        $menuHtml = '<ul class="list-group">';
        $menuHtml .= $this->getChildrenForMenu($categoryTree, $masterMenuId);
        $menuHtml .= '</ul>';

        return $this->responseJson([
            'functions' => ['updateBlock', 'closeModal'],
            'targetModal' => '#largeModal',
            'blockId' => '#menusList',
            'blockData' => $menuHtml,
            'success' => 'Меню изменено'
        ]);
    }
}
