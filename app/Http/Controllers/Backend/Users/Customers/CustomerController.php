<?php

namespace App\Http\Controllers\Backend\Users\Customers;

use App\Http\Requests\Backend\UsersRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CustomerController extends Controller
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function index()
    {
        $title = 'Список пользователей';

        return view('backend.users.customers.index', [
            'title' => $title,
        ]);
    }

    public function getList(Request $request)
    {
        $customers = $this->customer->orderBy('created_at','desc')->paginate(25);

        return response()->json([
            'tableData' => view('backend.users.customers.list', [
                'items' => $customers,
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $customers->appends($customers->all())->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function edit($customerId)
    {
        $customer = $this->customer->find($customerId);

        return response()->json([
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование пользователя',
            'modalContent' => view('backend.users.customers.form', [
                'customer' => $customer,
                'formAction' => route('admin.users.customers.update', ['customerId' => $customerId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }


    public function update(UsersRequest $request, $customerId)
    {
        $request->merge(['active' => $request->has('active')]);

        $customer = $this->customer->find($customerId);
        $customer->update($request->all());

        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('admin.users.customers')
        ]);
    }

}
