<?php

namespace App\Http\Controllers\Frontend;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Torann\LaravelMetaTags\Facades\MetaTag;


class NewsController extends Controller
{
    /**
     * @var News
     */
    private $news;

    public function __construct(News $news)
    {
        $this->news = $news;
    }


    public function show($locale, $slug)
    {
        $news = $this->news->with('mainImage')->where('slug',$slug)->first();
        if (!$news) {
            app()->abort(404, 'Страница не найдена');
        }

        MetaTag::set('title', $news->getTranslation('meta_title', $locale));
        MetaTag::set('description', $news->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $news->getTranslation('meta_keywords', $locale));
        return view('frontend.news.news_item',[
            'news'=>$news
        ]);
    }
}
