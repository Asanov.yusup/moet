<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\OrderRequest;
use App\Jobs\SendNotifyEmail;
use App\Jobs\SendNotifyTelegram;
use App\Mail\SendEmails;
use App\Models\Admin;
use App\Models\City;
use App\Models\Order;
use App\Models\Products;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PurchaseController extends Controller
{
    /**
     * @var Products
     */
    private $products;
    /**
     * @var Region
     */
    private $region;
    /**
     * @var City
     */
    private $city;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Admin
     */
    private $admin;

    /**
     * PurchaseController constructor.
     * @param Products $products
     * @param Region $region
     * @param City $city
     * @param Order $order
     * @param Admin $admin
     */
    public function __construct(Products $products, Region $region, City $city, Order $order, Admin $admin)
    {
        $this->products = $products;
        $this->region = $region;
        $this->city = $city;
        $this->order = $order;
        $this->admin = $admin;
    }


    public function addToCart(Request $request, $locale, $productId)
    {
        $array = session()->get('productIds');
        session()->forget('productIds');
        $array[$productId] = ['qty' => $request->get('qty')];
        session()->put('productIds', $array);

        $array = session()->get('productIds');

        return response()->json([
            'countCart' => count($array)
        ]);
    }

    public function removeFromCart($locale, $productId)
    {
        $array = session()->get('productIds');

        session()->forget('productIds');
        unset($array[$productId]);
        session()->put('productIds', $array);


        $array = session()->get('productIds');

        return response()->json([
            'countCart' => count($array)
        ]);
//        return response()->json(count($array));
    }

    public function getCity(Request $request, $locale)
    {
        $cities = $this->city->where('region_id', $request->get('id'))->get();

        $options = '';
        foreach ($cities as $city) {
            $options .= '<option value="' . $city->id . '">' . $city->name . '</option>';
        }

        return response()->json([
            'options' => $options,
        ]);
    }

    public function checkout(OrderRequest $request, $locale)
    {
        $customer = Auth::guard('customers')->user();

        if($customer){
            $request->merge([
                'name' => $customer->first_name,
                'email' => $customer->email,
                'customer_id' => $customer->id,
                'phone' => $customer->phone ?? ' '
            ]);
        }

        $order = $this->order->create($request->except('quantity'));
        $order->products()->attach($request->get('quantity'));
        session()->forget('productIds');

        $order = $this->order->with('products')->find($order->id);
        $text = 'Новый заказ товаров: ';
        foreach ($order->products as $product) {
            $text .= $product->getTranslation('title', 'ru') . ' ' . $product->pivot->qty . 'шт. ';
        }

        $text .= 'На общую сумму ' . $order->total . 'сом.  От клиента ' . $order->name . ' (тел: ' . $order->phone . ').';

        SendNotifyTelegram::dispatch($text)->delay(now()->addSecond(3));

        $admins = $this->admin->where('active', 1)->get();
        foreach ($admins as $admin) {
            Mail::to($admin->email)->queue(new SendEmails('Новый заказ', 'mail.new_order', $text));
        }


        return response()->json([
            'type' => 'redirect',
            'redirect_url' => route('home', ['locale' => $locale])
        ]);
    }
}
