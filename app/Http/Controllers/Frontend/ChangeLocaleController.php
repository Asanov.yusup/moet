<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class ChangeLocaleController extends Controller
{
    public function changeLocale($needLocale)
    {
        $previousUrl = str_replace('https://', '', str_replace('http://', '', url()->previous()));

        $arr = explode('/', $previousUrl);

        if (count($arr) > 2)
        {
            $arr[1] = $needLocale;
            $url = 'http://' . implode('/', $arr);

            return redirect($url);
        }

        return redirect()->route('home', $needLocale);


    }
}
