<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Torann\LaravelMetaTags\Facades\MetaTag;

class SearchController extends Controller
{
    /**
     * @var Products
     */
    private $products;
    /**
     * @var Category
     */
    private $category;

    private $perPage = 10;


    public function __construct(Products $products, Category $category)
    {
        $this->products = $products;
        $this->category = $category;
    }

//    public function index(Request $request, $locale){
//
//        if($request->has('search')) {
//            $category = strtolower($request->input('search'));
//            $categories = $this->category->whereRaw("LOWER(`name`->'$.\"$locale\"') like LOWER('%$category%')")->orderBy('created_at','desc')->get()->take(3);
//            $product = strtolower($request->input('search'));
//            $products = $this->products->whereRaw("LOWER(`title`->'$.\"$locale\"') like LOWER('%$product%')")->orderBy('created_at','desc')->get()->take(3);
//            $merged = $categories->merge($products);
////            var_dump($merged->toArray());
//            $data = [
//                'searchHtml'=> view('frontend.layouts.search_list',['merged'=>$merged])->render(),
//            ];
//            return response()->json($data);
//        }
//
//        return response()->json('kkkkk');
//    }

    public function search(Request $request, $locale)
    {
        $query = $request->input('query');

        if (!$query) {
            return redirect()->route('frontend.home', ['locale' => $locale]);
        }

        $data = [
            'products' => Products::search($query)->orderBy('created_at', 'desc')->get(),
            'categories' => Category::search($query)->orderBy('created_at', 'desc')->get(),
        ];

        MetaTag::set('title', 'Поиск');
        MetaTag::set('description', "результаты поистка");
        MetaTag::set('keywords', "найти поиск");
        return view('frontend.search.index', [
            'searchQuery' => $query,
            'data' => $data,
        ]);
    }

}
