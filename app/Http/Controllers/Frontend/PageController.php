<?php

namespace App\Http\Controllers\Frontend;

use App\Models\News;
use App\Models\Page;
use App\Http\Controllers\Controller;
use App\Models\Products;
use App\Models\Region;
use Torann\LaravelMetaTags\Facades\MetaTag;

class PageController extends Controller
{
    /**
     * @var Page
     */
    private $page;
    /**
     * @var Products
     */
    private $products;
    /**
     * @var Region
     */
    private $region;
    /**
     * @var News
     */
    private $news;

    /**
     * PageController constructor.
     * @param Page $page
     * @param Products $products
     * @param Region $region
     * @param News $news
     */
    public function __construct(Page $page, Products $products, Region $region, News $news)
    {

        $this->page = $page;
        $this->products = $products;
        $this->region = $region;
        $this->news = $news;
    }

    public function index($locale, $prefix)
    {
        $page = $this->page->where('prefix', $prefix)->first();

        if (!$page) {
            app()->abort(404, 'Страница не найдена');
        }

        MetaTag::set('title', $page->getTranslation('meta_title', $locale));
        MetaTag::set('description', $page->getTranslation('meta_description', $locale));
        MetaTag::set('keywords', $page->getTranslation('meta_keywords', $locale));
//        dd($page);
        switch ($page->prefix) {

            case 'cart':
                $array = session()->get('productIds');
                $products = collect();
                if (!empty($array)) {
                    $products = $this->products->whereIn('id', array_keys($array))->get();
                }
                return view('frontend.purchase.index', [
                    'products' => $products,
                    'regions' => $this->region->get(),
                    'array' => $array,
                    'page'=>$page
                ]);



            case 'contacts':
                return view('frontend.contacts.index',[
                    'page'=>$page
                ]);



            case 'news':
                $news = $this->news->with('mainImage')
                    ->orderBy('created_at', 'desc')
                    ->where('site_display', 1)
                    ->get();
                return view('frontend.news.news', [
                    'news' => $news,
                    'page'=>$page
                ]);



            default:
                return view('frontend.pages.index', [
                    'page' => $page
                ]);
        }
    }
}
