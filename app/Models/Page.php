<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Page extends Model
{
    use HasTranslations;

    public $translatable = ['title', 'url', 'page_content', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table = 'pages';
    protected $fillable = [
        'title',
        'page_content',
        'meta_keywords',
        'meta_description',
        'meta_title',
        'site_display',
        'url',
        'prefix'
    ];

    public function setSiteDisplayAttribute($value)
    {
        $this->attributes['site_display'] = ($value) ? 1 : 0;
    }

    public function getSiteDisplayAttribute($value)
    {
        $color = ($value) ? 'green' : 'red';

        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
    }

    public function menus()
    {
        return $this->hasMany(Category::class, 'page_id');
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'pages')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at', 'desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner', '=', 'pages')
            ->where('main_image', '=', 1);
    }
}
