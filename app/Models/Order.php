<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'name',
        'phone',
        'address',
        'comment',
        'region_id',
        'city_id',
        'total',
        'customer_id'
    ];


    public function products()
    {
        return $this->belongsToMany(\App\Models\Products::class, 'order_products', 'order_id', 'product_id')->withPivot('qty');
    }
}
