<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Promotion extends Model
{
    use Sluggable;
    use HasTranslations;
    use SoftDeletes;

    public $translatable = ['title', 'desc', 'meta_keywords', 'meta_description', 'meta_title'];

    protected $table = 'promotions';
    protected $fillable = [
        'title',
        'slug',
        'desc',
        'site_display',
        'meta_keywords',
        'meta_description',
        'meta_title',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    public function products()
    {
        return $this->belongsToMany(
            \App\Models\Products::class,
            'promotion_products',
            'promotion_id',
            'product_id'
        )->withPivot('product_id','promotion_id')->orderBy('created_at','desc');
    }
    public function setSiteDisplayAttribute($value)
    {
        $this->attributes['site_display'] = ($value) ? 1 : 0;
    }

    public function getSiteDisplayAttribute($value)
    {
        $color = ($value) ? 'green' : 'red';

        return '<i class="fa fa-power-off" style="color:'. $color .'"></i>';
    }
}
