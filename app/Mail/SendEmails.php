<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmails extends Mailable
{
    use Queueable, SerializesModels;
    private $text;

    /**
     * Create a new message instance.
     *
     * @param $subject
     * @param $view
     * @param $text
     */
    public function __construct($subject, $view, $text)
    {
        $this->text = $text;
        $this->subject = $subject;
        $this->view = $view;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view( $this->view)->with('text', $this->text);
    }
}
