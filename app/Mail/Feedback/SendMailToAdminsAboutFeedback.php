<?php

namespace App\Mail\Feedback;

use App\Models\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Kalnoy\Nestedset\Collection;

class SendMailToAdminsAboutFeedback extends Mailable
{
    use Queueable, SerializesModels;
    private $adminName;

    /**
     * Create a new message instance.
     *
     * @param $adminName
     */
    public function __construct($adminName)
    {
        //
        $this->adminName = $adminName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Сообщение с сайта')
            ->view('mail.feedback.admins', ['name' => $this->adminName]);
    }
}
