<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use stdClass;

use App\Models\Customer;

class ConfirmationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;
    public $params;

    /**
     * ConfirmationEmail constructor.
     * @param Customer $customer
     * @param stdClass $params
     */
    public function __construct(Customer $customer, stdClass $params)
    {
        $this->customer = $customer;
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = route('frontend.auth.confirm', [
            'locale' => $this->params->locale,
            'code' => $this->customer->confirm_code,
            'password' => $this->params->password

            ]);

        $subject = config('project.mail_subjects.register_confirmation.' . $this->params->locale);
        $template = 'mail.customer.register_confirmation.' . $this->params->locale;

        return $this->subject($subject)->view($template)->with([
            'link' => $link,
            'customer' => $this->customer,
            'password' => $this->params->password
        ]);
    }
}
