<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendNotifyTelegram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function handle()
    {
        $tokenTelegramBot = config('project.tokenTelegramBot');
        $chatId = config('project.chatId');
        $bot = new \TelegramBot\Api\BotApi($tokenTelegramBot);
        $bot->sendMessage($chatId, $this->text);
    }
}
