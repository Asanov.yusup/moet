<?php namespace App\Services;

use App\Models\Category;
use Illuminate\Http\Request;


class CategoryService {

    private $category;
    private $cats = [];
    private $strCats = '';

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function get($id)
    {
        return $this->category->find($id);
    }



    public function up($categoryId)
    {
        $category = $this->category->find($categoryId);
        $category->up();
    }

    public function down($categoryId)
    {
        $category = $this->category->find($categoryId);
        $category->down();
    }

    public function categoriesForList($owner)
    {
        return $this->buildCategoriesForList($owner);
    }

    public function categoriesForSelect($owner)
    {
        return $this->buildCategoriesForSelect($owner);
    }

    public function getTree($owner)
    {
        return $this->category->where('owner', $owner)->defaultOrder()->get()->toTree();
    }

    private function buildCategoriesForSelect($owner)
    {
        $categories = $this->getTree($owner);

        foreach ($categories as $category)
        {
            $this->cats[$category->id] = $category->name ;
            $this->getChildOptions($category);
            $this->depth = 0;
        }
        return $this->cats;
    }

    private function getChildOptions($category, $depth = 1)
    {
        foreach ($category->children as $k => $child)
        {
            $str = $this->getLines($depth);
            $this->cats[$child->id] = $str . $child->name;
            $this->getChildOptions($child, $depth+1);
        }
        return $this->cats;
    }

    private function buildCategoriesForList ($owner)
    {
        $categories = $this->getTree($owner);

        $this->strCats  .= '<ul class="list-group">';
        foreach ($categories as $category)
        {
            $this->strCats  .= '<li class="list-group-item">';
            $this->strCats  .= '<a class="handle-click" data-type="ajax-get" href="'.route("admin.categories.edit",['owner' => $owner, 'categoryId' => $category->id]).'">' . $category->name . '</a>';
            $this->strCats  .= '<span class="pull-right">';
            $this->strCats  .= '<div class="btn-group" role="group">' ;
            $this->strCats  .= '<a href="' . route('admin.categories.up', ['categoryId' => $category->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-up"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.categories.down', ['categoryId' => $category->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-down"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.categories.destroy', ['categoryId' => $category->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-trash-o"></i></a>' ;
            $this->strCats  .= '</div>' ;
            $this->strCats  .= '</span>' ;
            $this->strCats  .= '</li>' ;
            $this->getChildCats($category, $owner);
            $this->depth = 0;
        }

        $this->strCats  .= '</ul>';
        return $this->strCats;
    }

    private function getChildCats($category, $owner, $depth = 1)
    {
        foreach ($category->children as $k => $child)
        {
            $margin = $depth * 10;
            $str = $this->getLines($depth);
            $this->strCats  .= '<li class="list-group-item" style="margin-left: ' . $margin . 'px">';
            $this->strCats  .= '<a class="handle-click" data-type="ajax-get" href="' .route("admin.categories.edit",['owner' => $owner, 'categoryId' => $child->id]).'">'. $child->name .'</a>';
            $this->strCats  .= '<span class="pull-right">';
            $this->strCats  .= '<div class="btn-group" role="group">' ;
            $this->strCats  .= '<a href="' . route('admin.categories.up', ['categoryId' => $child->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-up"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.categories.down', ['categoryId' => $child->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-down"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.categories.destroy', ['categoryId' => $child->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-trash-o"></i></a>' ;
            $this->strCats  .= '</div>' ;
            $this->strCats  .= '</span>' ;
            $this->strCats  .= '</li>' ;
            $this->cats[$child->id] = $str . $child->name;
            $this->getChildCats($child,  $owner,$depth+1);
        }
        return $this->cats;
    }

    private function getLines($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++)
        {
            $str .= '-';
        }
        return $str;
    }



//    public function getChildSections($category, $owner, $depth = 1)
//    {
//        foreach ($category->children as $k => $child)
//        {
//            $margin = $depth * 10;
//            $str = $this->getLines($depth);
//            $this->strCats  .= '<li class="list-group-item" style="margin-left: ' . $margin . 'px">';
//            $this->strCats  .= '<a class="handle-click" data-type="modal" data-modal="#largeModal" href="' .route("admin.sections.edit",['categoryId' => $child->id]).'">'. $child->name .'</a> [300]';
//            $this->strCats  .= '<span class="pull-right">';
//
//            if (!count($child->children))
//            {
//                $this->strCats  .= '<a href="#"><i class="fa fa-plus-square-o"></i></a>' ;
//            }
//
//            $this->strCats  .= '</span>' ;
//            $this->strCats  .= '</li>' ;
//            $this->cats[$child->id] = $str . $child->name;
//            $this->getChildSections($child,  $owner,$depth+1);
//        }
//        return $this->cats;
//    }




    public function createCategory($requestData, $parentId)
    {
        $category = $this->category->create($requestData);

        if ($parentId) {
            $parent = $this->category->find($parentId);
            $this->category->find($category->id)->appendToNode($parent)->save();
        }

        return $category;
    }

    public function updateCategory($categoryId, $requestData, $parentId)
    {
        $category = $this->category->find($categoryId);

        $category->update($requestData);

        if ($parentId) {
            $parent = $this->category->find($parentId);
            $category->appendToNode($parent)->save();
        }
        return $category;
    }



    public function getCatalogHtmlList($owner, $route)
    {
        $categories = $this->category->with('products')->where('owner', $owner)->defaultOrder()->get()->toTree();
        $this->strCats  .= '<ul>';
        foreach ($categories as $category)
        {
            $this->strCats  .= "<li>";
            $this->strCats  .= '<a href="'.route($route, [ 'catalogId' => $category->id ]).'">' . $category->name . '</a>';

            $this->getChildLiHtml($category, $route);
            $this->strCats  .= '</li>';
        }
        $this->strCats  .= '</ul>';
        return $this->strCats;
    }


    public function getChildLiHtml($category, $route)
    {
        $this->strCats  .= '<ul>';
        foreach ($category->children as $child)
        {
            $this->strCats  .= '<li>';
            $total  = ($child->products->count() > 0) ? '( ' . $child->products->count() . ' )' : '';
            $this->strCats  .= '<a href="'.route($route, [ 'catalogId' => $child->id ]).'">' . $child->name . $total . '</a>';
            $this->getChildLiHtml($child, $route);
            $this->strCats  .= '</li>';
        }
        $this->strCats  .= '</ul>';
        return $this->strCats;
    }


    public function deleteCategory($categoryId)
    {
        $category = $this->category
            ->with('ancestors')
            ->with('descendants')
            ->find($categoryId);

        $descendants = $category->descendants;
        $ancestors = $category->ancestors;
        if ($descendants->count()) {
            if ($ancestors->count()) {
                foreach ($descendants->first()->getSiblings() as $sibling){
                    $sibling->appendToNode($ancestors->last())->save();
                }
                $descendants->first()->appendToNode($ancestors->last())->save();
            } else {
                foreach ($descendants->first()->getSiblings() as $sibling){
                    $sibling->makeRoot()->save();
                }
                $descendants->first()->makeRoot()->save();
            }
        }
        $category->delete();

    }


    public function getAjaxDataForModal($owner, $category = null)
    {
        $categories = $this->category->where('owner', $owner)->defaultOrder()->get()->toTree();

        $this->strCats  .= '<ul class="list-group">';
        foreach ($categories as $category)
        {
            $this->strCats  .= '<li class="list-group-item">';
            $this->strCats  .= $category->name;
            $this->strCats  .= '<span class="pull-right">';
            $this->strCats  .= '<div class="btn-group" role="group">' ;
            $this->strCats  .= '<a href="' . route('admin.catalog.up', ['categoryId' => $category->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-up"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.catalog.down', ['categoryId' => $category->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-down"></i></a>' ;
            $this->strCats  .= '</div>' ;
            $this->strCats  .= '</span>' ;
            $this->strCats  .= '</li>' ;
            $this->getChildCatsList($category, $owner);
            $this->depth = 0;
        }

        $this->strCats  .= '</ul>';

        $categoriesList = $this->strCats;
        $data = [
            'type' => 'updateModal',
            'modal' => '#largeModal',
            'modalTitle' => 'Каталог',
            'modalContent' => view('backend.catalogs.position', [
                'categoriesList' => $categoriesList,
            ])->render(),
        ];

        return $data;
    }

    private function getChildCatsList($category, $owner, $depth = 1)
    {
        foreach ($category->children as $k => $child)
        {
            $margin = $depth * 10;
            $str = $this->getLines($depth);
            $this->strCats  .= '<li class="list-group-item" style="margin-left: ' . $margin . 'px">';
            $this->strCats  .= $child->name;
            $this->strCats  .= '<span class="pull-right">';
            $this->strCats  .= '<div class="btn-group" role="group">' ;
            $this->strCats  .= '<a href="' . route('admin.catalog.up', ['categoryId' => $child->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-up"></i></a>' ;
            $this->strCats  .= '<a href="' . route('admin.catalog.down', ['categoryId' => $child->id, 'owner' => $owner]) . '" class="btn btn-default btn-sm handle-click" data-type="ajax-get"><i class="fa fa-arrow-down"></i></a>' ;
            $this->strCats  .= '</div>' ;
            $this->strCats  .= '</span>' ;
            $this->strCats  .= '</li>' ;
            $this->cats[$child->id] = $str . $child->name;
            $this->getChildCatsList($child,  $owner,$depth+1);
        }
        return $this->cats;
    }
}