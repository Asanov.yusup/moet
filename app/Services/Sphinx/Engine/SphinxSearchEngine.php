<?php

namespace App\Services\Sphinx\Engine;

use Foolz\SphinxQL\SphinxQL;
use Foolz\SphinxQL\Drivers\Mysqli\Connection;
use Laravel\Scout\Builder;
use Laravel\Scout\Engines\Engine as AbstractEngine;

class SphinxSearchEngine extends AbstractEngine
{
    protected $connection;
    protected $searchString;

    /**
     * SphinxSearchEngine constructor.
     */
    public function __construct()
    {
        $connection = new Connection();
        $connection->setParams(config('database.connections.sphinx'));
        $this->connection = $connection;

    }

    /**
     * Update the given model in the index.
     *
     * @param  \Illuminate\Database\Eloquent\Collection $models
     * @return void
     */
    public function update($models)
    {
        if ($models->isEmpty()) {
            return;
        }

        $model = $models->first();

        $index = $model->searchableAs();
        $columns = array_keys($model->toSearchableArray());


        $sphinxQuery = SphinxQL::create($this->connection)
            ->replace()
            ->into($index)
            ->columns($columns);

        $models->each(function ($model) use (&$sphinxQuery) {
            $sphinxQuery->values($model->toSearchableArray());
        });

        $sphinxQuery->execute();

    }

    /**
     * Remove the given model from the index.
     *
     * @param  \Illuminate\Database\Eloquent\Collection $models
     * @return void
     */
    public function delete($models)
    {
        if ($models->isEmpty()) {
            return;
        }

        $model = $models->first();
        $index = $model->searchableAs();
        $key   = $models->pluck($model->getKeyName())->values()->all();

        SphinxQL::create($this->connection)
            ->delete()
            ->from($index)
            ->where('id', 'IN', $key)
            ->execute();

    }

    /**
     * Perform the given search on the engine.
     *
     * @param  \Laravel\Scout\Builder $builder
     * @return mixed
     */
    public function search(Builder $builder)
    {

        return $this->performSearch($builder);
    }

    /**
     * Perform the given search on the engine.
     *
     * @param  \Laravel\Scout\Builder $builder
     * @param  int $perPage
     * @param  int $page
     * @return mixed
     */
    public function paginate(Builder $builder, $perPage, $page)
    {
        $this->searchString = $builder->query;
        return $this->performSearch($builder, $perPage, $page);
    }

    /**
     * Pluck and return the primary keys of the given results.
     *
     * @param  mixed $results
     * @return \Illuminate\Support\Collection
     */
    public function mapIds($results)
    {

        return collect($results['items'])->pluck('id')->values()->all();
    }

    /**
     * Map the given results to instances of the given model.
     *
     * @param Builder $builder
     * @param  mixed $results
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function map(Builder $builder, $results, $model)
    {

        $key = collect($results['items'])->pluck($model->getKeyName())->values()->all();


        if (config('scout.sphinx.snippet.enabled', false))
        {
            $models = $model->whereIn($model->getKeyName(), $key)->get();
            /*->map(function ($model) {
                $around = config('scout.sphinx.snippet_around', 5);
                $limit = config('scout.sphinx.snippet_limit', 256);
                $sql = "CALL SNIPPETS (
                '". addslashes(str_limit($model->content, 100000)) ."',
                '". $model->searchableAs() ."',
                '". $this->searchString."',
                ".$around." AS around,
                ".$limit." AS limit,
                '...<br />...' AS chunk_separator
            )";
                $snippets = SphinxQL::create($this->connection)->query($sql)->execute()->fetchAllAssoc();
                $model->content = $snippets[0]['snippet'];
                return $model;
            });*/

            $docs = [];
            switch (get_class($model))
            {
                case "App\Models\Products":
                    $docs = $models->pluck('title')->values()->all();
                    break;
            }


            if (!count($docs))
            {
                return $models;
            }

            foreach ($docs as &$doc)
            {
                $doc = "'".addslashes($doc)."'";
            }
            $docs = implode(',', $docs);

            $around = config('scout.sphinx.snippet.around', 5);
            $limit = config('scout.sphinx.snippet.limit', 256);
            $chunk_separator = config('scout.sphinx.snippet.chunk_separator', '<br />');

            $keywords = explode(' ', $this->searchString);

            foreach ($keywords as $key => $value)
            {
                $keywords[$key] = '*'.$value.'*';
            }


            $keywords = implode(' ', $keywords);

            $sql = "CALL SNIPPETS (
                (". $docs ."),
                '". $model->searchableAs() ."',
                '". $keywords."|".$this->searchString."',
                ".$around." AS around,
                ".$limit." AS limit,
                '".$chunk_separator."' AS chunk_separator
            )";
            //\Log::info($sql);
            $snippets = SphinxQL::create($this->connection)->query($sql)->execute()->fetchAllAssoc();
            //print_r($snippets);exit;
            $i = 0;
            foreach ($models as &$mod) {

                $mod->snippet = $snippets[$i]['snippet'];
                $i++;
            }
            return $models;
        }
        return $model->whereIn($model->getKeyName(), $key)->get();
    }

    /**
     * Get the total count from a raw result returned by the engine.
     *
     * @param  mixed $results
     * @return int
     */
    public function getTotalCount($results)
    {
        return (int) @$results['total'];
    }

    private function performSearch($builder, $perPage = false, $page = 1)
    {
        $model = $builder->model;
        $index = $model->searchableAs();
        $match = $model->searchableFields();

        $query = SphinxQL::create($this->connection)
            ->select("*")
            ->from($index);



        $wheres = collect($builder->wheres)->each(function ($item, $key) use (&$query) {
            $query = $query->where($key, $item);
        });

        $query->match($match, $builder->query);

        if ($perPage)
        {
            $query->limit(($page - 1) * $perPage, $perPage);
        } else {
            $query->limit(0, 1000000);
        }
        $query->option('max_matches', 1000000);
        $results = ['items' => $query->execute()->fetchAllAssoc()];

        //общее количество результатов
        $query = SphinxQL::create($this->connection)->query('SHOW META LIKE \'total_found\'');
        $meta = $query->execute()->fetchAllAssoc();
        $results['total'] = $this->processMeta($meta);
        return $results;
    }

    private function processMeta($meta)
    {
        return (int) @$meta[0]['Value'];
    }

    /**
     * Escapes characters that are treated as special operators by the query language parser
     *
     * @param string $string unescaped string
     *
     * @return string Escaped string.
     */
    public function escapeString($string)
    {
        $from = array('\\', '(', ')', '|', '-', '!', '@', '~', '"', '&', '/', '^', '$', '=');
        $to   = array('\\\\', '\(', '\)', '\|', '\-', '\!', '\@', '\~', '\"', '\&', '\/', '\^', '\$', '\=');

        return str_replace($from, $to, $string);
    }

    /**
     * Flush all of the model's records from the engine.
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function flush($model)
    {
        // TODO: Implement flush() method.
    }
}