-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: moet
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `super_user` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Dev','dev@dev.dev','$2y$10$.qUk8UasfFvTWLypDVCvZeJBBZj11q30AMKXQePWanFYJ6LHEOPDu',1,1,NULL,NULL,'2018-10-29 03:57:51','2018-12-08 02:58:27'),(2,'Master','saitmoetkg@gmail.com','$2y$10$tQgE1jNuxu2oF25zdm9nE.t1pM81K8wOXsuTrTAv2o.CVvvYmrnSq',1,0,NULL,NULL,'2018-12-08 02:56:29','2018-12-08 02:58:21');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handler` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` tinyint(1) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `page_id` int(10) unsigned DEFAULT NULL,
  `_lft` int(10) unsigned NOT NULL DEFAULT '0',
  `_rgt` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'menus',NULL,NULL,0,'{\"ru\":\"\\u0448\\u0430\\u043f\\u043a\\u0430\",\"en\":\"header\",\"kg\":\"\\u0431\\u0430\\u0448\\u044b\"}','shapka',NULL,NULL,NULL,NULL,1,12,NULL,'2018-10-29 03:57:51','2018-10-29 03:57:51'),(5,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0411\\u044b\\u0442\\u043e\\u0432\\u044b\\u0435 \\u043c\\u043e\\u044e\\u0449\\u0438\\u0435 \\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430\",\"en\":null,\"kg\":null}','sredstva-dlya-mytya-posudy-1','{\"en\": null, \"kg\": null, \"ru\": \"Бытовые моющие средства\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,13,38,NULL,'2018-12-08 04:05:21','2018-12-08 04:06:25'),(6,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u043c\\u044b\\u0442\\u044c\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u044b\",\"en\":null,\"kg\":null}','sredstva-dlya-mytya-posudy','{\"en\": null, \"kg\": null, \"ru\": \"Средства для мытья посуды\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,14,17,5,'2018-12-08 04:06:48','2018-12-20 03:42:42'),(7,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u043c\\u044b\\u0442\\u044c\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u044b\",\"en\":null,\"kg\":null}','sredstva-dlya-mytya-posudy-2','{\"en\": null, \"kg\": null, \"ru\": \"Средства для мытья посуды\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,15,16,6,'2018-12-08 04:08:09','2018-12-08 04:08:09'),(8,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u043a\\u0443\\u0445\\u043d\\u0438\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-kuhni','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки кухни\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,18,21,5,'2018-12-08 04:21:35','2018-12-20 03:42:32'),(9,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u043a\\u0443\\u0445\\u043d\\u0438\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-kuhni-1','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки кухни\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,19,20,8,'2018-12-08 04:22:05','2018-12-08 04:22:05'),(10,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u043e\\u043c\\u043e\\u0435\\u0447\\u043d\\u044b\\u0445 \\u043c\\u0430\\u0448\\u0438\\u043d\",\"en\":null,\"kg\":null}','sredstva-dlya-posudomoechnyh-mashin','{\"en\": null, \"kg\": null, \"ru\": \"Средства для посудомоечных машин\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,22,25,5,'2018-12-14 01:18:04','2018-12-14 01:18:04'),(11,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u043e\\u043c\\u043e\\u0435\\u0447\\u043d\\u044b\\u0445 \\u043c\\u0430\\u0448\\u0438\\u043d\",\"en\":null,\"kg\":null}','sredstva-dlya-posudomoechnyh-mashin-1','{\"en\": null, \"kg\": null, \"ru\": \"Средства для посудомоечных машин\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,23,24,10,'2018-12-14 01:18:18','2018-12-14 01:18:18'),(13,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0436\\u0438\\u043b\\u044b\\u0445 \\u0437\\u043e\\u043d\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-zhilyh-zon','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки жилых зон\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,26,29,5,'2018-12-14 01:25:03','2018-12-14 01:25:03'),(14,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0436\\u0438\\u043b\\u044b\\u0445 \\u0437\\u043e\\u043d\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-zhilyh-zon-1','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки жилых зон\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,27,28,13,'2018-12-14 01:25:24','2018-12-14 01:25:24'),(15,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0432\\u0430\\u043d\\u043d\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-vannoy-komnaty','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки ванной комнаты\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,30,33,5,'2018-12-14 03:41:05','2018-12-14 03:41:05'),(16,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0432\\u0430\\u043d\\u043d\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-vannoy-komnaty-1','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,31,32,15,'2018-12-14 03:41:19','2018-12-14 03:41:19'),(18,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0441\\u0430\\u043d\\u0443\\u0437\\u043b\\u0430\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-sanuzla-1','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки санузла\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,34,37,5,'2018-12-14 08:37:12','2018-12-14 09:11:53'),(19,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 (\\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b) \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438\",\"en\":null,\"kg\":null}','sredstva-materialy-dlya-uborki','{\"en\": null, \"kg\": null, \"ru\": \"Средства (материалы) для уборки\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,39,50,NULL,'2018-12-14 08:41:37','2018-12-14 08:41:37'),(20,'catalog',NULL,NULL,0,'{\"ru\":\"\\u041f\\u0440\\u043e\\u0442\\u0438\\u0440\\u043e\\u0447\\u043d\\u044b\\u0435 \\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b\",\"en\":null,\"kg\":null}','protirochnye-materialy','{\"en\": null, \"kg\": null, \"ru\": \"Протирочные материалы\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,40,49,19,'2018-12-14 08:42:14','2018-12-14 08:42:14'),(21,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0413\\u0443\\u0431\\u043a\\u0438\",\"en\":null,\"kg\":null}','protirochnye-materialy-1','{\"en\": null, \"kg\": null, \"ru\": \"Губки - протирочные материалы для уборки\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,41,42,20,'2018-12-14 08:42:24','2018-12-14 08:43:28'),(22,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0418\\u043d\\u0442\\u0435\\u043d\\u0441\\u0438\\u0432\\u043d\\u0430\\u044f \\u043e\\u0447\\u0438\\u0441\\u0442\\u043a\\u0430\",\"en\":null,\"kg\":null}','intensivnaya-ochistka','{\"en\": null, \"kg\": null, \"ru\": \"Интенсивная очистка\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,43,44,20,'2018-12-14 09:02:42','2018-12-18 02:19:02'),(23,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0430\\u043b\\u0444\\u0435\\u0442\\u043a\\u0438\",\"en\":null,\"kg\":null}','salfetki','{\"en\": null, \"kg\": null, \"ru\": \"Салфетки\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,45,46,20,'2018-12-14 09:09:42','2018-12-14 09:09:42'),(24,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0421\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0443\\u0431\\u043e\\u0440\\u043a\\u0438 \\u0443\\u043d\\u0438\\u0442\\u0430\\u0437\\u0430\",\"en\":null,\"kg\":null}','sredstva-dlya-uborki-unitaza','{\"en\": null, \"kg\": null, \"ru\": \"Средства для уборки унитаза\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,35,36,18,'2018-12-14 09:17:16','2018-12-14 09:17:16'),(25,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0422\\u0440\\u044f\\u043f\\u043a\\u0438 \\u0434\\u043b\\u044f \\u043f\\u043e\\u043b\\u0430\",\"en\":null,\"kg\":null}','tryapki-dlya-pola','{\"en\": null, \"kg\": null, \"ru\": \"Тряпки для пола\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,47,48,20,'2018-12-14 09:26:04','2018-12-14 09:26:04'),(28,'menus','{\"ru\":null}',NULL,0,'{\"ru\":\"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f\",\"en\":\"Main\",\"kg\":\"\\u0411\\u0430\\u0448\\u043a\\u044b\"}','glavnaya-1',NULL,NULL,NULL,1,2,3,1,'2018-12-16 02:10:54','2018-12-20 04:31:19'),(31,'menus','{\"ru\":\"contacts\",\"en\":null,\"kg\":null}',NULL,0,'{\"ru\":\"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b\",\"en\":\"Contacts\",\"kg\":\"\\u0411\\u0430\\u0439\\u043b\\u0430\\u043d\\u044b\\u0448\"}','kontakty',NULL,NULL,NULL,2,10,11,1,'2018-12-18 01:52:48','2018-12-20 03:49:00'),(32,'menus','{\"ru\":\"cart\"}',NULL,0,'{\"en\":null,\"kg\":null,\"ru\":\"\\u041a\\u043e\\u0440\\u0437\\u0438\\u043d\\u0430\"}','korzina',NULL,NULL,NULL,8,8,9,1,'2018-12-18 01:54:34','2018-12-18 13:51:47'),(33,'menus','{\"ru\":\"news\"}',NULL,0,'{\"en\":null,\"kg\":null,\"ru\":\"\\u041d\\u043e\\u0432\\u043e\\u0441\\u0442\\u0438\"}','novosti',NULL,NULL,NULL,9,6,7,1,'2018-12-18 01:55:59','2018-12-18 01:55:59'),(35,'menus','{\"ru\":\"o-nas\",\"en\":null,\"kg\":null}',NULL,0,'{\"ru\":\"\\u041e \\u043d\\u0430\\u0441\",\"en\":null,\"kg\":null}','o-nas',NULL,NULL,NULL,10,4,5,1,'2018-12-18 13:49:33','2018-12-18 14:02:27'),(39,'catalog',NULL,NULL,0,'{\"ru\":\"\\u041c\\u043e\\u044e\\u0449\\u0438\\u0435 \\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u0434\\u043b\\u044f \\u0430\\u0432\\u0442\\u043e\\u043c\\u043e\\u0431\\u0438\\u043b\\u0435\\u0439\",\"en\":null,\"kg\":null}','moyushchie-sredstva-dlya-avtomobiley','{\"en\": null, \"kg\": null, \"ru\": \"Моющие средства для автомобилей\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,51,62,NULL,'2018-12-19 05:17:45','2018-12-19 05:17:45'),(40,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0410\\u0432\\u0442\\u043e\\u043a\\u043e\\u0441\\u043c\\u0435\\u0442\\u0438\\u043a\\u0430\",\"en\":null,\"kg\":null}','avtokosmetika','{\"en\": null, \"kg\": null, \"ru\": \"Автокосметика\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,52,61,39,'2018-12-19 05:18:00','2018-12-19 05:18:00'),(41,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0423\\u0431\\u043e\\u0440\\u043a\\u0430 \\u0432 \\u0441\\u0430\\u043b\\u043e\\u043d\\u0435 \\u0430\\u0432\\u0442\\u043e\\u043c\\u043e\\u0431\\u0438\\u043b\\u044f\",\"en\":null,\"kg\":null}','uborka-v-salone-avtomobilya','{\"en\": null, \"kg\": null, \"ru\": \"Уборка в салоне автомобиля\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,53,54,40,'2018-12-19 05:18:47','2018-12-19 05:18:47'),(42,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0423\\u0445\\u043e\\u0434 \\u0437\\u0430 \\u0434\\u0432\\u0438\\u0433\\u0430\\u0442\\u0435\\u043b\\u0435\\u043c\",\"en\":null,\"kg\":null}','uhod-za-dvigatelem','{\"en\": null, \"kg\": null, \"ru\": \"Уход за двигателем\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,55,56,40,'2018-12-19 08:29:36','2018-12-19 08:29:36'),(43,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0423\\u0445\\u043e\\u0434 \\u0437\\u0430 \\u0448\\u0438\\u043d\\u0430\\u043c\\u0438 \\u0438 \\u0434\\u0438\\u0441\\u043a\\u0430\\u043c\\u0438\",\"en\":null,\"kg\":null}','uhod-za-shinami-i-diskami','{\"en\": null, \"kg\": null, \"ru\": \"Уход за шинами и дисками\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,57,58,40,'2018-12-19 08:32:39','2018-12-19 08:32:39'),(44,'catalog',NULL,NULL,0,'{\"ru\":\"\\u0423\\u0445\\u043e\\u0434 \\u0437\\u0430 \\u0441\\u0442\\u0435\\u043a\\u043b\\u0430\\u043c\\u0438 \\u0438 \\u0437\\u0435\\u0440\\u043a\\u0430\\u043b\\u0430\\u043c\\u0438\",\"en\":null,\"kg\":null}','uhod-za-steklami-i-zerkalami','{\"en\": null, \"kg\": null, \"ru\": \"Уход за стеклами и зеркалами\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',NULL,59,60,40,'2018-12-19 08:36:39','2018-12-19 08:36:39');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL,
  `name` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,2,'{\"en\": \"Batken\", \"kg\": \"Баткен\", \"ru\": \"Баткен\"}',NULL,NULL),(2,2,'{\"en\": \"Kyzyl-Kiya\", \"kg\": \"Кызыл-Кыя\", \"ru\": \"Кызыл-Кия\"}',NULL,NULL),(3,2,'{\"en\": \"Sulukta\", \"kg\": \"Сүлүктү\", \"ru\": \"Сулюкта\"}',NULL,NULL),(4,2,'{\"en\": \"Isfana\", \"kg\": \"Исфана\", \"ru\": \"Исфана\"}',NULL,NULL),(5,2,'{\"en\": \"Aydarken\", \"kg\": \"Айдаркен\", \"ru\": \"Айдаркен\"}',NULL,NULL),(6,2,'{\"en\": \"Kadamjai\", \"kg\": \"Кадамжай\", \"ru\": \"Кадамжай\"}',NULL,NULL),(7,3,'{\"en\": \"Jalal-Abad\", \"kg\": \"Жалал-Абад\", \"ru\": \"Жалал-Абад\"}',NULL,NULL),(8,3,'{\"en\": \"Kara-Kul\", \"kg\": \"Кара-Көл\", \"ru\": \"Кара-Куль\"}',NULL,NULL),(9,3,'{\"en\": \"Kerben\", \"kg\": \"Кербен\", \"ru\": \"Кербен\"}',NULL,NULL),(10,3,'{\"en\": \"Kok-Zhangak\", \"kg\": \"Кок-Жангак\", \"ru\": \"Кок-Жангак\"}',NULL,NULL),(11,3,'{\"en\": \"Kochkor-Ata\", \"kg\": \"Кочкор-Ата\", \"ru\": \"Кочкор-Ата\"}',NULL,NULL),(12,3,'{\"en\": \"Mailuu-Suu\", \"kg\": \"Майлуу-Суу\", \"ru\": \"Майлуу-Суу\"}',NULL,NULL),(13,3,'{\"en\": \"Masy\", \"kg\": \"Масы\", \"ru\": \"Масси\"}',NULL,NULL),(14,3,'{\"en\": \"Suzak\", \"kg\": \"Сузак\", \"ru\": \"Сузак\"}',NULL,NULL),(15,3,'{\"en\": \"Tash-Kumyr\", \"kg\": \"Таш-Көмүр\", \"ru\": \"Таш-Кумыр\"}',NULL,NULL),(16,3,'{\"en\": \"Toktogul\", \"kg\": \"Токтогул\", \"ru\": \"Токтогул\"}',NULL,NULL),(17,3,'{\"en\": \"Shamaldy-Say\", \"kg\": \"Шамалды-Сай\", \"ru\": \"Шамалды-Сай\"}',NULL,NULL),(18,4,'{\"en\": \"Balykchy\", \"kg\": \"Балыкчы\", \"ru\": \"Балыкчи\"}',NULL,NULL),(19,4,'{\"en\": \"Karakol\", \"kg\": \"Каракол\", \"ru\": \"Каракол\"}',NULL,NULL),(20,4,'{\"en\": \"Cholpon-Ata\", \"kg\": \"Чолпон-Ата\", \"ru\": \"Чолпон-Ата\"}',NULL,NULL),(21,4,'{\"en\": \"Ak-Suu\", \"kg\": \"Ак-Суу\", \"ru\": \"Ак-Суу\"}',NULL,NULL),(22,4,'{\"en\": \"Kaji-Say\", \"kg\": \"Кажы-Сай\", \"ru\": \"Каджи-Сай\"}',NULL,NULL),(23,5,'{\"en\": \"Naryn\", \"kg\": \"Нарын\", \"ru\": \"Нарын\"}',NULL,NULL),(24,5,'{\"en\": \"Kochkor\", \"kg\": \"Кочкор\", \"ru\": \"Кочкорка\"}',NULL,NULL),(25,5,'{\"en\": \"At-Bashy\", \"kg\": \"Ат-Башы\", \"ru\": \"Ат-Башы\"}',NULL,NULL),(26,5,'{\"en\": \"Chaek\", \"kg\": \"Чаек\", \"ru\": \"Чаек\"}',NULL,NULL),(27,6,'{\"en\": \"Osh\", \"kg\": \"Ош\", \"ru\": \"Ош\"}',NULL,NULL),(28,6,'{\"en\": \"Uzgen\", \"kg\": \"Өзгөн\", \"ru\": \"Узген\"}',NULL,NULL),(29,6,'{\"en\": \"Nookat\", \"kg\": \"Ноокат\", \"ru\": \"Ноокат\"}',NULL,NULL),(30,7,'{\"en\": \"Talas\", \"kg\": \"Талас\", \"ru\": \"Талас\"}',NULL,NULL),(31,7,'{\"en\": \"Bakai-Ata\", \"kg\": \"Бакай-Ата\", \"ru\": \"Бакай-Ата\"}',NULL,NULL),(32,7,'{\"en\": \"Kara-Buura\", \"kg\": \"Кара-Буура\", \"ru\": \"Кара-Буура\"}',NULL,NULL),(33,7,'{\"en\": \"Manas\", \"kg\": \"Манас\", \"ru\": \"Манас\"}',NULL,NULL),(34,8,'{\"en\": \"Kant\", \"kg\": \"Кант\", \"ru\": \"Кант\"}',NULL,NULL),(35,8,'{\"en\": \"Kara-Balta\", \"kg\": \"Кара-Балта\", \"ru\": \"Кара-Балта\"}',NULL,NULL),(36,8,'{\"en\": \"Tokmok\", \"kg\": \"Токмок\", \"ru\": \"Токмак\"}',NULL,NULL),(37,8,'{\"en\": \"Sokuluk\", \"kg\": \"Сокулук\", \"ru\": \"Сокулук\"}',NULL,NULL),(38,8,'{\"en\": \"Kaindy\", \"kg\": \"Каинды\", \"ru\": \"Каинды\"}',NULL,NULL),(39,8,'{\"en\": \"Orlovka\", \"kg\": \"Орловка\", \"ru\": \"Орловка\"}',NULL,NULL),(40,8,'{\"en\": \"Shopokov\", \"kg\": \"Шопоков\", \"ru\": \"Шопоков\"}',NULL,NULL),(41,8,'{\"en\": \"Kemin\", \"kg\": \"Кемин\", \"ru\": \"Кемин\"}',NULL,NULL);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Ришат','+996552222350','/dev/null',NULL,NULL,NULL,NULL,NULL,NULL),(2,'Ерназ','+77075036800','/dev/null',NULL,NULL,NULL,NULL,NULL,NULL),(3,'Айдос','+77759844489','/dev/null',NULL,NULL,NULL,NULL,NULL,NULL),(4,'Николай','+996555967021','/dev/null',NULL,NULL,NULL,NULL,NULL,NULL),(5,'Мерей','+77071566388','/dev/null',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned DEFAULT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `confirm_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restore_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,NULL,NULL,'Турганбай','С',NULL,NULL,'turganbay.030282@gmail.com','$2y$10$yFx5lgrnC9PfBwlVoNzyVOK.SKpHHrX.GZgoQagtiKuDhlxLVdfUW',NULL,NULL,NULL,1,NULL,NULL,NULL,'2018-10-29 04:09:34','2018-10-29 04:09:34');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `favoritesable_id` int(11) NOT NULL,
  `favoritesable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorites_customer_id_index` (`customer_id`),
  KEY `favorites_favoritesable_id_index` (`favoritesable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'conttnkyh','gwenyth_schnetzer83@rambler.ru',NULL,'Здравствуйте! \r\n \r\nПредлагаем отправку ваших сообщений через формы-контактов сайтов фирм России.  \r\nОтправляем ваши сообщения через формы обратной связи сайтов предприятий по любым странам мира на всех языках.  \r\n \r\nhttp://kontakt-forma.cn/ \r\n \r\nВаше сообщение приходит на контактный адрес электронной почты организации. \r\n \r\nТест: \r\n10000 сообщений по Российской Федерации на ваш E-mail - тысяча руб. \r\nдесять тысяч сообщений по зарубежным зонам на ваш адрес электронной почты - 20 $. \r\nОт вас требуется адрес электронной почты, заголовок и текст письма. \r\n \r\nПредприятия Российской Федерации - 3012045 организаций - 5000 рублей за 1млн. \r\n \r\nМосква и Московская область 627545 организаций - 5000 руб. \r\n \r\nСанкт-Петербург и область 374471 предприятий - 5000 рублей. \r\n \r\nНовые предприятия РФ зарегистрированные в 2016-2018 году - 5000 руб. \r\n \r\nUkraine 605745 сайтов - 5000 рублей. \r\n \r\nВсе русскоговорящие страны минус Россия 15 стран 1722657 сайтов - 10000 руб. \r\n \r\nВся Europe 44 страны 60726150 сайтов - 60000 рублей. \r\n \r\nВесь Европейский союз 28 стран 56752547 доменов- 55000 рублей. \r\n \r\nВся Asia 48 стран 14662004 доменных имён - 15000 руб. \r\n \r\nВся Africa 50 стран 1594390 сайтов - 10000 руб. \r\n \r\nВся Центральная и Северная Америка 35 стран 7441637 доменов - 15000 руб. \r\n \r\nВся Южная Америка 14 стран 5826884 доменных имён - 10000 руб. \r\n \r\nРассылки по движкам сайтов: \r\nАмиро	        2294     sites   1000 руб. \r\nBitrix	        175513   sites   3000 руб. \r\nConcrete5	49721    sites   2000 руб. \r\nCONTENIDO	7769     sites   1000 руб. \r\nCubeCart	1562     sites   1000 руб. \r\nДата лайф Энджин	29220    sites   2000 руб. \r\nДискуз	        70252    sites   2000 руб. \r\nДотнетнук	31114    sites   2000 руб. \r\nDrupal	        802121   sites   5000 руб. \r\nХост CMS	        6342     sites   1000 руб. \r\nInstantCMS	4936     sites   1000 руб. \r\nInvision Power Board 510 sites   1000 руб. \r\nJoomla	        1906994  sites  10000 руб. \r\nЛайфрей	        5937     sites   1000 руб. \r\nMagento	        269488   sites   3000 руб. \r\nMODx	        67023    sites   2000 руб. \r\nМувэбл Тайп	13523    sites   2000 руб. \r\nНетКэт	        6936     sites   1000 руб. \r\nНопКомерс	5313     sites   1000 руб. \r\nOpenCart	321057   sites   3000 руб. \r\nosCommerce	65468    sites   2000 руб. \r\nпхпББ	        3582     sites   1000 руб. \r\nPrestashop	92949    sites   2000 руб. \r\nShopify	        365755   sites   3000 руб. \r\nSimpla	        8963     sites   1000 руб. \r\nSitefinity	4883     sites   1000 руб. \r\nТайпо3	        227167   sites   3000 руб. \r\nЮМИ ЦМС	        15943    sites   2000 руб. \r\nvBulletin	154677   sites   3000 руб. \r\nВикс	        2305768  sites  10000 руб. \r\nВордпресс       14467405 sites  20000 руб. \r\nWooCommerce     2097367  sites  10000 руб. \r\n \r\n \r\nВыборки по сферам деятельности и отраслям России: \r\nB2B services, Автомобили, Car service, Auto products, Bezopasnost, Biznes, Компьютеры и интернет, Krasota, Культура и искусство, Medicina i Farmacevtika, Места и топографические обьекты,Nauka i obrazovanie, Образование, Obshchepit, Society, Отдых и туризм, Products, Proizvodstvo i postavki, Промышленные товары, Entertainment, Реклама и полиграфия, Ремонт стройка, Семья дети домашние животные, Specmagaziny, Спорт, Справочно-информационные системы, Sredstva massovoj informacii, Construction and Real Estate, Telecommunications and communication, Товары для дома и дачи, Transport i perevozki, Uslugi \r\n \r\nВыборки по городам и областям Российской Федерации: \r\nг.Абакан и Республика Хакасия, Anadyr and Chukotka Autonomous Region, г.Архангельск и Архангельская область, Astrahan i Astrahanskaya oblast, Barnaul i Altajskij kraj, Belgorod and Belgorod region, Birobidzhan i Evrejskij avtonomnyj okrug, Blagoveshchensk i Amurskaya oblast, г.Брянск и Брянская область, г.Великий Новгород и Новгородская область, г.Владивосток и Приморский край, Vladikavkaz i Respublika Severnaya Osetiya, г.Владимир и Владимирская область, Volgograd i Volgogradskaya oblast, Vologda i Vologodskaya oblast, Voronezh i Voronezhskaya oblast, г.Горно-Алтайск и Республика Алтай, Groznyj i CHechenskaya Respublika, г.Екатеринбург и Свердловская область, г.Иваново и Ивановская область, Izhevsk and the Republic of Udmurtia, г.Йошкар-Ола и Республика Марий Эл, Irkutsk i Irkutskaya oblast, г.Казань и Республика Татарстан, Kaliningrad i Kaliningradskaya oblast, г.Калуга и Калужская область, Kemerovo i Kemerovskaya oblast, г.Киров и Кировская область, Kostroma and Kostroma region, Krasnodar i Krasnodarskij kraj, Krasnoyarsk i Krasnoyarskij kraj, Kurgan i Kurganskaya oblast, Kursk and Kursk region, Kyzyl and Tyva Republic, г.Липецк и Липецкая область, Magadan and Magadan region, Magas and the Republic of Ingushetia, Maikop and the Republic of Adygea, Makhachkala and the Republic of Dagestan, г.Москва и Московская область, г.Мурманск и Мурманская область, Nalchik i Respublika Kabardino-Balkariya, Naryan-Mar and Nenets Autonomous Region, Nizhny Novgorod and Nizhny Novgorod region, г.Новосибирск и Новосибирская область, Omsk and Omsk Region, Orel i Orlovskaya oblast, г.Оренбург и Оренбургская область, г.Пенза и Пензенская область, Perm i Permskij kraj, г.Петрозаводск и Республика Карелия, Petropavlovsk-Kamchatsky and Kamchatsky Krai, Pskov i Pskovskaya oblast, Rostov-Na-Donu i Rostovskaya oblast, Ryazan and Ryazan region, Salekhard i YAmalo-Neneckij avtonomnyj okrug, г.Самара и Самарская область, St. Petersburg and Leningrad region, Saransk i Respublika Mordoviya, Saratov and Saratov region, Smolensk and Smolensk region, г.Ставрополь и Ставропольский край, Syktyvkar i Respublika Komi, г.Тамбов и Тамбовская область, г.Тверь и Тверская область, Tomsk and Tomsk region, г.Тула и Тульская область, г.Тюмень и Тюменская область, Ulan-Ude and the Republic of Buryatia, Ulyanovsk i Ulyanovskaya oblast, Ufa i Respublika Bashkortostan, Khabarovsk and Khabarovsk Territory, Khanty-Mansiysk and Khanty-Mansiysk Autonomous Okrug, г.Чебоксары и Чувашская Республика, CHelyabinsk i CHelyabinskaya oblast, Cherkessk and the Republic of Karachay-Cherkessia, Chita and Zabaykalsky Krai, Elista and the Republic of Kalmykia, Yuzhno-Sakhalinsk and Sakhalin Region, Yakutsk and Sakha Republic, YAroslavl i YAroslavskaya oblast \r\n \r\nНаши базы: \r\nОрганизации и Предприятия из сервисов Yandex и Google карт собранные по ОКАТО: 966 городов, 42187/108072 крупных/мелких населённых пунктов РФ. \r\nПредприятия и организации России из: YP, Дубль Гис, Рос-биз, Actinfo, Алинформ, Бтк-онлайн,Бигфоунбук, MBTG, Gdetomesto, Гдебиз, Евро-адрес, Б2Б Россия, Zakazrf, Sam5, Фолиант, Yarmap, Топплан, Тел09, Справочник-09, ЕГТС, SPR, Interweb.spb, Московфирма, ЕГРЮЛ, Дата.мос, Мосгид, Мск.справкер и другие. \r\nБазы ВОЙС доменных имён всех стран мира. \r\nВы можете приобрести наши базы отдельно от рассылки по запросу. \r\n \r\nP/S \r\nПжл. не отвечайте на это предложение со своего ящика электронной почты, так как оно создано автоматически и не дойдёт до нас! \r\nИспользуйте для связи контакт-форму <a href=http://kontakt-forma.cn/>рассылка через онтакт формы</a>','2018-12-17 15:42:40','2018-12-17 15:42:40');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_id` int(10) unsigned DEFAULT NULL,
  `main_image` tinyint(1) DEFAULT NULL,
  `client_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(10) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `mime` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_owner_index` (`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (4,'products',NULL,1,1,'хит_3_жпг.jpg','5c0b6f14d49fb.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-08 04:13:24','2018-12-08 04:13:24'),(5,'products',NULL,2,1,'хит_3_жпг.jpg','5c0b702e681bc.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-08 04:18:06','2018-12-08 04:18:06'),(6,'products',NULL,3,1,'антижир_06_пнг.png','5c0b718de3fc9.png',NULL,NULL,2146653,'image/png','null','2018-12-08 04:23:59','2018-12-08 04:23:59'),(7,'products',NULL,4,1,'антижир_1_жпг.jpg','5c0b7c9fcad3c.jpeg',NULL,NULL,907813,'image/jpeg','null','2018-12-08 05:11:12','2018-12-08 05:11:12'),(8,'products',NULL,5,1,'хит_3_жпг.jpg','5c0b874f0331f.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-08 05:56:47','2018-12-08 05:56:47'),(9,'products',NULL,6,1,'хит_3_жпг.jpg','5c13258e48e88.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-14 00:37:50','2018-12-14 00:37:50'),(10,'products',NULL,7,1,'хит_3_жпг.jpg','5c13265a79ef0.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-14 00:41:14','2018-12-14 00:41:14'),(11,'products',NULL,8,1,'хит_3_жпг.jpg','5c1326824b57f.jpeg',NULL,NULL,20977,'image/jpeg','null','2018-12-14 00:41:54','2018-12-14 00:41:54'),(12,'products',NULL,9,1,'антижир_3_пнг.png','5c1328bc558ab.png',NULL,NULL,4121170,'image/png','null','2018-12-14 00:51:27','2018-12-14 00:51:27'),(13,'products',NULL,10,1,'антижир_5_жпг.jpg','5c132a210a286.jpeg',NULL,NULL,1340829,'image/jpeg','null','2018-12-14 00:57:21','2018-12-14 00:57:21'),(14,'products',NULL,11,1,'антизасор_1_пнг.png','5c132a870cc09.png',NULL,NULL,14081,'image/png','null','2018-12-14 00:59:03','2018-12-14 00:59:03'),(15,'products',NULL,12,1,'антизасор_3_жпг.jpg','5c132ace496c3.jpeg',NULL,NULL,19533,'image/jpeg','null','2018-12-14 01:00:14','2018-12-14 01:00:14'),(16,'products',NULL,13,1,'полы_1_жпг.jpg','5c132c450e29c.jpeg',NULL,NULL,8744,'image/jpeg','null','2018-12-14 01:06:29','2018-12-14 01:06:29'),(17,'products',NULL,14,1,'полы_3_жпг.jpg','5c132c76c4601.jpeg',NULL,NULL,17281,'image/jpeg','null','2018-12-14 01:07:18','2018-12-14 01:07:18'),(18,'products',NULL,15,1,'стекло_06_жпг.jpg','5c132d9058e08.jpeg',NULL,NULL,679424,'image/jpeg','null','2018-12-14 01:12:00','2018-12-14 01:12:00'),(19,'products',NULL,16,1,'стекло_1_жпг.jpg','5c132dfc96eef.jpeg',NULL,NULL,962775,'image/jpeg','null','2018-12-14 01:13:49','2018-12-14 01:13:49'),(20,'products',NULL,17,1,'стекло_3_жпг.jpg','5c132e414966f.jpeg',NULL,NULL,1496487,'image/jpeg','null','2018-12-14 01:14:57','2018-12-14 01:14:57'),(21,'products',NULL,18,1,'стекло_5_жпг.jpg','5c132e9ec6c1a.jpeg',NULL,NULL,1399818,'image/jpeg','null','2018-12-14 01:16:31','2018-12-14 01:16:31'),(22,'products',NULL,19,1,'моющее_1_пнг.png','5c132fb173dc6.png',NULL,NULL,14610,'image/png','null','2018-12-14 01:21:05','2018-12-14 01:21:05'),(23,'products',NULL,20,1,'моющее_3_жпг.jpg','5c132fd071809.jpeg',NULL,NULL,19093,'image/jpeg','null','2018-12-14 01:21:36','2018-12-14 01:21:36'),(24,'products',NULL,21,1,'ополаскиватель_1_пнг.png','5c13300999604.png',NULL,NULL,14813,'image/png','null','2018-12-14 01:22:33','2018-12-14 01:22:33'),(25,'products',NULL,22,1,'ополаскиватель_3_жпг.jpg','5c133036e186f.jpeg',NULL,NULL,20807,'image/jpeg','null','2018-12-14 01:23:18','2018-12-14 01:23:18'),(26,'products',NULL,23,1,'универсал_06_жпг.jpg','5c1330f380796.jpeg',NULL,NULL,790296,'image/jpeg','null','2018-12-14 01:26:28','2018-12-14 01:26:28'),(27,'products',NULL,24,1,'универсал_1_пнг.png','5c133125e86b4.png',NULL,NULL,2359297,'image/png','null','2018-12-14 01:27:20','2018-12-14 01:27:20'),(28,'products',NULL,25,1,'универсал_3_жпг.jpg','5c13315ad06c3.jpeg',NULL,NULL,1193038,'image/jpeg','null','2018-12-14 01:28:11','2018-12-14 01:28:11'),(29,'products',NULL,26,1,'универсал_5_жпг.jpg','5c134b33320d1.jpeg',NULL,NULL,1305587,'image/jpeg','null','2018-12-14 03:18:27','2018-12-14 03:18:27'),(30,'products',NULL,27,1,'полироль_06_жпг.jpg','5c134d8cab14f.jpeg',NULL,NULL,1016078,'image/jpeg','null','2018-12-14 03:28:29','2018-12-14 03:28:29'),(31,'products',NULL,28,1,'полироль_1_жпг.jpg','5c134df1d7c2c.jpeg',NULL,NULL,977722,'image/jpeg','null','2018-12-14 03:30:10','2018-12-14 03:30:10'),(32,'products',NULL,29,1,'полироль_3_жпг.jpg','5c134e446194b.jpeg',NULL,NULL,1378230,'image/jpeg','null','2018-12-14 03:31:33','2018-12-14 03:31:33'),(33,'products',NULL,30,1,'полироль_5_жпг.jpg','5c134fb111647.jpeg',NULL,NULL,1200188,'image/jpeg','null','2018-12-14 03:37:37','2018-12-14 03:37:37'),(34,'products',NULL,31,1,'антинакипь_06_жпг.jpg','5c13526f88870.jpeg',NULL,NULL,876393,'image/jpeg','null','2018-12-14 03:49:20','2018-12-14 03:49:20'),(35,'products',NULL,32,1,'антинакипь_1_жпг.jpg','5c13529f1d1fe.jpeg',NULL,NULL,918082,'image/jpeg','null','2018-12-14 03:50:07','2018-12-14 03:50:07'),(36,'products',NULL,33,1,'антинакипь_3_пнг.png','5c1352c5a05dc.png',NULL,NULL,4130809,'image/png','null','2018-12-14 03:50:48','2018-12-14 03:50:48'),(37,'products',NULL,34,1,'антинакипь_5_пнг.png','5c1386829e382.png',NULL,NULL,3676159,'image/png','null','2018-12-14 07:31:33','2018-12-14 07:31:33'),(38,'products',NULL,35,1,'туалет_075_жпг.jpg','5c1396241c13d.jpeg',NULL,NULL,785182,'image/jpeg','null','2018-12-14 08:38:12','2018-12-14 08:38:12'),(39,'products',NULL,36,1,'H101_fabella.jpg','5c1397b9e86b7.jpeg',NULL,NULL,47138,'image/jpeg','null','2018-12-14 08:44:58','2018-12-14 08:44:58'),(40,'products',NULL,37,1,'H101_fabella.jpg','5c1398484ccf2.jpeg',NULL,NULL,47138,'image/jpeg','null','2018-12-14 08:47:20','2018-12-14 08:47:20'),(41,'products',NULL,38,1,'H101_fabella.jpg','5c139896aa487.jpeg',NULL,NULL,47138,'image/jpeg','null','2018-12-14 08:48:38','2018-12-14 08:48:38'),(42,'products',NULL,39,1,'H101_fabella.jpg','5c1398c3734aa.jpeg',NULL,NULL,47138,'image/jpeg','null','2018-12-14 08:49:23','2018-12-14 08:49:23'),(43,'products',NULL,40,1,'L103_lemonmoon.jpg','5c1398f7e6e3a.jpeg',NULL,NULL,57625,'image/jpeg','null','2018-12-14 08:50:16','2018-12-14 08:50:16'),(44,'products',NULL,41,1,'L004_lemonmoon.jpg','5c139931369bb.jpeg',NULL,NULL,64708,'image/jpeg','null','2018-12-14 08:51:13','2018-12-14 08:51:13'),(45,'products',NULL,42,1,'L003_lemonmoon.jpg','5c139abf41bdd.jpeg',NULL,NULL,67041,'image/jpeg','null','2018-12-14 08:57:51','2018-12-14 08:57:51'),(46,'products',NULL,43,1,'K0001_fabella.jpg','5c139b017fc47.jpeg',NULL,NULL,58012,'image/jpeg','null','2018-12-14 08:58:57','2018-12-14 08:58:57'),(47,'products',NULL,44,1,'L002_lemonmoon.jpg','5c139b68ad485.jpeg',NULL,NULL,82826,'image/jpeg','null','2018-12-14 09:00:40','2018-12-14 09:00:40'),(48,'products',NULL,45,1,'LC151_lemonmoon.jpg','5c139c169d151.jpeg',NULL,NULL,145927,'image/jpeg','null','2018-12-14 09:03:34','2018-12-14 09:03:34'),(49,'products',NULL,46,1,'L600_lemonmoon.jpg','5c139ced63664.jpeg',NULL,NULL,104195,'image/jpeg','null','2018-12-14 09:07:09','2018-12-14 09:07:09'),(50,'products',NULL,47,1,'L101_lemonmoon.jpg','5c139d6507258.jpeg',NULL,NULL,84800,'image/jpeg','null','2018-12-14 09:09:09','2018-12-14 09:09:09'),(51,'products',NULL,48,1,'туалет_075_жпг.jpg','5c139f7970eaa.jpeg',NULL,NULL,785182,'image/jpeg','null','2018-12-14 09:18:02','2018-12-14 09:18:02'),(52,'products',NULL,49,1,'L400_lemonmoon.jpg','5c13a018dff98.jpeg',NULL,NULL,141537,'image/jpeg','null','2018-12-14 09:20:41','2018-12-14 09:20:41'),(53,'products',NULL,50,1,'L401_lemonmoon.jpg','5c13a0469d736.jpeg',NULL,NULL,134048,'image/jpeg','null','2018-12-14 09:21:26','2018-12-14 09:21:26'),(54,'products',NULL,51,1,'L403_lemonmoon.jpg','5c13a07e20e57.jpeg',NULL,NULL,173090,'image/jpeg','null','2018-12-14 09:22:22','2018-12-14 09:22:22'),(55,'products',NULL,52,1,'L500_lemonmoon.jpg','5c13a0bc58354.jpeg',NULL,NULL,144638,'image/jpeg','null','2018-12-14 09:23:24','2018-12-14 09:23:24'),(56,'products',NULL,53,1,'L501_lemonmoon.jpg','5c13a11088649.jpeg',NULL,NULL,128096,'image/jpeg','null','2018-12-14 09:24:48','2018-12-14 09:24:48'),(57,'products',NULL,54,1,'L502_lemonmoon.jpg','5c13a137b9cc3.jpeg',NULL,NULL,211286,'image/jpeg','null','2018-12-14 09:25:27','2018-12-14 09:25:27'),(58,'products',NULL,55,1,'L405_lemonmoon.jpg','5c13a18551805.jpeg',NULL,NULL,109782,'image/jpeg','null','2018-12-14 09:26:45','2018-12-14 09:26:45'),(59,'products',NULL,56,1,'L404_lemonmoon.jpg','5c13a1acd0817.jpeg',NULL,NULL,113405,'image/jpeg','null','2018-12-14 09:27:25','2018-12-14 09:27:25'),(60,'products',NULL,57,1,'chisto_06_жпг.jpg','5c1a2a551b7e9.jpeg',NULL,NULL,12454,'image/jpeg','null','2018-12-19 08:24:05','2018-12-19 08:24:05'),(61,'products',NULL,58,1,'chisto_k_1_жпг.jpg','5c1a2b00cdcfb.jpeg',NULL,NULL,14099,'image/jpeg','null','2018-12-19 08:26:56','2018-12-19 08:26:56'),(62,'products',NULL,59,1,'plastik_06_жпг.jpg','5c1a2b69a3f07.jpeg',NULL,NULL,12968,'image/jpeg','null','2018-12-19 08:28:41','2018-12-19 08:28:41'),(63,'products',NULL,60,1,'motor_06_жпг.jpg','5c1a2be7ee137.jpeg',NULL,NULL,12088,'image/jpeg','null','2018-12-19 08:30:48','2018-12-19 08:30:48'),(64,'products',NULL,61,1,'motor_k_1_жпг.jpg','5c1a2c33e5b83.jpeg',NULL,NULL,14818,'image/jpeg','null','2018-12-19 08:32:04','2018-12-19 08:32:04'),(65,'products',NULL,62,1,'cherno_06_жпг.jpg','5c1a2c9b60eac.jpeg',NULL,NULL,13218,'image/jpeg','null','2018-12-19 08:33:47','2018-12-19 08:33:47'),(66,'products',NULL,63,1,'diski_06_жпг.jpg','5c1a2cd8dc1da.jpeg',NULL,NULL,13266,'image/jpeg','null','2018-12-19 08:34:48','2018-12-19 08:34:48'),(67,'products',NULL,64,1,'steklo_06_жпг.jpg','5c1a2d9aa2a85.jpeg',NULL,NULL,13303,'image/jpeg','null','2018-12-19 08:38:02','2018-12-19 08:38:02'),(71,'pages','image',1,1,'formula-add-kumilv.jpg','5c1bb78fe71bf.jpeg',NULL,NULL,49192,'image/jpeg','[\"512\",\"256\",\"128\"]','2018-12-20 12:38:55','2018-12-20 12:38:59'),(72,'pages','image',1,NULL,'tovaridomnews27.jpg','5c1bb78ff40e8.jpeg',NULL,NULL,50488,'image/jpeg','[\"512\",\"256\",\"128\"]','2018-12-20 12:38:56','2018-12-20 12:38:56'),(73,'pages','image',1,NULL,'моющие_ср-ва.jpg','5c1bb79009e3d.jpeg',NULL,NULL,36227,'image/jpeg','[\"512\",\"256\",\"128\"]','2018-12-20 12:38:56','2018-12-20 12:38:56');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2017_11_20_165143_create_admins_table',1),(2,'2017_11_22_142714_create_permission_groups_table',1),(3,'2017_11_22_145500_create_permissions_table',1),(4,'2017_11_22_152739_create_roles_table',1),(5,'2017_11_22_164941_create_role_permissions_table',1),(6,'2017_11_23_154003_create_role_admins_table',1),(7,'2018_03_20_085355_create_categories_table',1),(8,'2018_03_27_122134_create_news_table',1),(9,'2018_06_04_211436_create_media_table',1),(10,'2018_06_05_074343_create_settings_table',1),(11,'2018_06_05_111010_create_pages_table',1),(12,'2018_08_04_105233_create_contacts_table',1),(13,'2018_08_30_194930_create_products_table',1),(14,'2018_09_18_051610_create_regions_table',1),(15,'2018_09_18_054403_create_cities_table',1),(16,'2018_09_18_115516_create_orders_table',1),(17,'2018_09_18_120513_create_order_products_table',1),(18,'2018_10_05_135924_create_jobs_table',1),(19,'2018_10_06_141736_create_failed_jobs_table',1),(20,'2018_10_09_095254_create_feedback_table',1),(21,'2018_10_09_110836_add_column_prefix_in_pages_table',1),(22,'2018_10_10_054445_create_promotions_table',1),(23,'2018_10_10_054826_create_customers_table',1),(24,'2018_10_10_155555_create_promotion_products_table',1),(25,'2018_10_14_104937_add_status_column_to_orders_table',1),(26,'2018_10_20_140148_create_favorites_table',1),(28,'2018_12_23_133722_create_product_categories_table',2),(30,'2018_12_23_135726_drop_column_category_id_in_products_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `title` json NOT NULL,
  `short_content` json NOT NULL,
  `long_content` json NOT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `is_pinned` tinyint(1) NOT NULL DEFAULT '0',
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_display` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,1,'{\"en\": \"News\", \"kg\": \"Жанылыктар\", \"ru\": \"Новости\"}','{\"en\": \"Short content\", \"kg\": \"Кыскача жанылык\", \"ru\": \"Краткое содержание\"}','{\"en\": \"Long content\", \"kg\": \"Бардык жанылык\", \"ru\": \"Полное содержание\"}','{\"en\": \"News\", \"kg\": \"Жанылык\", \"ru\": \"Новости\"}','{\"en\": \"Description\", \"kg\": \"Баяндама\", \"ru\": \"Описание\"}','{\"en\": \"News\", \"kg\": \"Жанылык\", \"ru\": \"Новости\"}',0,0,'news',1,NULL,'2018-10-29 03:57:52','2018-10-29 03:57:52');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_products`
--

DROP TABLE IF EXISTS `order_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_products` (
  `order_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_products`
--

LOCK TABLES `order_products` WRITE;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
INSERT INTO `order_products` VALUES (1,8,1),(2,48,1),(2,64,1),(3,4,2),(4,36,1),(4,41,1),(5,8,1),(6,43,1),(7,58,1),(8,57,1);
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `city_id` int(10) unsigned DEFAULT NULL,
  `region_id` int(10) unsigned DEFAULT NULL,
  `total` double(8,2) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'Валентина','0770884008','Салиевой 145/86',NULL,NULL,NULL,250.00,NULL,'2018-12-21 09:52:43','2018-12-21 09:52:43'),(2,'Oksana  Kariakina','+996550586721','37 Kulikovskaya',NULL,NULL,NULL,360.00,NULL,'2018-12-21 10:18:48','2018-12-21 10:18:48'),(3,'Анна','0555770233','4/35/45','Ваша продукция просто улёт!',NULL,NULL,460.00,NULL,'2018-12-21 11:30:20','2018-12-21 11:30:20'),(4,'Анна','0555770233','4/35/45','Надеюсь, что понравится.',NULL,NULL,95.00,NULL,'2018-12-21 11:36:56','2018-12-21 11:36:56'),(5,'test','123','test','йцуйцуйцу',NULL,NULL,250.00,NULL,'2018-12-22 11:10:16','2018-12-22 11:10:16'),(6,'test','123','test','фывфыв',NULL,NULL,50.00,NULL,'2018-12-22 11:11:07','2018-12-22 11:11:07'),(7,'test','123','test','фывфыв',NULL,NULL,580.00,NULL,'2018-12-22 11:11:29','2018-12-22 11:11:29'),(8,'test','123','test','ыыыыы',NULL,NULL,140.00,NULL,'2018-12-22 11:12:08','2018-12-22 11:12:08');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `url` json DEFAULT NULL,
  `prefix` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `page_content` json DEFAULT NULL,
  `site_display` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'{\"en\": \"Main\", \"kg\": \"Башкы\", \"ru\": \"Главная\"}',NULL,NULL,'{\"en\": \"Main\", \"kg\": \"Башкы\", \"ru\": \"Главная\"}','{\"en\": \"Main\", \"kg\": \"Башкы\", \"ru\": \"Главная\"}','{\"en\": \"Main\", \"kg\": \"Башкы\", \"ru\": \"Главная\"}','{\"en\": \"<p>Main</p>\", \"kg\": \"<p>Башкы</p>\", \"ru\": null}',1,'2018-10-29 03:57:51','2018-12-20 12:39:03'),(2,'{\"en\": \"Contacts\", \"kg\": \"Байланыш\", \"ru\": \"Контакты\"}',NULL,'contacts','{\"en\": \"Contacts\", \"kg\": \"Байланыш\", \"ru\": \"Контакты\"}','{\"en\": \"Contacts\", \"kg\": \"Байланыш\", \"ru\": \"Контакты\"}','{\"en\": \"Contacts\", \"kg\": \"Байланыш\", \"ru\": \"Контакты\"}','{\"en\": \"<p>Contacts</p>\", \"kg\": \"<p>Байланыш</p>\", \"ru\": \"<p>Наш адрес:&nbsp;Кыргызстан, город Бишкек, улица Баялинова, дом №113</p>\\r\\n\\r\\n<p>Телефон:&nbsp;+996 705 40 10 30</p>\\r\\n\\r\\n<p>В правой части сайта Вы также найдете кнопки мессенджеров, с помощью которых сможете с нами связаться</p>\"}',1,'2018-10-29 03:57:51','2018-12-18 13:58:34'),(3,'{\"en\": \"news\", \"kg\": \"news\", \"ru\": \"Новости\"}',NULL,'news','{\"en\": \"news\", \"kg\": \"news\", \"ru\": \"Новости\"}','{\"en\": \"news\", \"kg\": \"news\", \"ru\": \"Новости\"}','{\"en\": \"news\", \"kg\": \"news\", \"ru\": \"Новости\"}','{\"en\": \"news\", \"kg\": \"news\", \"ru\": \"Новости\"}',1,'2018-10-29 03:57:51','2018-10-29 03:57:51'),(4,'{\"en\": \"Cart\", \"kg\": \"Корзина\", \"ru\": \"Корзина\"}',NULL,'cart','{\"en\": \"Корзина\", \"kg\": \"Корзина\", \"ru\": \"Корзина\"}','{\"en\": \"Корзина\", \"kg\": \"Корзина\", \"ru\": \"Корзина\"}','{\"en\": \"Корзина\", \"kg\": \"Корзина\", \"ru\": \"Корзина\"}','{\"en\": \"Cart\", \"kg\": \"Байланыш\", \"ru\": \"Корзина\"}',1,'2018-10-29 03:57:51','2018-10-29 03:57:51'),(7,'{\"en\": null, \"kg\": null, \"ru\": \"Контакт\"}',NULL,'wewewe','{\"en\": null, \"kg\": null, \"ru\": \"wewe\"}','{\"en\": null, \"kg\": null, \"ru\": \"wewewe\"}','{\"en\": null, \"kg\": null, \"ru\": \"wewewe\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>ул. Ахунбаева</p>\"}',1,'2018-12-17 08:51:28','2018-12-17 08:51:58'),(9,'{\"en\": null, \"kg\": null, \"ru\": \"Новости\"}',NULL,'news','{\"en\": null, \"kg\": null, \"ru\": \"qwewq\"}','{\"en\": null, \"kg\": null, \"ru\": \"qee\"}','{\"en\": null, \"kg\": null, \"ru\": \"qweqwe\"}','{\"en\": null, \"kg\": null, \"ru\": null}',1,'2018-12-18 01:55:51','2018-12-18 01:55:51'),(10,'{\"en\": null, \"kg\": null, \"ru\": \"О нас\"}',NULL,'o-nas','{\"en\": null, \"kg\": null, \"ru\": \"О нас\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Текст о нас</p>\\r\\n\\r\\n<p>Информация о компании</p>\"}',1,'2018-12-18 03:02:59','2018-12-18 14:01:46');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` enum('admin','client') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
INSERT INTO `permission_groups` VALUES (1,'admin','Пользователи'),(2,'admin','Управление');
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` enum('admin','client') COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `alias` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_alias_index` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'admin',1,'manage_admins','Управление администраторами','Позволяет создавать администраторов и назначать им роли'),(2,'admin',2,'manage_pages','Управление страницами','Позволяет редактировать страницы магазина'),(3,'admin',2,'manage_staff','Обслуживающий персонал','Позволяет создавать курьеров, кассиров и другой обслуживающий персонал');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `position` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_categories`
--

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` VALUES (1,3,9,0,NULL,NULL),(2,4,9,0,NULL,NULL),(3,5,7,0,NULL,NULL),(4,6,7,0,NULL,NULL),(5,7,7,0,NULL,NULL),(6,8,7,0,NULL,NULL),(7,9,9,0,NULL,NULL),(8,10,9,0,NULL,NULL),(9,11,9,0,NULL,NULL),(10,12,9,0,NULL,NULL),(11,13,9,0,NULL,NULL),(12,14,9,0,NULL,NULL),(13,15,9,0,NULL,NULL),(14,16,9,0,NULL,NULL),(15,17,9,0,NULL,NULL),(16,18,9,0,NULL,NULL),(17,19,11,0,NULL,NULL),(18,20,11,0,NULL,NULL),(19,21,11,0,NULL,NULL),(20,22,11,0,NULL,NULL),(21,23,14,0,NULL,NULL),(22,24,14,0,NULL,NULL),(23,25,14,0,NULL,NULL),(24,26,14,0,NULL,NULL),(25,27,14,0,NULL,NULL),(26,28,14,0,NULL,NULL),(27,29,14,0,NULL,NULL),(28,30,14,0,NULL,NULL),(29,31,16,0,NULL,NULL),(30,32,16,0,NULL,NULL),(31,33,16,0,NULL,NULL),(32,34,16,0,NULL,NULL),(33,35,18,0,NULL,NULL),(34,36,21,0,NULL,NULL),(35,37,21,0,NULL,NULL),(36,38,21,0,NULL,NULL),(37,39,21,0,NULL,NULL),(38,40,21,0,NULL,NULL),(39,41,21,0,NULL,NULL),(40,42,21,0,NULL,NULL),(41,43,21,0,NULL,NULL),(42,44,21,0,NULL,NULL),(43,45,22,0,NULL,NULL),(44,46,22,0,NULL,NULL),(45,47,22,0,NULL,NULL),(46,48,24,0,NULL,NULL),(47,49,23,0,NULL,NULL),(48,50,23,0,NULL,NULL),(49,51,23,0,NULL,NULL),(50,52,23,0,NULL,NULL),(51,53,23,0,NULL,NULL),(52,54,23,0,NULL,NULL),(53,55,25,0,NULL,NULL),(54,56,25,0,NULL,NULL),(55,57,41,0,NULL,NULL),(56,58,41,0,NULL,NULL),(57,59,41,0,NULL,NULL),(58,60,42,0,NULL,NULL),(59,61,42,0,NULL,NULL),(60,62,43,0,NULL,NULL),(61,63,43,0,NULL,NULL),(62,64,44,0,NULL,NULL);
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `short_desc` json DEFAULT NULL,
  `desc` json DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `site_display` tinyint(1) NOT NULL DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `hit` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.</p>\"}',100.00,'moyushchee-dlya-posudy-hit-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,'2018-12-08 04:16:04','2018-12-08 04:10:30','2018-12-08 04:16:04'),(2,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.</p>\"}',NULL,'moyushchee-dlya-posudy-hit-3-l',100,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,'2018-12-08 04:20:46','2018-12-08 04:18:06','2018-12-08 04:20:46'),(3,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды…\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды&hellip;</p>\"}',140.00,'ochistitel-antizhir-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-08 04:23:57','2018-12-14 01:01:31'),(4,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"В комплекте пустая бутылка с распылителем\\r\\nУдаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды…\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды&hellip;</p>\"}',230.00,'ochistitel-antizhir-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-08 05:11:11','2018-12-14 01:01:22'),(5,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде.</p>\"}',440.00,'moyushchee-dlya-posudy-hit-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-08 05:56:47','2018-12-14 01:02:09'),(6,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Высокопенный концентрат, очень экономный для самого требовательного клиента, можно мыть детские игрушки и овощи-фрукты. Без запаха = не остается на посуде</p>\"}',680.00,'moyushchee-dlya-posudy-hit-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее для посуды \\\"Хит\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:37:50','2018-12-14 01:02:01'),(7,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее \\\"Для посуды\\\", 5 л (лимон)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для ручной мойки посуды. Высокопенное, не сушит руки, легко смывается водой\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для ручной мойки посуды. Высокопенное, не сушит руки, легко смывается водой</p>\"}',250.00,'moyushchee-dlya-posudy-5-l-limon',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее \\\"Для посуды\\\", 5 л (лимон)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:41:14','2018-12-14 01:01:55'),(8,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее \\\"Для посуды\\\", 5 л (ягода)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для ручной мойки посуды. Высокопенное, не сушит руки, легко смывается водой\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для ручной мойки посуды. Высокопенное, не сушит руки, легко смывается водой</p>\"}',250.00,'moyushchee-dlya-posudy-5-l-yagoda',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Моющее \\\"Для посуды\\\", 5 л (ягода)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:41:41','2018-12-14 01:01:47'),(9,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды…\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды&hellip;</p>\"}',430.00,'ochistitel-antizhir-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:51:24','2018-12-14 01:01:15'),(10,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды…\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Удаляет жир с поверхности кухонных плит, посуды, казанов, кухонных вытяжек, мебели, бытовой техники на кухне, со скатертей и одежды&hellip;</p>\"}',700.00,'ochistitel-antizhir-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антижир\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:57:21','2018-12-14 01:01:07'),(11,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Антизасор\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления белковых и жировых отложений в трубах канализации и устранения запахов. 200-400 мл для интенсивной очистки, 100-200 мл для проведения профилактики\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления белковых и жировых отложений в трубах канализации и устранения запахов. 200-400 мл для интенсивной очистки, 100-200 мл для проведения профилактики</p>\"}',210.00,'sredstvo-antizasor-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Антизасор\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 00:59:03','2018-12-14 01:01:00'),(12,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Антизасор\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления белковых и жировых отложений в трубах канализации и устранения запахов. 200-400 мл для интенсивной очистки, 100-200 мл для проведения профилактики\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления белковых и жировых отложений в трубах канализации и устранения запахов. 200-400 мл для интенсивной очистки, 100-200 мл для проведения профилактики</p>\"}',540.00,'sredstvo-antizasor-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Антизасор\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:00:14','2018-12-14 01:00:14'),(13,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для мытья пола\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Мойка полов из любых материалов: линолеум, ламинат, камень, дерево, быстро высыхает без разводов, дозировка: 2-4 крышки (40-80 мл) на ведро 10 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Мойка полов из любых материалов: линолеум, ламинат, камень, дерево, быстро высыхает без разводов, дозировка: 2-4 крышки (40-80 мл) на ведро 10 л</p>\"}',200.00,'sredstvo-dlya-mytya-pola-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для мытья пола\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:06:29','2018-12-14 01:06:29'),(14,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для мытья пола\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Мойка полов из любых материалов: линолеум, ламинат, камень, дерево, быстро высыхает без разводов, дозировка: 2-4 крышки (40-80 мл) на ведро 10 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Мойка полов из любых материалов: линолеум, ламинат, камень, дерево, быстро высыхает без разводов, дозировка: 2-4 крышки (40-80 мл) на ведро 10 л</p>\"}',480.00,'sredstvo-dlya-mytya-pola-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для мытья пола\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:07:18','2018-12-14 01:07:18'),(15,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять \\\"жирные пальцы\\\" и со стекла, и с любых твердых поверхностей\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять &quot;жирные пальцы&quot; и со стекла, и с любых твердых поверхностей</p>\"}',130.00,'ochistitel-dlya-stekol-i-zerkal-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:12:00','2018-12-14 01:12:00'),(16,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять \\\"жирные пальцы\\\" и со стекла, и с любых твердых поверхностей\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять &quot;жирные пальцы&quot; и со стекла, и с любых твердых поверхностей</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',210.00,'ochistitel-dlya-stekol-i-zerkal-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:13:48','2018-12-14 01:13:48'),(17,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять \\\"жирные пальцы\\\" и со стекла, и с любых твердых поверхностей\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять &quot;жирные пальцы&quot; и со стекла, и с любых твердых поверхностей</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',360.00,'ochistitel-dlya-stekol-i-zerkal-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:14:57','2018-12-14 01:14:57'),(18,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять \\\"жирные пальцы\\\" и со стекла, и с любых твердых поверхностей\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Может очищать и внешнюю сторону окон от дорожной грязи, быстро испаряется с поверхности, средство с нейтральным запахом. Можно удалять &quot;жирные пальцы&quot; и со стекла, и с любых твердых поверхностей</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',580.00,'ochistitel-dlya-stekol-i-zerkal-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Для стекол и зеркал\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:16:30','2018-12-14 01:16:30'),(19,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для посудомоек\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Беспенный концентрат для автоматического мытья посуды из любых материалов: стекло, фаянс, пластмасса, сталь, фарфор… Быстро растворяется в воде. Норма дозировки меняется в зависимости от технических характеристик машины\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Беспенный концентрат для автоматического мытья посуды из любых материалов: стекло, фаянс, пластмасса, сталь, фарфор&hellip; Быстро растворяется в воде. Норма дозировки меняется в зависимости от технических характеристик машины</p>\"}',370.00,'sredstvo-dlya-posudomoek-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для посудомоек\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:21:05','2018-12-14 01:21:05'),(20,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для посудомоек\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Беспенный концентрат для автоматического мытья посуды из любых материалов: стекло, фаянс, пластмасса, сталь, фарфор… Быстро растворяется в воде. Норма дозировки меняется в зависимости от технических характеристик машины\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Беспенный концентрат для автоматического мытья посуды из любых материалов: стекло, фаянс, пластмасса, сталь, фарфор&hellip; Быстро растворяется в воде. Норма дозировки меняется в зависимости от технических характеристик машины</p>\"}',720.00,'sredstvo-dlya-posudomoek-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для посудомоек\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:21:36','2018-12-14 01:21:36'),(21,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Ополаскиватель\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Предотвращает образование разводов и пятен на посуде после высыхания, придает блеск\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Предотвращает образование разводов и пятен на посуде после высыхания, придает блеск</p>\"}',370.00,'sredstvo-opolaskivatel-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Ополаскиватель\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:22:33','2018-12-14 01:22:33'),(22,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Ополаскиватель\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Предотвращает образование разводов и пятен на посуде после высыхания, придает блеск\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Предотвращает образование разводов и пятен на посуде после высыхания, придает блеск</p>\"}',720.00,'sredstvo-opolaskivatel-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Ополаскиватель\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:23:18','2018-12-14 01:23:18'),(23,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани…\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани&hellip;</p>\"}',140.00,'ochistitel-universal-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:26:27','2018-12-14 01:28:45'),(24,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани…\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани&hellip;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',230.00,'ochistitel-universal-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:27:17','2018-12-14 01:28:31'),(25,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани…\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани&hellip;</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',430.00,'ochistitel-universal-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 01:28:10','2018-12-14 01:28:10'),(26,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани…\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для очистки твердых и мягких поверхностей, мытья пола, очистки пластиковых стеклопакетов, отбеливающий эффект для ткани&hellip;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',700.00,'ochistitel-universal-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Универсал\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:18:27','2018-12-14 03:18:27'),(27,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект</p>\"}',150.00,'ochistitel-polirol-mebeli-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:28:28','2018-12-14 03:28:28'),(28,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',250.00,'ochistitel-polirol-mebeli-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:30:09','2018-12-14 03:30:09'),(29,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',530.00,'ochistitel-polirol-mebeli-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:31:32','2018-12-14 03:31:32'),(30,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Антистатик-полироль для мебели из любых твердых материалов. Обладает хорошим запахом. Матовый эффект</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',850.00,'ochistitel-polirol-mebeli-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Полироль мебели\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:37:37','2018-12-14 03:37:37'),(31,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн…Предотвращает быстрое образование накипи\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн&hellip;Предотвращает быстрое образование накипи</p>\"}',170.00,'ochistitel-antinakip-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:49:19','2018-12-14 03:49:19'),(32,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн…Предотвращает быстрое образование накипи\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн&hellip;Предотвращает быстрое образование накипи</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',270.00,'ochistitel-antinakip-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:50:07','2018-12-14 03:50:07'),(33,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн…Предотвращает быстрое образование накипи\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн&hellip;Предотвращает быстрое образование накипи</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',530.00,'ochistitel-antinakip-3-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антинакипь\\\", 3 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 03:50:45','2018-12-14 03:50:45'),(34,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антианкипь\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн…Предотвращает быстрое образование накипи\\r\\n\\r\\nВ комплекте пустая бутылка с распылителем\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Для удаления известкового налета (от воды) с любых поверхностей: с кафеля, с сантехники, с фаянса, со стекол душевых кабин, акриловых ванн&hellip;Предотвращает быстрое образование накипи</p>\\r\\n\\r\\n<p>В комплекте пустая бутылка с распылителем</p>\"}',850.00,'ochistitel-antiankip-5-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Очиститель \\\"Антианкипь\\\", 5 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 07:31:30','2018-12-14 07:31:30'),(35,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для туалета\\\", 0,75 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Средство-гель для удаления желтизны в унитазе. Нейтральных запах\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Средство-гель для удаления желтизны в унитазе. Нейтральных запах</p>\"}',230.00,'sredstvo-dlya-tualeta-0-75-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для туалета\\\", 0,75 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:38:12','2018-12-14 08:38:12'),(36,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Фигурные. Структура с открытыми порами. Не теряют форму. Легко промываются\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Фигурные. Структура с открытыми порами. Не теряют форму. Легко промываются</p>\"}',60.00,'gubki-dlya-posudy-fabella-2-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:44:57','2018-12-14 08:44:57'),(37,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA целлюлозные (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Целлюлозные. Супервпитывающие, с абразивом высокой прочности\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Целлюлозные. Супервпитывающие, с абразивом высокой прочности</p>\"}',120.00,'gubki-dlya-posudy-fabella-cellyuloznye-2-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA целлюлозные (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:47:20','2018-12-14 08:47:20'),(38,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA с особым абразивом (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Удобные. Абразив с высокой чистящей способностью. Легко промываются\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Удобные. Абразив с высокой чистящей способностью. Легко промываются</p>\"}',60.00,'gubki-dlya-posudy-fabella-s-osobym-abrazivom-2-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды FABELLA с особым абразивом (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:48:38','2018-12-14 08:48:38'),(39,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для кухни  FABELLA (4 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Целлюлозные. Высокая впитывающая способность. В пачке 4 штуки\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Целлюлозные. Высокая впитывающая способность. В пачке 4 штуки</p>\"}',200.00,'gubka-dlya-kuhni-fabella-4-shtuki',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для кухни  FABELLA (4 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:49:23','2018-12-14 08:49:23'),(40,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды LEMONMOON (4 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Ретикулированный чистящий слой. Не царапает. Легко промывается. 4 шт в пачке\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Ретикулированный чистящий слой. Не царапает. Легко промывается. 4 шт в пачке</p>\"}',100.00,'gubki-dlya-posudy-lemonmoon-4-shtuki',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды LEMONMOON (4 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:50:15','2018-12-14 08:50:15'),(41,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды LEMONMOON (5 штук)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 96*64 мм. Абразив высокого качества. 5 шт в пачке\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 96*64 мм. Абразив высокого качества. 5 шт в пачке</p>\"}',35.00,'gubki-dlya-posudy-lemonmoon-5-shtuk',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки для посуды LEMONMOON (5 штук)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:51:13','2018-12-14 08:51:13'),(42,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для посуды LEMONMOON (3 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Из крупнопористового поролона. 3 штуки в пачке\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Из крупнопористового поролона. 3 штуки в пачке</p>\"}',35.00,'gubka-dlya-posudy-lemonmoon-3-shtuki',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для посуды LEMONMOON (3 штуки)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:57:51','2018-12-14 08:57:51'),(43,'{\"en\": null, \"kg\": null, \"ru\": \"Губка гриль FABELLA (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Две чистящие стороны. Для сверхстойких загрязнений\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Две чистящие стороны. Для сверхстойких загрязнений</p>\"}',50.00,'gubka-gril-fabella-1-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губка гриль FABELLA (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 08:58:57','2018-12-14 08:58:57'),(44,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для посуды LEMONMOON с удобным захватом для пальцев (5 штук)\"}','{\"en\": null, \"kg\": null, \"ru\": \"С абразивным слоем. Удобный захват для пальцев. 5 штук в пачке\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>С абразивным слоем. Удобный захват для пальцев. 5 штук в пачке</p>\"}',60.00,'gubka-dlya-posudy-lemonmoon-5-shtuk',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губка для посуды LEMONMOON с удобным захватом для пальцев (5 штук)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:00:40','2018-12-14 09:01:47'),(45,'{\"en\": null, \"kg\": null, \"ru\": \"Скрабы металлические LEMONMOON (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Хромированная сталь. Экстремально сильная очистка\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Хромированная сталь. Экстремально сильная очистка</p>\"}',40.00,'skraby-metallicheskie-lemonmoon-2-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Скрабы металлические LEMONMOON (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:03:34','2018-12-14 09:03:34'),(46,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки абразивные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 133*133 мм. Абразив высокого качества. Цвета: синий, желтый, красный\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 133*133 мм. Абразив высокого качества. Цвета: синий, желтый, красный</p>\"}',50.00,'salfetki-abrazivnye-lemonmoon-3-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки абразивные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:07:09','2018-12-14 09:07:09'),(47,'{\"en\": null, \"kg\": null, \"ru\": \"Губки полимерные LEMONMOON (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 120*80 мм. Из полимерных нитей. Для очистки антипригарных поверхностей\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 120*80 мм. Из полимерных нитей. Для очистки антипригарных поверхностей</p>\"}',70.00,'gubki-polimernye-lemonmoon-2-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Губки полимерные LEMONMOON (2 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:09:09','2018-12-18 13:38:55'),(48,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для туалета\\\", 0,75 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Средство-гель для удаления желтизны в унитазе. Нейтральных запах\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Средство-гель для удаления желтизны в унитазе. Нейтральных запах</p>\"}',230.00,'sredstvo-dlya-tualeta-0-75-l-1',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство \\\"Для туалета\\\", 0,75 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:18:01','2018-12-14 09:18:01'),(49,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки вискозные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 30*38 см. 80%  вискозы. Устойчивые к химсоставам. Хорошо впитывают\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 30*38 см. 80% &nbsp;вискозы. Устойчивые к химсоставам. Хорошо впитывают</p>\"}',35.00,'salfetki-viskoznye-lemonmoon-3-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки вискозные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:20:40','2018-12-14 09:20:40'),(50,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки вискозные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 38*38 см. 80%  вискозы. Устойчивые к химсоставам. Хорошо впитывают\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 38*38 см. 80% &nbsp;вискозы. Устойчивые к химсоставам. Хорошо впитывают</p>\"}',60.00,'salfetki-viskoznye-lemonmoon-3-sht-1',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки вискозные LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:21:26','2018-12-14 09:21:26'),(51,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки воскозные LEMONMOON (10 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 34*38 см. Для разл.поверхностей. Быстро высыхают. 10 шт в пачке\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 34*38 см. Для разл.поверхностей. Быстро высыхают. 10 шт в пачке</p>\"}',110.00,'salfetki-voskoznye-lemonmoon-10-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки воскозные LEMONMOON (10 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:22:22','2018-12-14 09:22:22'),(52,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 15*15 см. Высшая впитывающая способность - в 12 раз больше собственного веса\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 15*15 см. Высшая впитывающая способность - в 12 раз больше собственного веса</p>\"}',70.00,'salfetki-gubchatye-lemonmoon-3-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:23:24','2018-12-14 09:24:08'),(53,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 15*18 см. Высшая впитывающая способность - в 12 раз больше собственного веса\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 15*18 см. Высшая впитывающая способность - в 12 раз больше собственного веса</p>\"}',80.00,'salfetki-gubchatye-lemonmoon-3-sht-1',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:24:48','2018-12-18 13:14:00'),(54,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 18*20 см. Высшая впитывающая способность - в 12 раз больше собственного веса\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 18*20 см. Высшая впитывающая способность - в 12 раз больше собственного веса</p>\"}',120.00,'salfetki-gubchatye-lemonmoon-3-sht-2',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Салфетки губчатые LEMONMOON (3 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:25:27','2018-12-18 13:44:36'),(55,'{\"en\": null, \"kg\": null, \"ru\": \"Тряпка для пола LEMONMOON (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 50*60 см. Из вискозы. Плотность 120 гр/м2\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 50*60 см. Из вискозы. Плотность 120 гр/м2</p>\"}',40.00,'tryapka-dlya-pola-lemonmoon-1-sht',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Тряпка для пола LEMONMOON (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:26:45','2018-12-14 09:26:45'),(56,'{\"en\": null, \"kg\": null, \"ru\": \"Тряпка для пола LEMONMOON (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": \"Размер 50*60 см. Из вискозы. Плотность 160 гр/м2\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Размер 50*60 см. Из вискозы. Плотность 160 гр/м2</p>\"}',55.00,'tryapka-dlya-pola-lemonmoon-1-sht-1',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Тряпка для пола LEMONMOON (1 шт)\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-14 09:27:24','2018-12-14 09:27:24'),(57,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для химчистки \\\"CHISTO\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Химчистка салона, очистка сидений и всех элементов интерьера. Готовое к применению.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Химчистка салона, очистка сидений и всех элементов интерьера. Готовое к применению.</p>\"}',140.00,'sredstvo-dlya-himchistki-chisto-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для химчистки \\\"CHISTO\\\", 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:24:05','2018-12-19 08:25:33'),(58,'{\"en\": null, \"kg\": null, \"ru\": \"Концентрат для химчистки \\\"CHISTO\\\" 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Химчистка салона, очистка сидений и всех элементов интерьера. \\r\\nБутылка с распылителем в комплекте\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Концентрат для химчистки салона. Разведение водой: 5%-20%.</p>\\r\\n\\r\\n<p>Бутылка с распылителем в комплекте.</p>\"}',580.00,'koncentrat-dlya-himchistki-chisto-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:26:56','2018-12-19 08:27:30'),(59,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для приборной панели \\\"PLASTIK\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Средство для полировки пластиковых деталей интерьера. Готов к применению.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Средство для полировки пластиковых деталей интерьера. Готов к применению.</p>\"}',150.00,'sredstvo-dlya-pribornoy-paneli-plastik-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для приборной панели \\\"PLASTIK\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:28:41','2018-12-19 08:28:41'),(60,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки мотора \\\"MOTOR\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Средство для мытья двигателя (мотора). Готовое к применению\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Средство для мытья двигателя (мотора). Готовое к применению</p>\"}',140.00,'sredstvo-dlya-chistki-motora-motor-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки мотора \\\"MOTOR\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:30:47','2018-12-19 08:30:47'),(61,'{\"en\": null, \"kg\": null, \"ru\": \"Концентрат для чистки мотора \\\"MOTOR\\\" 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Концентрат для мытья двигателя (мотора). Разведение водой: 5%-20%.\\r\\nБутылка с рапылителем в комплекте\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Концентрат для мытья двигателя (мотора). Разведение водой: 5%-20%.</p>\\r\\n\\r\\n<p>Бутылка с рапылителем в комплекте</p>\"}',580.00,'koncentrat-dlya-chistki-motora-motor-1-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Концентрат для чистки мотора \\\"MOTOR\\\" 1 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:32:03','2018-12-19 08:32:03'),(62,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чернения шин \\\"CHERNO\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Жидкость для чернения покрышек. Готовое к применению. Антистатический эффект.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Жидкость для чернения покрышек. Готовое к применению. Антистатический эффект.</p>\"}',170.00,'sredstvo-dlya-cherneniya-shin-cherno-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чернения шин \\\"CHERNO\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:33:47','2018-12-19 08:33:47'),(63,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки автомобильных дисков \\\"DISKI\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Жидкость для удаления нагара с колесных дисков. Готово к применению.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Жидкость для удаления нагара с колесных дисков. Готово к применению.</p>\"}',150.00,'sredstvo-dlya-chistki-avtomobilnyh-diskov-diski-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки автомобильных дисков \\\"DISKI\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:34:48','2018-12-19 08:34:48'),(64,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки автомобильных стекол \\\"STEKLO\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": \"Спиртосодержащее средство для мытья стекол и зеркал. Готово к применению.\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>Спиртосодержащее средство для мытья стекол и зеркал. Готово к применению.</p>\"}',130.00,'sredstvo-dlya-chistki-avtomobilnyh-stekol-steklo-0-6-l',NULL,'{\"en\": null, \"kg\": null, \"ru\": \"Средство для чистки автомобильных стекол \\\"STEKLO\\\" 0,6 л\"}','{\"en\": null, \"kg\": null, \"ru\": null}','{\"en\": null, \"kg\": null, \"ru\": null}',1,0,0,NULL,'2018-12-19 08:38:02','2018-12-19 08:38:02'),(65,'{\"en\": null, \"kg\": null, \"ru\": \"qqq111\"}','{\"en\": null, \"kg\": null, \"ru\": \"qqq11\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>qqqq11</p>\"}',111.00,'qqq',111,'{\"en\": null, \"kg\": null, \"ru\": \"aaa11\"}','{\"en\": null, \"kg\": null, \"ru\": \"aaa11\"}','{\"en\": null, \"kg\": null, \"ru\": \"aaa11\"}',0,0,0,'2018-12-23 12:05:06','2018-12-23 11:42:37','2018-12-23 12:05:06'),(66,'{\"en\": null, \"kg\": null, \"ru\": \"qwe\"}','{\"en\": null, \"kg\": null, \"ru\": \"qwe\"}','{\"en\": null, \"kg\": null, \"ru\": \"<p>qwe</p>\"}',111.00,'qwe',123,'{\"en\": null, \"kg\": null, \"ru\": \"qwe\"}','{\"en\": null, \"kg\": null, \"ru\": \"qwe\"}','{\"en\": null, \"kg\": null, \"ru\": \"qwe\"}',0,0,0,'2018-12-23 14:16:15','2018-12-23 14:05:39','2018-12-23 14:16:15');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotion_products`
--

DROP TABLE IF EXISTS `promotion_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotion_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `promotion_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotion_products`
--

LOCK TABLES `promotion_products` WRITE;
/*!40000 ALTER TABLE `promotion_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotion_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promotions`
--

DROP TABLE IF EXISTS `promotions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promotions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` json NOT NULL,
  `desc` json DEFAULT NULL,
  `meta_title` json DEFAULT NULL,
  `meta_description` json DEFAULT NULL,
  `meta_keywords` json DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_display` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promotions`
--

LOCK TABLES `promotions` WRITE;
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions`
--

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` VALUES (1,'{\"en\": \"Bishkek\", \"kg\": \"Бишкек\", \"ru\": \"Бишкек\"}',NULL,NULL),(2,'{\"en\": \"Batken\", \"kg\": \"Баткен\", \"ru\": \"Баткен\"}',NULL,NULL),(3,'{\"en\": \"Jalal-Abad\", \"kg\": \"Жалал-Абад\", \"ru\": \"Жалал-Абад\"}',NULL,NULL),(4,'{\"en\": \"Issyk-Kul\", \"kg\": \"Ысык-Көл\", \"ru\": \"Иссык-Куль\"}',NULL,NULL),(5,'{\"en\": \"Naryn\", \"kg\": \"Нарын\", \"ru\": \"Нарын\"}',NULL,NULL),(6,'{\"en\": \"Osh\", \"kg\": \"Ош\", \"ru\": \"Ош\"}',NULL,NULL),(7,'{\"en\": \"Talas\", \"kg\": \"Талас\", \"ru\": \"Талас\"}',NULL,NULL),(8,'{\"en\": \"Chui\", \"kg\": \"Чуй\", \"ru\": \"Чуй\"}',NULL,NULL);
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_admins`
--

DROP TABLE IF EXISTS `role_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_admins` (
  `admin_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`admin_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_admins`
--

LOCK TABLES `role_admins` WRITE;
/*!40000 ALTER TABLE `role_admins` DISABLE KEYS */;
INSERT INTO `role_admins` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `role_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permissions` (
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permissions`
--

LOCK TABLES `role_permissions` WRITE;
/*!40000 ALTER TABLE `role_permissions` DISABLE KEYS */;
INSERT INTO `role_permissions` VALUES (1,1),(1,2),(1,3);
/*!40000 ALTER TABLE `role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` enum('admin','client') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Админ_С');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-23 14:30:28
