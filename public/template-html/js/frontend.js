$('#search-form .search').bind('input', function(){
    // console.log($('.search-table').attr('data-url'));
    $('#search-form').submit();

});
$('.search').on('click',function () {
    $('.search').css('width','300px');
    $('.search').css('background-position','270px');
    // $('.search').css('transition','2s');
});
$('.search').focusout(function () {
    $('.search').css('width', '155px');
    $('.search').css('background-position', '130px');
    $('.search-results').css('display','none');
});
$('#search-form').on('submit',function (e) {
    e.preventDefault();
    let url = $(this).attr('action');
    let search = $(this).find('input').val();
    $.get(url, {search: search}, function(data){
        $('.search-table').html(data.searchHtml);

        // console.log(data);

       // searchHtml
    });
    // $('.search-table').load($(this).attr('data-url'))
    // $('#search-table').children()
});
$('.sub-sub-cat').mouseover(function(){
    $(this).css('color','#FE980F');
});
$('.sub-sub-cat').mouseleave(function(){
    $(this).css('color','#000');
});
$('.category-li').mouseover(function(){
    $(this).children().css('color','rgb(254,160,0)')
});
$('.category-li').mouseleave(function(){
    $(this).children().css('color','rgb(104,104,99)')
});
$('.sub-cat-li').mouseover(function(){
    $(this).children().css('color','rgb(254,160,0)')
});
$('.sub-cat-li').mouseleave(function(){
    $(this).children().css('color','rgb(104,104,99)')
});
$('.sub-sub-cat-li').mouseover(function(){
    $(this).children().css('color','rgb(254,160,0)');
    $(this).children().css('border-top','1px solid');
    $(this).children().css('border-bottom','1px solid');
});
$('.sub-sub-cat-li').mouseleave(function(){
    $(this).children().css('color','rgb(104,104,99)');
    $(this).children().css('border-top','none');
    $(this).children().css('border-bottom','none');
});