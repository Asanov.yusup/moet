$('#sectionsPlaceholder .tree-data').on('select_node.jstree', function (e, data) {
    var link = $('#' + data.selected).find('a');
    if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
        if (link.attr("target") == "_blank") {
            link.attr("href").target = "_blank";
        }
        document.location.href = link.attr("href");
        return false;
    }
});

$(document).ready(function () {
    initJsTree("#sectionsPlaceholder");
});

$("#largeModal").on("hidden.bs.modal", function () {
    location.reload();
});


$('#sectionsPlaceholder .tree-data').on('ready.jstree', function () {
    let li = $(this).find('li');
    li.each(function (i) {
        $(this).jstree('open_node', li.eq(i));
    });

    $(this).fadeIn("slow");
});


let initJsTree = function (selector) {

    let tree = $(selector).find('.tree-data');
    tree.jstree({
        "core": {
            "themes": {
                "responsive": false
            }
        },
        "types": {
            "default": {
                "icon": "fa fa-folder m--font-warning"
            },
            "file": {
                "icon": "fa fa-file  m--font-warning"
            }
        },
        "plugins": ['types'],

        "onCreateLi": function (node, $li) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.
            $li.find('.jqtree-element').append(
                '<a href="#node-' + node.id + '" class="edit" data-node-id="' +
                node.id + '">edit</a>'
            );
        }
    });
};

$(document.body).on('click', '.delete-catalog-item', function (e) {

    e.preventDefault();
    let url = $(this).attr('href');

    swal({
        title: 'Вы уверены, что хотите удалить?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Удалить!',
        cancelButtonText: 'Отмена'
    }).then((result) => {
        if (result.value) {

            $.get(url, function (data) {
                if(data.redirect_url){
                    location.replace(data.redirect_url);
                } else {
                    swal({
                        type: data.message_data.type,
                        title: data.message_data.title,
                        text: data.message_data.text,
                    })
                }
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {

        }
    })
});


